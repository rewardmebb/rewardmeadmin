/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.brandowner;

import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement;
import com.mollatech.rewardme.nucleus.settings.SendNotification;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "SetandResendPassword", urlPatterns = {"/SetandResendPassword"})
public class SetandResendPassword extends HttpServlet {

    final String itemtype = "USERPASSWORD";

    final String itemTypeAUTH = "AUTHORIZTION";

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;
    
     static final Logger logger = Logger.getLogger(SetandResendPassword.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String channelName = "Reward Me Portal";
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String _userid = request.getParameter("_partnerId");
        int id = Integer.parseInt(_userid);
        String result = "success";
        String message = "Password set and send successfully";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        logger.info("ownerId: " + _userid);
        int iapprovalID = -1;
        if (_userid == null) {
            result = "error";
            message = "Fill all Details";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        BrandOwnerManagement um = new BrandOwnerManagement();
        RmBrandownerdetails user = um.getBrandOwnerDetails(id);
        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + _userid + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;        
        retValue = um.SetPassword(_userid, strPassword);        
        if (retValue == 0) {
        } else {
            result = "error";
            message = "password reset failed";
            try {
                json.put("_result", result);
                json.put("_message", message);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  result is " + result);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        String tmessage = LoadSettings.g_templateSettings.getProperty("email.partner.password");        
        if (tmessage != null) {
            d = new Date();
            String enquiryId = LoadSettings.g_sSettings.getProperty("email.enquiry");
            String supportId = LoadSettings.g_sSettings.getProperty("email.question");
            String ideaId = LoadSettings.g_sSettings.getProperty("email.idea");
            String appurl = (request.getRequestURL().toString());
            URL myAppUrl = new URL(appurl);
            int port = myAppUrl.getPort();
            if (myAppUrl.getProtocol().equals("https") && port == -1) {
                port = 443;
            } else if (myAppUrl.getProtocol().equals("http") && port == -1) {
                port = 80;
            }            
            String path = request.getContextPath();    
            String greenbackGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/greenback.gif";
            String spadeGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/spade.gif";
            String addressbookGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/addressbook.gif";
            String penpaperGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/penpaper.gif";
            String lightbulbGIF = myAppUrl.getProtocol() + "://" + myAppUrl.getHost() + ":" + port + path + "/images/email/lightbulb.gif";            
            logger.info("greenbackGIF image path : " + greenbackGIF);
            
            String[] enquiryEmailDetails = enquiryId.split(":");
            String[] supportEmailDetails = supportId.split(":");
            String[] ideaEmailDetails = ideaId.split(":");
            
            tmessage = tmessage.replaceAll("#name#", user.getBrandName());
            tmessage = tmessage.replaceAll("#channel#", channelName);
            tmessage = tmessage.replaceAll("#password#", strPassword);
            tmessage = tmessage.replaceAll("#email#", user.getEmail());
            tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
            tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
            tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
            tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
            tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

            tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
            tmessage = tmessage.replaceAll("#supportId#", supportId);
            tmessage = tmessage.replaceAll("#ideaId#", ideaId);

            tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
            tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
            tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);    
        }
        if (user == null || strPassword == null) {
            result = "error";
            message = "Password Not Sent";
            try {
                json.put("_result", result);
                json.put("_message", message);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  result is " + result);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

        int productType = 3;
        RMStatus statusSG = new SendNotification().SendEmail(user.getEmail(), " Your Password Reset for Reward Me", tmessage, null, null, null, null, productType);
        if (statusSG.iStatus == PENDING || statusSG.iStatus == SENT) {
        } else {
            result = "error";
            message = "Password Not Sent";
            try {
                json.put("_result", result);
                json.put("_message", message);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  result is " + result);
                logger.info("Response of #SetandResendPassword from Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            logger.info("Response of #SetandResendPassword from Servlet's Parameter  result is " + result);
            logger.info("Response of #SetandResendPassword from Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
