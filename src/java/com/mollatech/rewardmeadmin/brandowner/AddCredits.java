/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.brandowner;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

@WebServlet(name = "AddCredits", urlPatterns = {"/AddCredits"})
public class AddCredits extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            String developerId = request.getParameter("developerId");
            String mCredit = request.getParameter("_mainCredits");
            String Credit = request.getParameter("_Credits");
            double mc = 0f;
            double c = 0f;
            double cr = 0;
            if (!Credit.isEmpty()) {
                mc = Double.parseDouble(Credit);
                c = Double.parseDouble(mCredit);
                cr = mc + c;

            }
            float f = (float) cr;
            int res=-1;
            int id = Integer.parseInt(developerId);
            RmCreditinfo info = new CreditManagement().getDetailsByOwnerId(id);
            if(info !=null){
                info.setMainCredit(f);
                info.setStatus(GlobalStatus.UPDATED);
                float expectedCreditUsed = info.getExpectedCreditDeduction();
                expectedCreditUsed = Float.parseFloat(String.valueOf(mc)) + expectedCreditUsed;
                info.setExpectedCreditDeduction(expectedCreditUsed);
                res = new CreditManagement().updateDetails(info);
            }
            if (res == 0) {
                json.put("result", "success");
                json.put("message", "Credits Added Successfully. Please Wait for 1 Minutes For Getting It Reflected.");
            } else {
                json.put("result", "error");
                json.put("message", "Error In Adding Credits.");
            }
        } catch (Exception ex) {
            json.put("result", "error");
            json.put("message", "Error In Adding Credits.");
            ex.printStackTrace();
        }
        out.print(json);
        out.flush();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
