package com.mollatech.rewardmeadmin.common;


import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@WebServlet(name = "Log4JInitServlet", urlPatterns = "/init", loadOnStartup = 1)
public class Log4JInitServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext sc = config.getServletContext();
        String webAppPath = sc.getContextPath();
        webAppPath = webAppPath.replace("/", "");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = System.getenv("DOMAIN_HOME");
        }
        String ErrorLevel_RewardMe = "ALL";
        if (LoadSettings.g_sSettings.getProperty("brandowner.log.level") != null && !LoadSettings.g_sSettings.getProperty("brandowner.log.level").isEmpty()) {
            ErrorLevel_RewardMe = LoadSettings.g_sSettings.getProperty("brandowner.log.level");
        }
        Properties logProp = new Properties();
        logProp.put("log4j.debug", "TRUE");
        logProp.put("log4j.appender.file", "org.apache.log4j.RollingFileAppender");
        logProp.put("log4j.rootLogger", ErrorLevel_RewardMe + ", RollingAppender");
        logProp.put("log4j.appender.RollingAppender", "org.apache.log4j.DailyRollingFileAppender");
        logProp.put("log4j.appender.RollingAppender.File", usrhome + "/RewardMeLogs/"+ "/RewardMeAdmin.log");
        logProp.put("log4j.appender.RollingAppender.DatePattern", "'.'yyyy-MM-dd");
        logProp.put("log4j.appender.RollingAppender.layout", "org.apache.log4j.PatternLayout");
        logProp.put("log4j.appender.RollingAppender.layout.ConversionPattern", "[%p] %d %c %M - %m%n");
        logProp.put("log4j.appender.file.MaxFileSize", "100MB");
        logProp.put("log4j.appender.file.MaxBackupIndex", "10");
        PropertyConfigurator.configure(logProp);
        super.init(config);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This is the Log4JInitServlet<br/>");
        String logLevel = request.getParameter("logLevel");
        String reloadPropertiesFile = request.getParameter("reloadPropertiesFile");
        if (logLevel != null) {
            setLogLevelWithParameter(out, logLevel);
        } else if (reloadPropertiesFile != null) {
            out.println("Attempting to reload log4j properties file<br/>");
            loadLog4jPropertiesFile(out);
        } else {
            out.println("no logLevel or reloadPropertiesFile parameters were found<br/>");
        }
    }

    private void setLogLevelWithParameter(PrintWriter out, String logLevel) {
        Logger root = Logger.getRootLogger();
        boolean logLevelRecognized = true;
        if ("DEBUG".equalsIgnoreCase(logLevel)) {
            root.setLevel(Level.DEBUG);
        } else if ("INFO".equalsIgnoreCase(logLevel)) {
            root.setLevel(Level.INFO);
        } else if ("WARN".equalsIgnoreCase(logLevel)) {
            root.setLevel(Level.WARN);
        } else if ("ERROR".equalsIgnoreCase(logLevel)) {
            root.setLevel(Level.ERROR);
        } else if ("FATAL".equalsIgnoreCase(logLevel)) {
            root.setLevel(Level.FATAL);
        } else {
            logLevelRecognized = false;
        }

        if (logLevelRecognized) {
            out.println("Log level has been set to: " + logLevel + "<br/>");
        } else {
            out.println("logLevel parameter '" + logLevel + "' level not recognized<br/>");
        }
    }

    private void loadLog4jPropertiesFile(PrintWriter out) {
        ServletContext sc = getServletConfig().getServletContext();
        String log4jLocation = getInitParameter("log4j-properties-location");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = System.getenv("DOMAIN_HOME");
        }
        if (log4jLocation == null) {
            out.println("*** No log4j-properties-location init param, so initializing log4j with BasicConfigurator<br/>");
            BasicConfigurator.configure();
        } else {
            String webAppPath = sc.getRealPath("/");
            String log4jProp = webAppPath + log4jLocation;
            File log4jFile = new File(log4jProp);
            if (log4jFile.exists()) {
                out.println("Initializing log4j with: " + log4jProp + "<br/>");
                //code to edit according to your dynamic war name
                Properties logProp = new Properties();
                logProp.put("log4j.debug", "TRUE");
                logProp.put("log4j.appender.file", "org.apache.log4j.RollingFileAppender");
                logProp.put("log4j.rootLogger", "ALL, RollingAppender");
                logProp.put("log4j.appender.RollingAppender", "org.apache.log4j.DailyRollingFileAppender");
                logProp.put("log4j.appender.RollingAppender.File", usrhome + "/log4jlogs/" + webAppPath);

                logProp.put("log4j.appender.RollingAppender.DatePattern", "'.'yyyy-MM-dd");
                logProp.put("log4j.appender.RollingAppender.layout", "org.apache.log4j.PatternLayout");
                logProp.put("log4j.appender.RollingAppender.layout.ConversionPattern", "[%p] %d %c %M - %m%n");
                logProp.put("log4j.appender.file.MaxFileSize", "100MB");
                logProp.put("log4j.appender.file.MaxBackupIndex", "10");
//
                PropertyConfigurator.configure(log4jProp);
            } else {
                out.println("*** " + log4jProp + " file not found, so initializing log4j with BasicConfigurator<br/>");
                BasicConfigurator.configure();
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
