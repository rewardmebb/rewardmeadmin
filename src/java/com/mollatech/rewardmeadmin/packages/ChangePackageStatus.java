/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.packages;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.UnapprovalPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ChangePackageStatus", urlPatterns = {"/ChangePackageStatus"})
public class ChangePackageStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Package approved successfully";
        PrintWriter out = response.getWriter();
        String _packageName = request.getParameter("_packageName");
        String reqStatus = request.getParameter("_status");
        int ireqStatus = 0;
        if (reqStatus != null) {
            ireqStatus = Integer.parseInt(reqStatus);
        }
        int a = -1;
        ApprovedPackageManagement pm = new ApprovedPackageManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            RmApprovedpackagedetails sgReqPackage = pm.getReqPackageByName(SessionId, _packageName);
            if (sgReqPackage != null) {
                RmUnapprovalpackagedetails bucketdetails = new UnapprovalPackageManagement().getPackageByName(SessionId, _packageName);
                                
                sgReqPackage.setPackageDuration(bucketdetails.getPackageDuration());
                sgReqPackage.setCreditDeductionConfiguration(bucketdetails.getCreditDeductionConfiguration());                
                sgReqPackage.setMainCredits(bucketdetails.getMainCredits());
                sgReqPackage.setTax(bucketdetails.getTax());               
                sgReqPackage.setBrandOwnerVisibility(bucketdetails.getBrandOwnerVisibility());
                sgReqPackage.setMakerflag(GlobalStatus.SUSPEND);
                sgReqPackage.setUpdationDate(new Date());
                sgReqPackage.setApprovedpckId(sgReqPackage.getApprovedpckId());
                sgReqPackage.setPackageDescription(bucketdetails.getPackageDescription());
                sgReqPackage.setPlanAmount(bucketdetails.getPlanAmount());
                sgReqPackage.setRecurrenceBillingPlanId(bucketdetails.getRecurrenceBillingPlanId());
                
                pm.updateDetails(sgReqPackage);
                a = pm.ChangeRequestStatus(SessionId, sgReqPackage.getPackageName(), ireqStatus);
            }
            if (a == 0) {
                result = "success";
                message = "Package approved successfully";
            } else if (a == 3) {
                result = "error";
                if (ireqStatus == GlobalStatus.ACTIVE) {
                    message = "Package details already approved  ";
                } else {
                    message = "Package did not approved ";
                }
            } else {
                result = "error";
                message = "Package did not approved ";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
