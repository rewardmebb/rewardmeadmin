/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.packages;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.commons.MakerChecker;
import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.UnapprovalPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "CreatePackage", urlPatterns = {"/CreatePackage"})
public class CreatePackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreatePackage.class);
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #CreatePackage from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package have been created successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        MakerChecker makerChakerObj = (MakerChecker) request.getSession().getAttribute("makerChacker");
        
        String packageName = request.getParameter("_packageName");
        String packagePrice = request.getParameter("_packagePrice");

        String perCampaign = request.getParameter("_Per_Campaign");
        String userSearched = request.getParameter("_userSearched");

        String packageDuration = request.getParameter("_packageDuration");
        String paymentMode = request.getParameter("_paymentMode");
        String packageDesc = request.getParameter("packageDesc");
        String _recurrienceBillingID = request.getParameter("_recurrienceBillingID");
        String _credit = request.getParameter("_credit");
        String _tax = request.getParameter("_tax");
        String BrandOwnerVisibility[] = request.getParameterValues("visibleTo");
        if (BrandOwnerVisibility != null) {
            logger.debug("Value of PartnerVisibility = " + Arrays.toString(BrandOwnerVisibility));
        }
        logger.debug("Value of _packageName = " + packageName);
        logger.debug("Value of _packagePrice = " + packagePrice);
        logger.debug("Value of perCampaign = " + perCampaign);
        logger.debug("Value of userSearched = " + userSearched);
        logger.debug("Value of _packageDuration = " + packageDuration);
        logger.debug("Value of _paymentMode = " + paymentMode);
        logger.debug("Value of packageDesc = " + packageDesc);
        logger.debug("Value of _recurrienceBillingID = " + _recurrienceBillingID);
        logger.debug("Value of _credit = " + _credit);
        logger.debug("Value of _tax = " + _tax);
        RmUnapprovalpackagedetails packageObject = new UnapprovalPackageManagement().getPackageByName(SessionId, packageName);
        if (packageObject != null) {
            result = "error";
            message = "Package name already taken, Try with other name.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreatePackage from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        String partnerId = "";
        if (BrandOwnerVisibility != null) {
            for (int i = 0; i < BrandOwnerVisibility.length; i++) {
                partnerId += BrandOwnerVisibility[i] + ",";
            }
        }
        Float price = Float.parseFloat(packagePrice);
        Float credit = Float.parseFloat(_credit);
        RmUnapprovalpackagedetails bucketObj = new RmUnapprovalpackagedetails();

        bucketObj.setPackageName(packageName.trim());
        bucketObj.setPlanAmount(price);
        bucketObj.setPaymentMode(paymentMode);
        JSONObject jsonvalue = new JSONObject();
        jsonvalue.put("perCampaign", perCampaign);
        jsonvalue.put("userSearched", userSearched);
        bucketObj.setPackageDuration(packageDuration);
        bucketObj.setCreditDeductionConfiguration(jsonvalue.toString());
        
        if (makerChakerObj != null) {
            if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                bucketObj.setStatus(GlobalStatus.APPROVED);
            }else{
                bucketObj.setStatus(GlobalStatus.SENDTO_CHECKER);
            }
        }else{
            bucketObj.setStatus(GlobalStatus.SENDTO_CHECKER);
        }
        //bucketObj.setStatus(GlobalStatus.SENDTO_CHECKER);
        bucketObj.setCreationDate(new Date());
        bucketObj.setPackageDescription(packageDesc);
        bucketObj.setRecurrenceBillingPlanId(_recurrienceBillingID);
        bucketObj.setMainCredits(credit);
        bucketObj.setTax(_tax);
        bucketObj.setBrandOwnerVisibility(partnerId);
        if (_tax != null) {
            bucketObj.setTax(_tax);
        }
        int retValue = new UnapprovalPackageManagement().CreateUnApprovalPackage(SessionId, bucketObj);
        
        RmApprovedpackagedetails reqObj = new RmApprovedpackagedetails();
        RmApprovedpackagedetails existsObj = new ApprovedPackageManagement().getReqPackageByName(SessionId,bucketObj.getPackageName());
        if (makerChakerObj != null) {
            if (makerChakerObj.status == GlobalStatus.ACTIVE) {
                reqObj.setMakerflag(1);
                reqObj.setStatus(bucketObj.getStatus());
                message = "Package created successfully and send Checker to validate";
            } else {
                reqObj.setStatus(GlobalStatus.APPROVED);
                reqObj.setMakerflag(0);
            }
        } else {
            reqObj.setStatus(bucketObj.getStatus());
            reqObj.setMakerflag(GlobalStatus.APPROVED);
        }
        reqObj.setMainCredits(bucketObj.getMainCredits());
                if (existsObj == null) {                    
                    reqObj.setPackageName(bucketObj.getPackageName());
                    reqObj.setPlanAmount(bucketObj.getPlanAmount());
                    reqObj.setPaymentMode(bucketObj.getPaymentMode());
                    reqObj.setPackageDuration(bucketObj.getPackageDuration());
                    reqObj.setBrandOwnerVisibility(bucketObj.getBrandOwnerVisibility());                                                                                
                    reqObj.setTax(bucketObj.getTax());
                    reqObj.setCreationDate(bucketObj.getCreationDate());
                    reqObj.setCreditDeductionConfiguration(bucketObj.getCreditDeductionConfiguration());                    
                    reqObj.setPackageDescription(bucketObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(bucketObj.getRecurrenceBillingPlanId());                    
                    new ApprovedPackageManagement().CreateReqPackage(SessionId, reqObj);
                } else {
                    reqObj.setPackageName(bucketObj.getPackageName());
                    reqObj.setPlanAmount(bucketObj.getPlanAmount());
                    reqObj.setPaymentMode(bucketObj.getPaymentMode());
                    reqObj.setPackageDuration(bucketObj.getPackageDuration());
                    reqObj.setBrandOwnerVisibility(bucketObj.getBrandOwnerVisibility());                                                                                
                    reqObj.setTax(bucketObj.getTax());
                    reqObj.setCreationDate(bucketObj.getCreationDate());
                    reqObj.setCreditDeductionConfiguration(bucketObj.getCreditDeductionConfiguration());                    
                    reqObj.setPackageDescription(bucketObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(bucketObj.getRecurrenceBillingPlanId());  
                    reqObj.setStatus(GlobalStatus.SENDTO_CHECKER);                    
                    new ApprovedPackageManagement().editReqPackage(SessionId, reqObj);
                   // message = "Package Updated successfully.";
                    message = "Package updated successfully and send Checker to validate";
                }
        try {
            if (retValue > 0) {
                json.put("result", result);
                json.put("message", message);
                json.put("retValue", retValue);
            } else {
                result = "error";
                message = "Package does not created.";
                json.put("result", result);
                logger.debug("Response of #CreatePackage Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreatePackage Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #CreatePackage from " + json.toString());
            logger.info("Response of #CreatePackage from Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
