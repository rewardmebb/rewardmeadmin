/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.setting;

import com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "TestConnection", urlPatterns = {"/TestConnection"})
public class TestConnection extends HttpServlet {

    static final Logger logger = Logger.getLogger(TestConnection.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is TestConnection at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");        
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String _type = request.getParameter("_type");
        String _preference = request.getParameter("_preference");
        String _testphone = null;
        String _testmsg = null;
        if (_preference.compareTo("1") == 0) {
            _testphone = request.getParameter("_testphone");
            _testmsg = request.getParameter("_testmsg");
        } else if (_preference.compareTo("2") == 0) {
            _testphone = request.getParameter("_testphoneS");
            _testmsg = request.getParameter("_testmsgS");
        }
        int _type1 = Integer.parseInt(_type);
        int _preference1 = Integer.parseInt(_preference);
        String result = "success";
        String message = "Connection sucessful";
        int retValue = -1;
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            retValue = sMngmt.testSetting(sessionId, _type1, _preference1, _testphone, _testmsg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (retValue == 0) {
            result = "success";
            message = "Connection successful";
        } else if (retValue == -3) {
            result = "error";
            message = "Message blocked by Prefix Filter";
        } else if (retValue == -4) {
            result = "error";
            message = "Message blocked by Domain Filter";
        } else {
            result = "error";
            message = "Failed to send test message. Check Settings";
        }
        try {
            json.put("_result", result);
            logger.debug("Response of TestConnection Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of TestConnection Servlet's Parameter  message is " + message);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            logger.info("Response of TestConnection " + json.toString());
            logger.info("Response of TestConnection Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
