/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.setting;

import com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.rewardme.nucleus.settings.OOBEmailChannelSettings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "EditEmailSettings", urlPatterns = {"/EditEmailSettings"})
public class EditEmailSettings extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditEmailSettings.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPENDED_STATUS = 1;

    public static final int PREFERENCE_ONE = 1;

    public static final int PREFERENCE_TWO = 2;

    final String itemtype = "SETTINGS";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is EditEmailSettings at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");        
        SettingsManagement sMngmt = new SettingsManagement();
        int retValue = -1;
        boolean digits = true;
        String result = "success";
        String message = "Email SMTP Gateway Settings Update Successful";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.Email_Gateway_Connector) != 0) {
//            result = "error";
//            message = "This feature is not available in this license";
//            try {
//                json.put("_result", result);
//                logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
//                json.put("_message", message);
//                logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
//            } catch (Exception e) {
//                logger.error("Exception at EditEmailSettings ", e);
//            }
//            out.print(json);
//            out.flush();
//            return;
//        }
        String _preference = request.getParameter("_perferenceEMAIL");
        logger.debug("Value of _perferenceEMAIL = " + _preference);
        int _iPreference = Integer.parseInt(_preference);
        int _type = SettingsManagement.EMAIL;
        String strType = String.valueOf(_type);
        Object settingsObj = sMngmt.getSetting(sessionId, _type, _iPreference);
        OOBEmailChannelSettings emailObj = null;
        boolean bAddSetting = false;
        if (settingsObj == null) {
            emailObj = new OOBEmailChannelSettings();
            emailObj.setPreference(_iPreference);
            bAddSetting = true;
        } else {
            emailObj = (OOBEmailChannelSettings) settingsObj;
        }
        if (_type == SettingsManagement.EMAIL) {
            if (_iPreference == SettingsManagement.PREFERENCE_ONE) {
                String ssl = request.getParameter("_sslEMAIL");
                logger.debug("Value of _sslEMAIL = " + ssl);
                boolean bSSLEnabled = false;
                if (ssl != null) {
                    if (ssl.equals("sslEnabled")) {
                        bSSLEnabled = true;
                    }
                }
                String _auth = request.getParameter("_authRequiredEMAIL");
                logger.debug("Value of _authRequiredEMAIL = " + _auth);
                boolean bAuthRequired = false;
                if (_auth != null) {
                    if (_auth.equals("authEnabled")) {
                        bAuthRequired = true;
                    }
                }
                String emailPort = request.getParameter("_portEMAIL");
                logger.debug("Value of _portEMAIL = " + emailPort);
                String emailIP = request.getParameter("_ipEMAIL");
                logger.debug("Value of _ipEMAIL = " + emailIP);
                String implclass = request.getParameter("_classNameEMAIL");
                logger.debug("Value of _classNameEMAIL = " + implclass);
                for (int i = 0; i < emailPort.length(); i++) {
                    if (!Character.isDigit(emailPort.charAt(i))) {
                        digits = false;
                        break;
                    }
                }
                if (emailIP == null || emailPort == null || implclass == null || emailIP.isEmpty() || emailPort.isEmpty() || implclass.isEmpty()) {
                    result = "error";
                    message = "Fill all Details";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
                    } catch (Exception e) {
                        logger.error("Exception at EditEmailSettings ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } else if (!digits) {
                    result = "error";
                    message = "Port should contain only digits";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
                    } catch (Exception e) {
                        logger.error("Exception at EditEmailSettings ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                int _autofailoverEMAIL = Integer.valueOf(request.getParameter("_autofailoverEMAIL")).intValue();
                int _port = Integer.valueOf(request.getParameter("_portEMAIL")).intValue();
                int _retryduration = Integer.valueOf(request.getParameter("_retrydurationEMAIL")).intValue();
                int _retrycount = Integer.valueOf(request.getParameter("_retriesEMAIL")).intValue();
                int _status = Integer.valueOf(request.getParameter("_statusEMAIL")).intValue();
                emailObj.setAuthRequired(bAuthRequired);
                emailObj.setSsl(bSSLEnabled);
                emailObj.setPreference(_iPreference);
                emailObj.setAutofailover(_autofailoverEMAIL);
                emailObj.setClassName(request.getParameter("_classNameEMAIL"));
                emailObj.setFromEmail(request.getParameter("_fromEmailEMAIL"));
                emailObj.setFromName(request.getParameter("_fromNameEMAIL"));
                emailObj.setImplementationJAR("");
                emailObj.setPassword(request.getParameter("_passwordEMAIL"));
                emailObj.setPort(_port);
                emailObj.setReserve1(request.getParameter("_reserve1EMAIL"));
                emailObj.setReserve2(request.getParameter("_reserve2EMAIL"));
                emailObj.setReserve3(request.getParameter("_reserve3EMAIL"));
                emailObj.setRetrycount(_retrycount);
                emailObj.setRetryduration(_retryduration);
                emailObj.setSmtpIp(request.getParameter("_ipEMAIL"));
                emailObj.setStatus(_status);
                emailObj.setUserId(request.getParameter("_userIdEMAIL"));
            } else {
                String ssl = request.getParameter("_sslEMAILS");
                boolean bSSLEnabled = false;
                if (ssl != null) {
                    if (ssl.equals("sslEnabled")) {
                        bSSLEnabled = true;
                    }
                }
                String _auth = request.getParameter("_authRequiredEMAILS");
                boolean bAuthRequired = false;
                if (_auth != null) {
                    if (_auth.equals("authEnabled")) {
                        bAuthRequired = true;
                    }
                }
                String emailPort = request.getParameter("_portEMAILS");
                String emailIP = request.getParameter("_ipEMAILS");
                String implclass = request.getParameter("_classNameEMAILS");
                for (int i = 0; i < emailPort.length(); i++) {
                    if (!Character.isDigit(emailPort.charAt(i))) {
                        digits = false;
                        break;
                    }
                }
                if (emailIP == null || emailPort == null || implclass == null || emailIP.isEmpty() || emailPort.isEmpty() || implclass.isEmpty()) {
                    result = "error";
                    message = "Fill all Details";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
                    } catch (Exception e) {
                        logger.error("Exception at EditEmailSettings ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                } else if (!digits) {
                    result = "error";
                    message = "Port should contain only digits";
                    try {
                        json.put("_result", result);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
                    } catch (Exception e) {
                        logger.error("Exception at EditEmailSettings ", e);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
                int _port = Integer.valueOf(request.getParameter("_portEMAILS")).intValue();
                int _retryduration = Integer.valueOf(request.getParameter("_retrydurationEMAILS")).intValue();
                int _retrycount = Integer.valueOf(request.getParameter("_retriesEMAILS")).intValue();
                int _status = Integer.valueOf(request.getParameter("_statusEMAILS")).intValue();
                emailObj.setAuthRequired(bAuthRequired);
                emailObj.setSsl(bSSLEnabled);
                emailObj.setPreference(_iPreference);
                emailObj.setAutofailover(0);
                emailObj.setClassName(request.getParameter("_classNameEMAILS"));
                emailObj.setFromEmail(request.getParameter("_fromEmailEMAILS"));
                emailObj.setFromName(request.getParameter("_fromNameEMAILS"));
                emailObj.setImplementationJAR("");
                emailObj.setPassword(request.getParameter("_passwordEMAILS"));
                emailObj.setPort(_port);
                emailObj.setReserve1(request.getParameter("_reserve1EMAILS"));
                emailObj.setReserve2(request.getParameter("_reserve2EMAILS"));
                emailObj.setReserve3(request.getParameter("_reserve3EMAILS"));
                emailObj.setRetrycount(_retrycount);
                emailObj.setRetryduration(_retryduration);
                emailObj.setSmtpIp(request.getParameter("_ipEMAILS"));
                emailObj.setStatus(_status);
                emailObj.setUserId(request.getParameter("_userIdEMAILS"));
            }
        }
//        AuditManagement audit = new AuditManagement();
        if (bAddSetting == true) {
            retValue = sMngmt.addSetting(sessionId, _type, _iPreference, emailObj);
            String resultString = "Failure";
            if (retValue == 0) {
                resultString = "Success";
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Add Email Setting", resultString, retValue, "Setting Management", "", " Auth Required=" + emailObj.isAuthRequired() + " Secure=" + emailObj.isSsl() + " Preference=" + emailObj.getPreference() + " AutoFailOver=" + emailObj.getAutofailover() + " Channel Id=" + emailObj.getChannelId() + " Class Name=" + emailObj.getClassName() + " From Email=" + emailObj.getFromEmail() + " From Name=" + emailObj.getFromName() + " Implementation Jar=" + emailObj.getImplementationJAR() + " Password=*****" + " Port=" + emailObj.getPort() + " Reserve1=" + emailObj.getReserve1() + " Reserve2=" + emailObj.getReserve2() + " Reserve3=" + emailObj.getReserve3() + " Retry Count=" + emailObj.getRetrycount() + " Retry Duration=" + emailObj.getRetryduration() + " SMTP Ip=" + emailObj.getSmtpIp() + " Status=" + emailObj.getStatus() + " User Id" + emailObj.getUserId(), itemtype, OperatorID);
            } else if (retValue != 0) {                
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Add Email Setting", resultString, retValue, "Setting Management", "", "Failed to add EMAIL Settings", itemtype, OperatorID);
            }
        } else {
            OOBEmailChannelSettings oldemailObj = (OOBEmailChannelSettings) sMngmt.getSetting(sessionId, _type, _iPreference);
            retValue = sMngmt.changeSetting(sessionId, _type, _iPreference, oldemailObj, emailObj);
            String resultString = "Failure";
            if (retValue == 0) {
                resultString = "Success";
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Change Email Setting", resultString, retValue, "Setting Management", " Auth Required=" + oldemailObj.isAuthRequired() + " Secure=" + oldemailObj.isSsl() + " Preference=" + oldemailObj.getPreference() + " AutoFailOver=" + oldemailObj.getAutofailover() + " Channel Id=" + oldemailObj.getChannelId() + " Class Name=" + oldemailObj.getClassName() + " From Email=" + oldemailObj.getFromEmail() + " From Name=" + oldemailObj.getFromName() + " Implementation Jar=" + oldemailObj.getImplementationJAR() + " Password=*****" + " Port=" + oldemailObj.getPort() + " Reserve1=" + oldemailObj.getReserve1() + " Reserve2=" + oldemailObj.getReserve2() + " Reserve3=" + oldemailObj.getReserve3() + " Retry Count=" + oldemailObj.getRetrycount() + " Retry Duration=" + oldemailObj.getRetryduration() + " SMTP Ip=" + oldemailObj.getSmtpIp() + " Status=" + oldemailObj.getStatus() + " User Id" + oldemailObj.getUserId(), " Auth Required=" + emailObj.isAuthRequired() + " Secure=" + emailObj.isSsl() + " Preference=" + emailObj.getPreference() + " AutoFailOver=" + emailObj.getAutofailover() + " Channel Id=" + emailObj.getChannelId() + " Class Name=" + emailObj.getClassName() + " From Email=" + emailObj.getFromEmail() + " From Name=" + emailObj.getFromName() + " Implementation Jar=" + emailObj.getImplementationJAR() + " Password=*****" + " Port=" + emailObj.getPort() + " Reserve1=" + emailObj.getReserve1() + " Reserve2=" + emailObj.getReserve2() + " Reserve3=" + emailObj.getReserve3() + " Retry Count=" + emailObj.getRetrycount() + " Retry Duration=" + emailObj.getRetryduration() + " SMTP Ip=" + emailObj.getSmtpIp() + " Status=" + emailObj.getStatus() + " User Id" + emailObj.getUserId(), itemtype, OperatorID);
            } else if (retValue != 0) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channel.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Change Email Setting", resultString, retValue, "Setting Management", " Auth Required=" + oldemailObj.isAuthRequired() + " Secure=" + oldemailObj.isSsl() + " Preference=" + oldemailObj.getPreference() + " AutoFailOver=" + oldemailObj.getAutofailover() + " Channel Id=" + oldemailObj.getChannelId() + " Class Name=" + oldemailObj.getClassName() + " From Email=" + oldemailObj.getFromEmail() + " From Name=" + oldemailObj.getFromName() + " Implementation Jar=" + oldemailObj.getImplementationJAR() + " Password=*****" + " Port=" + oldemailObj.getPort() + " Reserve1=" + oldemailObj.getReserve1() + " Reserve2=" + oldemailObj.getReserve2() + " Reserve3=" + oldemailObj.getReserve3() + " Retry Count=" + oldemailObj.getRetrycount() + " Retry Duration=" + oldemailObj.getRetryduration() + " SMTP Ip=" + oldemailObj.getSmtpIp() + " Status=" + oldemailObj.getStatus() + " User Id" + oldemailObj.getUserId(), "Failed TO Edit Setting", itemtype, OperatorID);
            }
        }
        if (retValue != 0) {
            result = "error";
            message = "Email Gateway Settings Update Failed";
        }
        try {
            json.put("_result", result);
            logger.debug("Response of EditEmailSettings Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of EditEmailSettings Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            logger.error("Exception at EditEmailSettings ", e);
        } finally {
            logger.info("Response of EditEmailSettings " + json.toString());
            logger.info("Response of EditEmailSettings Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
