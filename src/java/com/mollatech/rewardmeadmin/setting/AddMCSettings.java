/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.setting;

import com.mollatech.rewardme.nucleus.commons.MakerChecker;
import com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "AddMCSettings", urlPatterns = {"/AddMCSettings"})
public class AddMCSettings extends HttpServlet {

    static final Logger logger = Logger.getLogger(AddMCSettings.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is AddMCSettings at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String result = "", message = "";
        String status = request.getParameter("mStatus");
        String maker[] = request.getParameterValues("maker");
        String chaker[] = request.getParameterValues("checker");
        int _preference1 = SettingsManagement.PREFERENCE_ONE;
        int _type1 = SettingsManagement.MAKERCHAKER;
        try {
            SettingsManagement sMngmt = new SettingsManagement();
            Object settingsObj =  sMngmt.getSetting(sessionId, _type1, _preference1);
            MakerChecker config = null;
            boolean bAddSetting = false;
            if (maker == null || chaker == null) {
                result = "error";
                message = "Please Fill All Details.";
                json.put("result", result);
                logger.debug("Response of AddMCSettings Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of AddMCSettings Servlet's Parameter  message is " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            String makerS = "";
            String chakerS = "";
            for (int i = 0; i < maker.length; i++) {
                makerS += maker[i] + ",";
            }
            for (int i = 0; i < chaker.length; i++) {
                chakerS += chaker[i] + ",";
            }
            makerS = makerS.substring(0, makerS.length() - 1);
            chakerS = chakerS.substring(0, chakerS.length() - 1);
            for (int i = 0; i < chakerS.split(",").length; i++) {
                if (makerS.contains(chakerS.split(",")[i])) {
                    result = "error";
                    message = "Same Operator Can Not Become Maker And Chacker.";
                    json.put("result", result);
                    logger.debug("Response of AddMCSettings Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of AddMCSettings Servlet's Parameter  message is " + message);
                    out.print(json);
                    out.flush();
                    out.close();
                    return;
                }
            }
            if (settingsObj == null) {
                config = new MakerChecker();
                config.status = Integer.parseInt(status);
                config.maker = makerS;
                config.chaker = chakerS;
                bAddSetting = true;
            } else {
                config =  (MakerChecker) settingsObj;
                config.status = Integer.parseInt(status);
                config.maker = makerS;
                config.chaker = chakerS;
            }
            if (bAddSetting == true) {
                int retValue = sMngmt.addSetting(sessionId, _type1, _preference1, config);
                String resultString = "Failure";
                if (retValue == 0) {
                    resultString = "Success";
                    result = "success";
                    message = "Maker Checker Configuration Added Successfully.";
                } else {
                    result = "error";
                    message = "Error in Adding Maker Checker Configuration ";
                }
            } else {
                int retValue = sMngmt.changeSetting(sessionId, _type1, _preference1, settingsObj, settingsObj);
                String resultString = "Failure";
                if (retValue == 0) {
                    resultString = "Success";
                    result = "success";
                    message = "Maker Checker Configuration Updated Successfully.";
                } else {
                    result = "error";
                    message = "Error in Adding Maker Checker Configuration ";
                }
            }
            json.put("result", result);
            logger.debug("Response of AddMCSettings Servlet's Parameter  result is " + result);
            json.put("message", message);
            logger.debug("Response of AddMCSettings Servlet's Parameter  message is " + message);
            out.print(json);
            out.flush();
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
