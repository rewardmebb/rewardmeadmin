/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "unlockpassword", urlPatterns = {"/unlockpassword"})
public class unlockpassword extends HttpServlet {

    public static int ACTIVE_OPERATOR = 1;

    public static int SUSPENDED_OPERATOR = 0;

    public static int LOCKED_OPERATOR = -1;

    public static int REMOVED_OPERATOR = -99;

    final String itemTypeOp = "OPERATOR";

    static final Logger logger = Logger.getLogger(unlockpassword.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is unlockpassword at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String _operId = request.getParameter("_oprid");
        logger.debug("Value of _operId  = " + _operId);
        RmOperators operators = (RmOperators) request.getSession().getAttribute("_apOprDetail");

        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Unlock Password Successful";
        String _value = "0";
        int retValue = 0;
        OperatorsManagement oManagement = new OperatorsManagement();

        RmOperators oprObj = oManagement.getOperatorById(_operId);
        int iOprStatus = oprObj.getStatus();
        if (iOprStatus == LOCKED_OPERATOR) {
            retValue = oManagement.UnlockPassword(SessionId, _operId);
            String resultString = "Failure";
            if (retValue == 0) {
                resultString = "Success";
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Unlock Password", resultString, retValue, "Operator Management", "Locked Password", "Unlock Password", itemTypeOp, _operId);
            } else if (retValue != 0) {
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Unlock Password", resultString, retValue, "Operator Management", "Locked Password", "Failed to Unlocked Password", itemTypeOp, _operId);
            }
        } else {
            result = "error";
            message = "Password Is Not Locked";
        }
        if (retValue != 0) {
            result = "error";
            message = "Failed to Unlocked Password";
            _value = "0";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            logger.error("Exception at unlockpassword ", ex);
        } finally {
            logger.info("Response of unlockpassword " + json.toString());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
