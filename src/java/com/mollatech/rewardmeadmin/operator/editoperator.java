/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "editoperator", urlPatterns = {"/editoperator"})
public class editoperator extends HttpServlet {

    final String itemTypeOp = "OPERATOR";

    static final Logger logger = Logger.getLogger(editoperator.class);
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is editoperator at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        RmOperators operators = (RmOperators) request.getSession().getAttribute("_apOprDetail");

        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator Updated Successfully";
        JSONObject json = new JSONObject();
        int retValue = 0;
        int checkValue = 0;
        String _op_name = request.getParameter("_oprnameE");
        logger.debug("Value of _op_name = " + _op_name);
        String _op_phone = request.getParameter("_oprphoneE");
        logger.debug("Value of _op_phone = " + _op_phone);
        String _op_email = request.getParameter("_opremailE");
        logger.debug("Value of _op_email = " + _op_email);
        String _op_roleid_str = request.getParameter("_oprroleidE");
        logger.debug("Value of _op_roleid_str = " + _op_roleid_str);
        String _op_status_str = request.getParameter("_oprstatusE");
        logger.debug("Value of _op_status_str = " + _op_status_str);
        RmOperators operatorS = (RmOperators) request.getSession().getAttribute("_apOprDetail");
        int _op_roleid = Integer.parseInt(_op_roleid_str);
        int _op_status = Integer.parseInt(_op_status_str);
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_op_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_op_name);
        boolean b1 = m1.find();
        boolean spaces = false;
        if (_op_name != null) {
            for (int i = 0; i < _op_name.length(); i++) {
                if (Character.isWhitespace(_op_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        if (_op_name == null || _op_phone == null || _op_email == null || _op_name.length() == 0 || _op_phone.length() == 0 || _op_email.length() == 0 || _op_name.isEmpty() == true || _op_phone.isEmpty() == true || _op_email.isEmpty() == true) {
            result = "error";
            message = "Invalid Parameters";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                logger.error("Exception at editoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_name == null) {
            result = "error";
            message = "Name could not Empty ";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (spaces) {
            result = "error";
            message = "Name should not contain any blank spaces";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_name.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b1) {
            result = "error";
            message = "Name should not contain digits";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidEmail(_op_email) == false) {
            result = "error";
            message = "Invalid Email ID";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                logger.error("Exception at editoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_phone.length() < 10 || _op_phone.length() > 14) {
            result = "error";
            message = "Enter Valid mobile number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                logger.error("Exception at editoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidPhoneNumber(_op_phone) == false) {
            result = "error";
            message = "Invalid Mobile Number";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                logger.error("Exception at editoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        }
        String _operId = request.getParameter("_opridE");
        OperatorsManagement oManagement = new OperatorsManagement();
//        AuditManagement audit = new AuditManagement();
        RmOperators operator = oManagement.getOperatorById(_operId);
        Date d = new Date();

        //RmOperators newoperator = new RmOperators(operator.getOperatorid(), _op_name, _op_phone, _op_email, operator.getPasssword(), _op_roleid, _op_status, operator.getCurrentAttempts(), operator.getCreatedOn(), d, new Date(), operator.getChangePassword());
        //retValue = oManagement.EditOperator(sessionId, operator.getOperatorid(), operator, newoperator);
        String resultString = "Failure";
        if (retValue == -4) {
            result = "error";
            message = "Details Are Also Taken. Provide Unique Credentials.";
        } else {
            if (retValue == 0) {
                resultString = "Success";
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Edit Operator", resultString, retValue, "Operator Management", "Name=" + operator.getName() + ",Email =" + operator.getEmailid() + ",Phone =" + operator.getPhone(), "Name=" + newoperator.getName() + ",Email =" + newoperator.getEmailid() + ",Phone =" + newoperator.getPhone(), itemTypeOp, _operId);
            }
            if (retValue != 0) {
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operatorS.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operatorS.getName(), new Date(), "Edit Operator", resultString, retValue, "Operator Management", "Name=" + operator.getName() + ",Email =" + operator.getEmailid() + ",Phone =" + operator.getPhone(), "Failed To Edit Operator", itemTypeOp, _operId);
                result = "error";
                message = "Operator Update Failed";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            logger.error("Exception at editoperator ", e);
        } finally {
            logger.info("Response of editoperator " + json.toString());
            logger.info("Response of editoperator Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
