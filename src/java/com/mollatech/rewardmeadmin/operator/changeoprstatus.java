/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "changeoprstatus", urlPatterns = {"/changeoprstatus"})
public class changeoprstatus extends HttpServlet {

    static final Logger logger = Logger.getLogger(changeoprstatus.class);

    final int ACTIVE_STATUS = 1;

    final int SUSPEND_STATUS = 0;

    final int LOCKED_STATUS = -1;

    final int REMOVE_STATUS = -99;

    final String itemTypeOp = "OPERATOR";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        logger.info("Requested Servlet is changestatus at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        RmOperators operator = (RmOperators) request.getSession().getAttribute("_apOprDetail");
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");       
        String _op_status = request.getParameter("_op_status");
        logger.debug("Value of _op_status = " + _op_status);
        String _operId = request.getParameter("_oprid");
        logger.debug("Value of _oprid = " + _operId);

        String _old_op_status = request.getParameter("_op_status");
        logger.debug("Value of _op_status = " + _old_op_status);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Status Updated Successfully";
        int retValue = 0;
        int status = Integer.parseInt(_op_status);
        String _value = "Active";
        if (status == 0) {
            _value = "Suspended";
        }
        OperatorsManagement oManagement = new OperatorsManagement();

        RmOperators oldOpObj = oManagement.getOperatorById(_operId);
        if (oldOpObj.getRoleid() != operator.getRoleid()) {
            if (oldOpObj.getRoleid() == 1 && operator.getRoleid() != 1) {
                result = "error";
                message = "You Don't Have Privileges to Change Sysadmin Operator Status";
                try {
                    json.put("_result", result);
                    logger.debug("Response of changestatus Servlet's Parameter  result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of changestatus Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    logger.info("Response of changestatus " + json.toString());
                    logger.error("Exception at changestatus ", e);
                }
                out.print(json);
                out.flush();
                return;
            }
        }
        retValue = oManagement.ChangeStatus(sessionId, _operId, status);
        int istatus = oldOpObj.getStatus();
        String strStatus = "";
        if (istatus == ACTIVE_STATUS) {
            strStatus = "ACTIVE_STATUS";
        } else if (istatus == LOCKED_STATUS) {
            strStatus = "LOCKED_STATUS";
        } else if (istatus == REMOVE_STATUS) {
            strStatus = "REMOVE_STATUS";
        } else if (istatus == SUSPEND_STATUS) {
            strStatus = "SUSPEND_STATUS";
        }
        String strNewStatus = "";
        if (status == ACTIVE_STATUS) {
            strNewStatus = "ACTIVE_STATUS";
        } else if (status == LOCKED_STATUS) {
            strNewStatus = "LOCKED_STATUS";
        } else if (status == REMOVE_STATUS) {
            strNewStatus = "REMOVE_STATUS";
        } else if (status == SUSPEND_STATUS) {
            strNewStatus = "SUSPEND_STATUS";
        }
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
//            audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Change Status", resultString, retValue, "Operator Management", "Old Status=" + strStatus, "New Status =" + strNewStatus, itemTypeOp, _operId);
        }
        if (retValue != 0) {
//            audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Change Status", resultString, retValue, "Operator Management", "Old Status=" + strStatus, "Failed To Change Status", itemTypeOp, _operId);
            result = "error";
            message = "Status Update Failed";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            json.put("_value", _value);
        } catch (Exception ex) {
            logger.error("Exception at changestatus ", ex);
        } finally {
            logger.info("Response of changestatus Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
