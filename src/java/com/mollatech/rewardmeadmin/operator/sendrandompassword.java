/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "sendrandompassword", urlPatterns = {"/sendrandompassword"})
public class sendrandompassword extends HttpServlet {

    static final Logger logger = Logger.getLogger(sendrandompassword.class);

    final String itemTypeOp = "OPERATOR";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
        String _operId = request.getParameter("_oprid");
        String _sendOrNot = request.getParameter("_send");
        boolean bsendOrNot = Boolean.parseBoolean(_sendOrNot);
        RmOperators operator = (RmOperators) request.getSession().getAttribute("_apOprDetail");
        OperatorsManagement oManagement = new OperatorsManagement();
      
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Password Set sucessfully.";
        int retValue = 0;
        JSONObject json = new JSONObject();
        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(_operId + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;
        retValue = oManagement.SetPassword(sessionId, _operId, strPassword, bsendOrNot);
        String resultString = "Failure";
        if (retValue == 0) {
            resultString = "Success";
//            audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Set Password", resultString, retValue, "Operator Management", "Old Password =*****", "New Password =*****", itemTypeOp, _operId);
        } else if (retValue != 0) {
//            audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Set Password", resultString, retValue, "Operator Management", "Old Password =*****", "Failed To Set Password .!", itemTypeOp, _operId);
        }
        if (retValue == 0) {
            result = "success";
            message = "Password Set sucessfully.";
        } else {
            result = "error";
            message = "Password Not Set Successfully";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                logger.error("Exception at getopraudits ", e);
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            logger.error("Exception at sendrandompassword ", e);
        } finally {
            logger.info("Response of sendrandompassword " + json.toString());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
