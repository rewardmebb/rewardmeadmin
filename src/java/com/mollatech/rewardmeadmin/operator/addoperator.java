/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.commons.UtilityFunctions;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.RmRoles;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.rewardme.nucleus.db.operation.REWARDMEOperator;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

@WebServlet(name = "addoperator", urlPatterns = {"/addoperator"})
public class addoperator extends HttpServlet {

    static final Logger logger = Logger.getLogger(addoperator.class);

    final int SYS_ADMIN = 1;

    final int ADMIN = 2;

    final int HELPDESK = 3;

    final int REPORTER = 4;

//    final int ACTIVE_STATUS = 1;
//
//    final int SUSPEND_STATUS = 0;
//
//    final int LOCKED_STATUS = -1;
//
//    final int REMOVE_STATUS = -99;
    final String itemTypeOp = "OPERATOR";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is addoperator at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        RmOperators operators = (RmOperators) request.getSession().getAttribute("_apOprDetail");

        String sessionId = (String) request.getSession().getAttribute("_apSessionID");
        response.setContentType("application/json");
        RmOperators operator = (RmOperators) request.getSession().getAttribute("_apOprDetail");
        String operatorId = operator.getOperatorid();
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Operator Added Successfully";
        int retValue = 0;
        String _op_name = request.getParameter("_oprname");
        logger.debug("Value of _oprname  = " + _op_name);
        String _op_phone = request.getParameter("_oprphone");
        logger.debug("Value of _oprphone  = " + _op_phone);
        String _op_email = request.getParameter("_opremail");
        logger.debug("Value of _opremail  = " + _op_email);
        String _op_role = request.getParameter("_oprrole");
        logger.debug("Value of _oprrole  = " + _op_role);
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(_op_name);
        boolean b = m.find();
        Pattern p1 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_op_name);
        boolean b1 = m1.find();
        boolean spaces = false;
        if (_op_name != null) {
            for (int i = 0; i < _op_name.length(); i++) {
                if (Character.isWhitespace(_op_name.charAt(i))) {
                    spaces = true;
                }
            }
        }
        if (_op_name == null || _op_phone == null || _op_email == null || _op_name.isEmpty() == true || _op_phone.isEmpty() == true || _op_email.isEmpty() == true) {
            result = "error";
            message = "Fill all Details";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                logger.error("Exception at addoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_name == null) {
            result = "error";
            message = "Name could not Empty ";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (spaces) {
            result = "error";
            message = "Name should not contain any blank spaces";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_name.length() < 3) {
            result = "error";
            message = "Name should be more than 3 characters";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b) {
            result = "error";
            message = "Name should not contain any symbols";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (b1) {
            result = "error";
            message = "Name should not contain digits";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (UtilityFunctions.isValidEmail(_op_email) == false) {
            result = "error";
            message = "Enter Valid Email address";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (_op_phone.length() < 8 || _op_phone.length() > 14) {
            result = "error";
            message = "Enter Valid mobile number";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        } else if (UtilityFunctions.isValidPhoneNumber(_op_phone) == false) {
            result = "error";
            message = "Enter Valid mobile number";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            }
            out.print(json);
            out.flush();
            return;
        }
        OperatorsManagement om = new OperatorsManagement();
        REWARDMEOperator[] ops = om.ListOperatorsInternal();
        int iCurrentOperatorCount = ops.length;
        int iOperatorCount = AxiomProtect.GetOperatorsAllowed();
        if (UtilityFunctions.isValidEmail(_op_email) == false) {
            result = "error";
            message = "Invalid Email ID";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                logger.error("Exception at addoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        }
        if (UtilityFunctions.isValidPhoneNumber(_op_phone) == false) {
            result = "error";
            message = "Invalid Mobile Number";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                logger.error("Exception at addoperator ", e);
            }
            out.print(json);
            out.flush();
            return;
        }
        UtilityFunctions u = new UtilityFunctions();
        Date dDated = new Date();
        String _op_password = u.HexSHA1(_op_name + _op_phone + _op_email + dDated.toString());
        _op_password = _op_password.substring(0, 8);
        String roleNames = null;
        //int _op_status = 1;
        int _op_status = GlobalStatus.ACTIVE;
        int checkValue = 0;
        int _op_roleID = Integer.valueOf(_op_role);
        if (_op_roleID == ADMIN) {
            roleNames = "ADMIN";
        } else if (_op_roleID == HELPDESK) {
            roleNames = "HELPDESK";
        } else if (_op_roleID == REPORTER) {
            roleNames = "REPORTER";
        } else if (_op_roleID == SYS_ADMIN) {
            roleNames = "SYS_ADMIN";
        }
        OperatorsManagement oManagement = new OperatorsManagement();
        //RmRoles roleObj = oManagement.getRoleByRoleId(operator.getRoleid());
//        if (roleObj.getName().equals(OperatorsManagement.admin) && roleNames.equals("SYS_ADMIN")) {
//            result = "error";
//            message = "Admin Don't Have Permissions to Create Sysadmin";
//            try {
//                json.put("_result", result);
//                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
//                json.put("_message", message);
//                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
//            } catch (Exception e) {
//                logger.error("Exception at addoperator ", e);
//            } finally {
//                logger.info("Response of addoperator " + json.toString());
//                out.print(json);
//                out.flush();
//                return;
//            }
//        }
        checkValue = oManagement.checkIsUniqueInChannel( _op_name, _op_email, _op_phone);
        if (checkValue == 0) {
            retValue = oManagement.AddOperator(sessionId,  _op_name, _op_password, _op_phone, _op_email, _op_roleID, GlobalStatus.ACTIVE);
            if (retValue == 0) {
                REWARDMEOperator op = oManagement.GetOperatorByName(sessionId,  _op_name);
            }
//            AuditManagement audit = new AuditManagement();
            String strRole = "";
            if (_op_roleID == ADMIN) {
                strRole = "ADMIN";
            } else if (_op_roleID == HELPDESK) {
                strRole = "HELPDESK";
            } else if (_op_roleID == REPORTER) {
                strRole = "REPORTER";
            } else if (_op_roleID == SYS_ADMIN) {
                strRole = "SYS_ADMIN";
            }
            String strStatus = "";
            if (_op_status == GlobalStatus.ACTIVE) {
                strStatus = "ACTIVE_STATUS";
            } else if (_op_status == GlobalStatus.LOCKED) {
                strStatus = "LOCKED_STATUS";
            } else if (_op_status == GlobalStatus.DELETED) {
                strStatus = "REMOVE_STATUS";
            } else if (_op_status == GlobalStatus.SUSPEND) {
                strStatus = "SUSPEND_STATUS";
            }
            String resultString = "Failure";
            String remoteaccesslogin = (String) request.getSession().getAttribute("_apSessionRemoteAccessLoginID");
            if (retValue == 0) {
                resultString = "Success";
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), channels.getName(), remoteaccesslogin, operator.getName(), new Date(), "Add Operator", resultString, retValue, "Operator Management", "", "Name=" + _op_name + ",Password=*****" + ",Phone" + _op_phone + ",Email=" + _op_email + ",Role=" + strRole + ",Status=" + strStatus, itemTypeOp, operatorId);
            }
            if (retValue != 0) {
//                audit.AddAuditTrail(sessionId, channels.getChannelid(), operator.getOperatorid(), request.getRemoteAddr(), remoteaccesslogin, channels.getName(), operator.getName(), new Date(), "Add Operator", resultString, retValue, "Operator Management", "", "Failed To add Operator", itemTypeOp, operatorId);
                result = "error";
                message = "Adding Operator Failed";
            }
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of addoperator Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at addoperator ", ex);
            } finally {
                logger.info("Response of addoperator " + json.toString());
                out.print(json);
                out.flush();
            }
        } else {
            result = "error";
            message = "Provide Unique Credentials.";
            try {
                json.put("_result", result);
                logger.debug("Response of addoperator Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of ChangeRole Servlet's Parameter  message is " + message);
            } catch (Exception e) {
                logger.error("Exception at addoperator ", e);
            } finally {
                logger.info("Response of addoperator " + json.toString());
                logger.info("Response of ResetPasswordAfterConfirmDetails Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
