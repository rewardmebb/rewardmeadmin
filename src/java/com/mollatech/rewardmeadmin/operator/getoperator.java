/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardmeadmin.operator;

import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.OperatorUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.hibernate.Session;
/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "getoperator", urlPatterns = {"/getoperator"})
public class getoperator extends HttpServlet {

    static final Logger logger = Logger.getLogger(getoperator.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        String _oprid = request.getParameter("_oprid");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        RmOperators operators = (RmOperators) request.getSession().getAttribute("_apOprDetail");
        try {
            SessionFactoryUtil suOpr = new SessionFactoryUtil(SessionFactoryUtil.operators);
            Session sOpr = suOpr.openSession();
            OperatorUtils oprUtil = new OperatorUtils(suOpr, sOpr);
            RmOperators oprObj = oprUtil.GetOperatorsById(_oprid);
            sOpr.close();
            suOpr.close();
            json.put("_id", oprObj.getOperatorid());
            json.put("_name", oprObj.getName());
            json.put("_email", oprObj.getEmailid());
            json.put("_phone", oprObj.getPhone());
            json.put("_roleid", oprObj.getRoleid());
            json.put("_status", oprObj.getStatus());
            json.put("_result", "success");
        }catch (Exception ex) {
            logger.error("Exception at getoperator ", ex);
            try {
                json.put("_result", "error");
                json.put("_message", ex.getMessage());
            } catch (Exception e) {
                logger.error("Exception at getoperator ", e);
            }
        }finally {
            logger.info("Response of getoperator " + json.toString());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
