<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="java.io.ObjectInputStream"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@include file="header.jsp" %>
<script src="js/partnerRequest.js" type="text/javascript"></script>
<script src="./js/profile.js"></script>
<%
    String _OwnerId = request.getParameter("_partnerId");
    int OwnerId = Integer.parseInt(_OwnerId);
    RmBrandownerdetails sglist = new BrandOwnerManagement().getBrandOwnerDetails(OwnerId);
    String status = null;
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">Information of <%=sglist.getBrandName()%></h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><a href="partners.jsp"> List of Brand Owner's</a>&#47; <i class="glyphicon glyphicon-list"></i><b> Brand Information</b>
                    </div>
                    <div class="panel-body">
                        <%if (sglist != null) {%>
                        <form class="form-validate form-horizontal" id="update-profile" method="POST" action="#">                    
                            <div class="row" style="margin-left: 10px;">
                                <label >
                                    <font style=" font-weight: bold; font-size: 25px" class="text-primary"><u>Brand Details</u></font>
                                </label>                                                               
                            </div>                                    
                            <br>                    
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Brand Name</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getBrandName()%></label>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Phone Number</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getPhone()%></label>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Email Id </label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl"class="control-label " >:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getEmail()%></label>
                                </div>
                            </div>

                            <hr>
                            <div class="row" style="margin-left: 10px;">
                                <label >
                                    <font style=" font-weight: bold; font-size: 25px" class="text-primary"><u>Company Details</u></font>
                                </label>                                                               
                            </div> <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Company Name </label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl"class="control-label " >:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getCompanyName()%></label>
                                </div> 
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Registration Number</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getRegistrationNumber()%></label>
                                </div>
                            </div>
                            <br>   
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Company Address1</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getComapanyAddrLine1()%></label>
                                </div>

                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Company Address2 </label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getComapanyAddrLine2()%></label>
                                </div>
                            </div>                                      
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Company Address3</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getComapanyAddrLine3()%></label>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">City</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getCity()%></label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;State&nbsp;&nbsp;</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl"class="control-label " >:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getCompanyState()%></label>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2 ">Country </label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getCountry()%></label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Pin Code</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label " >:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getPincode()%></label>
                                </div> 

                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;Landline Number&nbsp;&nbsp;</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sglist.getLandlineNumber()%></label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;Status&nbsp;&nbsp;</label>
                                </font>
                                <%
                                    if (sglist.getStatus() == GlobalStatus.ACTIVE) {
                                        status = "Active";
                                    } else {
                                        status = "Suspend";
                                    }
                                    String stripeSubscriptionId = "NA";
                                    if(sglist.getStripeSubscriptionId() != null){
                                        stripeSubscriptionId = sglist.getStripeSubscriptionId();
                                    }
                                    String stripeCustomeId = "NA";
                                    if(sglist.getStripeCustomerId() != null){
                                        stripeCustomeId = sglist.getStripeCustomerId();
                                    }
                                %>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=status%></label>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="margin-left: 10px;">
                                <label >
                                    <font style=" font-weight: bold; font-size: 25px" class="text-primary"><u>Subscription Details</u></font>
                                </label>                                                               
                            </div> 
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Stripe SubscriptionId </label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=stripeSubscriptionId%></label>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;Stripe CustomerId&nbsp;&nbsp;</label>
                                </font>
                                <div class="col-lg-4">
                                    <label for="curl" class="control-label ">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=stripeCustomeId%></label>
                                </div>
                            </div> 
                        </form>
                        <br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="button" class="btn btn-success btn-md" value="Back" onclick="newDoc()">
                        <br>
                        <%}%>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function newDoc() {
        window.location.assign("partners.jsp");
    }
</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
</body>
</html>
<%@include file="footer.jsp" %>
