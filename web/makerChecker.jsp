<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement"%>
<%@include file="header.jsp" %>
<link href="select2/select2.css" rel="stylesheet" type="text/css"/>
<%    RmOperators[] operatorses = new OperatorsManagement().getAllOperators();
    String options = "";
    if (operatorses != null) {
        for (int i = 0; i < operatorses.length; i++) {
            if (operatorses[i].getStatus() == 1) {
                options += "<option value='" + operatorses[i].getEmailid() + "'>" + operatorses[i].getName() + "</option>\n";
            }
        }
    }
%>
<div id="page-wrapper"> 

    <div class="row">
        <div class="col-lg-12"><div id = "alert_placeholder"></div>
            <h1 class="page-header">Maker Checker Setting</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;                
                <i class="fa fa-edit"></i> Maker Checker
            </div>
            <div class="panel-body">
                <h4>Maker Checker</h4>
                <hr>                   
                <form class="form-horizontal" id="addMCForm" name="addMCForm" role="form" method="POST">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Status</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <select class="form-control" id="mStatus" name="mStatus">
                                        <option value="">Select</option>
                                        <option value="<%=GlobalStatus.ACTIVE%>">Active</option>
                                        <option value="<%=GlobalStatus.SUSPEND%>">Suspend</option>
                                    </select>                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Maker(s)</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select id ="maker" name="maker" multiple class="span6" style="width: 86%">
                                        <%=options%>  
                                    </select>                                  
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Checker(s)</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select id ="checker" name="checker" multiple class="span6" style="width: 86%">
                                        <%=options%>
                                    </select>                                  
                                </div>
                                <br>
                                <a  class="btn btn-success btn-xs" onclick="addMCFormData()"><i class="fa fa-plus-circle"></i> Save package</a>
                            </div>

                        </div> 

                    </div> 

                </form>
            </div>
        </div>
    </div>
</div>  
<%@include file="footer.jsp" %>
<script src="select2/select2.js" type="text/javascript"></script>
<script src="js/makerchaker.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("#maker").select2();
        $("#checker").select2();

    });
    loadMCSetting();
</script>      

