<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@include file="header.jsp" %>
<link href="select2/select2.css" rel="stylesheet" type="text/css"/>
<script src="js/packageOperation.js" type="text/javascript"></script>
<%    
    
    String packageId = request.getParameter("_edit");
    int pId = 0;
    if (packageId != null) {
        pId = Integer.parseInt(packageId);
    }
    RmUnapprovalpackagedetails packageObj = new UnapprovalPackageManagement().getPackageDetails(SessionId, pId);
    String partnerIds = packageObj.getBrandOwnerVisibility();
    String creditDeduction = packageObj.getCreditDeductionConfiguration();
    boolean flag = false;
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all,")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            RmBrandownerdetails parObj = new BrandOwnerManagement().getBrandOwnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getEmail();
            }
            options += "<option selected value='" + parId + "'>" + emailId + "</option>\n";
        }
        flag = true;
    }
    RmBrandownerdetails[] partnerObj = new BrandOwnerManagement().getAllBrandOwnerDetails(SessionId);
    if (partnerObj != null) {
        if (flag) {
            options += "<option value=all>ALL</option>\n";
        }
        for (int i = 0; i < partnerObj.length; i++) {
            if (partnerIds != null && partnerIds.contains(String.valueOf(partnerObj[i].getOwnerId()))) {
                continue;
            }
            if (partnerObj[i].getStatus() == 1) {
                options += "<option value='" + partnerObj[i].getOwnerId() + "'>" + partnerObj[i].getEmail()+ "</option>\n";
            }
        }
    }
    JSONObject jsonObj = new JSONObject(creditDeduction);
    String perUserCredit = null;
    String perCampaignCredit = null;
    if (jsonObj.has("userSearched")) {
        perUserCredit = jsonObj.getString("userSearched");
    }
    if (jsonObj.has("perCampaign")) {
        perCampaignCredit = jsonObj.getString("perCampaign");
    }
%>
<div id="wrapper">
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Edit " <%=packageObj.getPackageName()%> " package details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                <i class="fa fa-shopping-cart"></i><a href="package.jsp"> Package Details</a> &#47;
                <i class="fa fa-edit"></i> Edit Package Details
            </div>
            <div class="panel-body">
                <h4>Package details</h4>
                <hr>                   
                <form class="form-horizontal" id="addMCForm" name="addMCForm" role="form" method="POST">
                    <div class="form-group">
                        <label class="control-label col-lg-2">Name</label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                                <%if (packageObj != null) {%>
                                <input type="text" id="_packageName" name="_packageName" class="form-control" value="<%=packageObj.getPackageName()%>" disabled placeholder="Package name">
                                <%}else{%>
                                <input type="text" id="_packageName" name="_packageName" class="form-control" placeholder="Package name">
                                <%}%>
                            </div>
                            <div id="checkAvailability-result">
                            </div>
                        </div>
                        <label class="control-label col-lg-2">Price</label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                
                                <%if (packageObj != null) {%>
                                <input type="text" id="_packagePrice" name="_packagePrice" class="form-control" placeholder="Price" value="<%=packageObj.getPlanAmount()%>" onkeypress="return isNumericKey(event)">                                    
                                <%} else {%>
                                <input type="text" class="form-control" placeholder="Price">                                    
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Package duration</label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                 <%if (packageObj != null) {%>
                                <select class="form-control" disabled>
                                    <option value="<%=packageObj.getPackageDuration()%>"><%=packageObj.getPackageDuration()%></option>
                                </select>
                                <%} else {%>
                                <select class="form-control">
                                    <option value="">Select Duration</option>
                                    <option >Daily</option>
                                    <option>Weekly</option>
                                    <option>Monthly</option>
                                    <option>Quarterly</option>
                                    <option>Half yearly</option>
                                    <option>Yearly</option>
                                </select>                                     
                                <%}%>                                    
                            </div>
                        </div>
                        <label class="control-label col-lg-2">Payment mode</label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    <%if (packageObj != null) {%>
                                    <select class="form-control" disabled>
                                        <option value="<%=packageObj.getPaymentMode()%>"><%=packageObj.getPaymentMode()%></option>
                                    </select>
                                    <%} else {%>
                                    <select class="form-control">
                                        <option>Select Payment type</option>
                                        <option>Prepaid</option>
                                        <option>PostPaid</option>
                                    </select>
                                    <%}%>       
                            </div>
                        </div>
                        <br><br>  <br>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Recurring billing Id</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <%if (packageObj != null) {%>
                                        <input type="text" id="_recurrienceBillingID" name="_recurrienceBillingID" value="<%=packageObj.getRecurrenceBillingPlanId()%>" class="form-control" placeholder="Recurrence Billing Id">                                   
                                        <%}else{%>
                                        <input type="text" id="_recurrienceBillingID" name="_recurrienceBillingID" class="form-control" placeholder="Recurrence Billing Id">                                   
                                        <%}%>
                                    </div>
                                </div>
                                <!--                            </div>
                                                            <div class="form-group">-->
                                <label class="control-label col-lg-2">Credit</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <%if (packageObj != null) {%>
                                        <input type="text" id="_credit" name="_credit" class="form-control" placeholder="Credit" value="<%=packageObj.getMainCredits()%>" onkeypress="return isNumericKey(event)">                                    
                                        <%}else{%>
                                        <input type="text" id="_credit" name="_credit" class="form-control" placeholder="Credit" onkeypress="return isNumericKey(event)">                                    
                                        <%}%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Credit Per Campaign</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <%if(perCampaignCredit != null){%>
                                        <input type="text" id="_Per_Campaign" name="_Per_Campaign" class="form-control" value=<%=perCampaignCredit%> placeholder="Credit Per Campaign">                                    
                                        <%}else{%>
                                        <input type="text" id="_Per_Campaign" name="_Per_Campaign" class="form-control" placeholder="Credit Per Campaign">                                    
                                        <%}%>
                                    </div>
                                </div>
                                <!--</div>-->
                                <!--&nbsp;&nbsp;&nbsp;-->
                                <!--<div class="form-group">-->
                                <label class="control-label col-lg-2">Credit Per User Searched</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <%if(perUserCredit != null){%>
                                        <input type="text" id="_userSearched" name="_userSearched" class="form-control" value=<%=perUserCredit%> placeholder="Credit Per User Searched" onkeypress="">                                    
                                        <%}else{%>
                                        <input type="text" id="_userSearched" name="_userSearched" class="form-control" placeholder="Credit Per User Searched" onkeypress="">                                    
                                        <%}%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Tax</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">%</span>
                                        <%if(packageObj != null){%>
                                        <input type="text" id="_tax" name="_tax" class="form-control" placeholder="Tax" value=<%=packageObj.getTax()%> onkeypress="return isNumericKey(event)">                                    
                                        <%}else{%>
                                        <input type="text" id="_tax" name="_tax" class="form-control" placeholder="Tax" onkeypress="return isNumericKey(event)">                                    
                                        <%}%>
                                    </div>
                                </div>
                            </div>                            
                        </div>  
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Brand Owner Visibility</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                        <select id ="visibleTo" name="visibleTo" multiple class="span6" style="width: 100%">
                                            <%=options%>
                                        </select>                                  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Package Description</label>
                                <div class="col-lg-8">
                                    <div class="input-group">                                        
                                         <%if(packageObj != null && packageObj.getPackageDescription() != null){%>
                                         <textarea id="packageDesc" name="packageDesc" rows="4" cols="190" class="form-control"><%=packageObj.getPackageDescription()%></textarea>                               
                                         <%}else{%>
                                         <textarea id="packageDesc" name="packageDesc" rows="4" cols="190" class="form-control"></textarea>                               
                                         <%}%>
                                    </div>
                                    <br>
                                    <a  class="btn btn-success btn-xs" onclick="editPackage(<%=packageObj.getPckId()%>)"><i class="fa fa-plus-circle"></i> Save Package</a>

                                </div>
                            </div>

                        </div>  
                    </div>
                </form>

            </div>
        </div>
    </div>
    </div>
</div>
</div>
<%@include file="footer.jsp" %>
<script>

    function showAlert(message, type, closeDelay) {
        if ($("#alerts-container").length == 0) {
            // alerts-container does not exist, create it
            $("body")
                    .append($('<div id="container" style="' +
                            'width: %; margin-left: 65%; margin-top: 10%;">'));
        }
        // default to alert-info; other options include success, warning, danger
        type = type || "info";

        // create the alert div
        var alert = $('<div class="alert alert-' + type + ' fade in">')
                .append(
                        $('<button type="button" class="close" data-dismiss="alert">')
                        .append("&times;")
                        )
                .append(message);

        // add the alert div to top of alerts-container, use append() to add to bottom
        $("#alerts-container").prepend(alert);

        // if closeDelay was passed - set a timeout to close the alert
        if (closeDelay)
            window.setTimeout(function () {
                alert.alert("close")
            }, closeDelay);

    }
</script>
<script src="select2/select2.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("#visibleTo").select2();

    });
</script>
