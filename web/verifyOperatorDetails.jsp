<%@page import="java.util.Date"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
    <head>
        <script src="js/utilityFunction.js" type="text/javascript"></script>
        <script src="js/utilityFunction.js" type="text/javascript"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RewardMe Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.css" rel="stylesheet" type="text/css"/>
        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet" type="text/css"/>
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
        <!-- Morris Charts CSS -->
        <link href="bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css"/>
        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery.js"></script>

        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js"></script> 
        <!--        <script src="./js/operators.js"></script>-->
        <script src="./js/operatorsOLD2.js"></script>
        <script src="./js/modal.js"></script>
    </head>
    <%
        String username = request.getParameter("username");
        username = new String(Base64.decode(username));
        String email = request.getParameter("email");
        String expiryTime = request.getParameter("expiryTime");
        expiryTime = new String(Base64.decode(expiryTime));
        String userId = request.getParameter("ref");
        userId = new String(Base64.decode(userId));
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
        Date expiry = formatter.parse(expiryTime);
        if (expiry.before(new Date())) {
            response.sendRedirect("linkExpire.jsp");
            return;
        }
    %>
    <body>
        <div class="container">
            <div id = "alert_placeholder"></div>
            <div class="login-panel-show">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b> ADMIN LOGIN</b>
                        </div>
                        <div class="panel-body">
                            <form id="forgetiPasswordForm">
                                <div class="form-group">
                                    <label class="control-label" for="email">Username</label>
                                    <input type="text" placeholder="User name" title="Please enter you user name" required="" value="<%=username%>" name="fpUsername" id="fpUsername" class="form-control" disabled>

                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="email">Email Id</label>
                                    <input type="text" placeholder="abc@noemail.com" title="Please enter you email id" required="" value="<%=email%>" name="fpEmailID" id="fpEmailID" class="form-control" disabled>

                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="email">New Password</label>
                                    <input type="password" placeholder="New Password"  value="" name="newPassword" id="newPassword" class="form-control">

                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="email">Confirm </label>
                                    <input type="password" placeholder="Confirm your new password" value="" name="confirmPassword" id="confirmPassword" class="form-control">

                                </div>
                                <button type="button" class="btn btn-success ladda-button btn-block" data-style="zoom-in" id="forgetPasswordButton" onclick="forgetPassword('<%=userId%>', '<%=username%>', '<%=email%>')"> Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        New ly added-->

        <%
            java.util.Date dFooter = new java.util.Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);
            SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            String completeTimewithLocalTZ = tz.format(dFooter);
            long LongTime = dFooter.getTime() / 1000;

        %>
        <footer>
            <div align="center" style="margin-top: 10%">
                <p>&copy; Molla Technologies 2009-<%=strYYYY%> <a href="http://www.mollatech.com" target="_blank"> (www.mollatech.com)</a></p>
                <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
            </div>
        </footer>
    </body>
</html>