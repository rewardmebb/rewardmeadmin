<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails"%>
<%@include file="header.jsp" %>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <%  
        
        int totalpartner = 0;        
    %>
    <!-- /.row -->
    <div class="row">
        <%            
            RmBrandownerdetails[] p = new BrandOwnerManagement().getAllBrandOwnerDetails(SessionId);
            if (p != null) {
                totalpartner = p.length;
            }
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= totalpartner%></div>
                            <div>Total BrandOwner's</div>
                        </div>
                    </div>
                </div>
                <a href="partners.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <%
            Object setting1 = new SettingsManagement().getSetting(channelId, SettingsManagement.MAKERCHAKER, SettingsManagement.PREFERENCE_ONE);
            MakerChecker makerChakerObj1 = (MakerChecker) setting1;
            if (makerChakerObj1 != null) {
                if (makerChakerObj1.chaker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
                    int newPackageRequestCount = 0;
                    RmApprovedpackagedetails[] sgRequestedPackage = null;
                    sgRequestedPackage = new ApprovedPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
                    if (sgRequestedPackage != null) {
                        newPackageRequestCount = sgRequestedPackage.length;
                    }                                                            
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%=newPackageRequestCount%></div>
                            <div>Package Request</div>
                        </div>
                    </div>
                </div>
                <a href="packageRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
                            <%}}%>
    </div>
                            
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
<!-- /#wrapper -->



</body>

</html>
<%@include file="footer.jsp" %>
