<%@page import="com.mollatech.rewardme.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@include file="header.jsp" %>
<script src="js/packageOperation.js" type="text/javascript"></script>
<%
    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";
    Enumeration enuKeys = LoadSettings.g_packageRejectionSettings.keys();
    while (enuKeys.hasMoreElements()) {
        String key = (String) enuKeys.nextElement();
        String value = LoadSettings.g_packageRejectionSettings.getProperty(key);
        options += "<option value='" + value + "'>" + value + "</option>\n";
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Package Request</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i> Package Request &#47;                                                          
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Package</th>
                                <th>Price</th>
                                <th>Duration</th>                                
                                <th>Manage</th>
                                <th>Requested On</th>
                                <th>Action On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;
                                String resultInfo = "No Record Found";
                                String updationDate = "Not Updated yet";
                                ApprovedPackageManagement um = new ApprovedPackageManagement();
                                RmApprovedpackagedetails[] rDetails = null;
                                RmUnapprovalpackagedetails details = null;
                                int pId = -1;
                                rDetails = um.listpackage(SessionId);
                                if (rDetails != null) {
                                    if (rDetails.length == 0) {
                                        rDetails = null;
                                    }
                                }
                                if (rDetails != null) {
                                    for (int i = 0; i < rDetails.length; i++) {
                                       
                                        String userStatus = "user-status-value-" + i;
                                        details = new UnapprovalPackageManagement().getPackageByName(SessionId, rDetails[i].getPackageName());
                                        if (details != null && details.getStatus() != GlobalStatus.REJECTED) {
                                            count++;
                            %>
                            <tr style="text-align: center">
                                <td/>
                                <td><%=count%></td>
                                <%if (rDetails[i].getStatus() == UnapprovalPackageManagement.SUSPEND_STATUS || rDetails[i].getStatus() == UnapprovalPackageManagement.UPDATED_REQUEST_STATUS || rDetails[i].getStatus() == GlobalStatus.ACTIVE) {%>
                                <td>
                                    <a href="#" class="btn btn-warning btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is pending for approval"><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (rDetails[i].getStatus() == GlobalStatus.REJECTED) { %>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is rejected" ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <%} else {%>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is approved"><i class="fa fa-thumbs-o-up"></i></a>
                                </td>
                                <%}%>
                                <td><%=rDetails[i].getPackageName()%></td>                                
                                <td><%=rDetails[i].getPlanAmount()%></td>
                                <td><%=rDetails[i].getPackageDuration()%></td>
<!--                                <td><%=rDetails[i].getPaymentMode()%></td>-->
                                <td>                                                                                                                                                                         
                                    <div class="btn btn-group">
                                        <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%if (rDetails[i].getStatus() == GlobalStatus.APPROVED) {%> <font style="font-size: 10px;">Approved</font> <%} else if (rDetails[i].getStatus() == GlobalStatus.REJECTED) {%><font style="font-size: 10px;"> Rejected</font> <%} else {%> <font style="font-size: 10px;"> New</font> <%}%><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <%
                                                if (rDetails[i].getStatus() != GlobalStatus.APPROVED) {
                                            %>
                                            <li><a href="#" onclick="changePackageStatus('<%=GlobalStatus.APPROVED%>', '<%=rDetails[i].getPackageName()%>')" >Approve?</a></li>
                                            <li><a href="#" onclick="rejectPackageModal('<%=GlobalStatus.REJECTED%>', '<%=rDetails[i].getPackageName()%>')" >Reject?</a></li>                                                               
                                            <li><a class="divider"></a></li>
                                                <%}%>
                                                <%
                                                    if (rDetails[i].getStatus() == (GlobalStatus.ACTIVE) || rDetails[i].getStatus() == (GlobalStatus.APPROVED)) {
                                                %>
                                            <li><a href="showPackage.jsp?_edit=<%=rDetails[i].getPackageName()%>">View Details</a></li>
                                                <%
                                                } else if (rDetails[i].getStatus() == GlobalStatus.SENDTO_CHECKER) {
                                                %>
                                            <li><a href="oldDetails.jsp?_edit=<%=rDetails[i].getPackageName()%>" target="_blank">Old Details</a></li>
                                            <li><a href="newDetails.jsp?_edit=<%=rDetails[i].getPackageName()%>"  target="_blank">New Details</a></li>
                                                <%
                                                    }
                                                %>
                                        </ul>
                                    </div>

                                </td>
                                <td><%=UtilityFunctions.getTMReqDate(rDetails[i].getCreationDate())%></td>
                                <%if (rDetails[i].getUpdationDate() == null) {%>
                                <td><%=updationDate%></td>
                                <%} else {%>
                                <td><%=UtilityFunctions.getTMReqDate(rDetails[i].getUpdationDate())%></td>
                                <%}%>
                            </tr>
                            <%}
                                }
                            }%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="rejectPackage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <b id="rejectPackageName">Reject package</b>
                    </div>          
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="rejectPackageForm">
                                <fieldset>
                                    <div class="control-group">
                                        <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                        <div class="controls col-lg-10">
                                            <input type="hidden"  id="_packageName" name="_packageName" >
                                            <input type="hidden"  id="_packagestatus" name="_packagestatus" >
                                            <select id="reason" name="reason" class="form-control" >
                                                <%=options%>        
                                            </select>
                                        </div>
                                    </div>                            
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="edit-partner-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                        <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="bower_components/jquery/dist/jquery.min.js"></script>
 Bootstrap Core JavaScript 
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 Metis Menu Plugin JavaScript 
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
 Custom Theme JavaScript 
<script src="dist/js/sb-admin-2.js"></script>-->
<%@include file="footer.jsp"%>
<!--<script src="js/bootbox.min.js" type="text/javascript"></script>-->
<script src="js/bootbox.min(1).js" type="text/javascript"></script>  
</body>
</html>
