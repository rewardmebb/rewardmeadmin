
<%@page import="com.mollatech.rewardme.nucleus.db.operation.REWARDMEOperator"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmRoles"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.nio.channels.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<html>
    <%@include file="header.jsp" %>
    <script src="./js/modal.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="js/operators.js" type="text/javascript"></script>
    <body>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Operator's Details</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
                <div class="col-lg-12">                    
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                            <i class="fa fa-shopping-cart"></i> Operator's Details &#47;                                                          
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered display nowrap">
                                <thead>
                                    <tr>

                                        <td>No.</td>
                                        <td>Name</td>
                                        <!--<td>Contact No.</td>-->
                                        <td>Email</td>
                                        <td>Manage</td>
                                        <!--<td>Role</td>-->
                                        <td>Password</td>
                                        <!--<td>Attempts</td>-->
                                        <!--                                        <td>Audit</td>-->
                                        <td>Created</td>
                                        <td>Last Access</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%                       
                                        OperatorsManagement oManagement = new OperatorsManagement();                                       
                                        REWARDMEOperator[] axiomoperatorObj = null;
                                        RmRoles[] roles = oManagement.getAllRoles();
                                        axiomoperatorObj = oManagement.ListOperatorsInternal();

                                        for (int i = 0; i < axiomoperatorObj.length; i++) {
                                            REWARDMEOperator axoprObj = axiomoperatorObj[i];
                                            java.util.Date dLA = new java.util.Date(axoprObj.getLastUpdateOn());
                                            java.util.Date dCR = new java.util.Date(axoprObj.getUtcCreatedOn());

                                            int iOprStatus = axoprObj.getiStatus();
                                            String strStatus;
                                            if (iOprStatus == GlobalStatus.ACTIVE) {
                                                strStatus = "Active";
                                            } else if (iOprStatus == GlobalStatus.SUSPEND) {
                                                strStatus = "Suspended";
                                            } else {
                                                strStatus = "Locked";
                                            }
                                            SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm ");

                                            String uidiv4OprStatus = "operator-status-value-" + i;

                                            String uidiv4OprRole = "operator-role-value-" + i;

                                            String uidiv4OprAttempts = "operator-attempts-value-" + i;
                                    %>
                                    <tr>

                                        <td font style="font-size: 14px"><%=i + 1%></td>
                                        <td font style="font-size: 14px"><%=axoprObj.getStrName()%></td>
                                        <td font style="font-size: 14px"><%=axoprObj.getStrEmail()%></td>
                                        <td>
                                            <div class="btn-group">                                                
                                                <a class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><%=strStatus%> <span class="caret"><font style="font-size: 12px"></font></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', '<%=GlobalStatus.ACTIVE%>', '<%=uidiv4OprStatus%>')">Active</a></li>
                                                    <li><a href="#" onclick="ChangeOperatorStatus('<%=axoprObj.getStrOperatorid()%>', '<%=GlobalStatus.SUSPEND%>', '<%=uidiv4OprStatus%>')">Suspended</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" onclick="loadEditOperatorDetails('<%=axoprObj.getStrOperatorid()%>')">Edit Details</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="btn-group">                                                
                                                <a class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown">***** <span class="caret"><font style="font-size: 12px"></font></span></a>
                                                <ul class="dropdown-menu" >
                                                    <li><a href="#" onclick="OperatorUnlockPassword('<%=axoprObj.getStrOperatorid()%>', '<%=uidiv4OprAttempts%>')" >Unlock Password</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" onclick="OperatorResendPassword('<%=axoprObj.getStrOperatorid()%>')">Resend Current Password (via email)</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#" onclick="OperatorSetPassword('<%=axoprObj.getStrOperatorid()%>')">Reset Password</a></li>
                                                    <li><a href="#" onclick="OperatorSendRandomPassword('<%=axoprObj.getStrOperatorid()%>')">Reset & Send Password (via email)</a></li>
                                                </ul>
                                            </div>
                                        </td>

                                        <td font style="font-size: 14px"><%=UtilityFunctions.getTMReqDate(dCR)%></td>
                                        <td font style="font-size: 14px"><%=UtilityFunctions.getTMReqDate(dLA)%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <p><a href="#addOperator" class="btn btn-primary btn-xs" data-toggle="modal">Add New Operator</a></p>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <!-- Trigger the modal with a button -->
            <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Test Connection</button>-->
            <!-- Modal -->
            <div class="modal fade" id="addOperator" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h3 id="myModalLabel" style="margin-left: 20px">Add New Operator</h3>
                        </div>
                        <div class="modal-body ">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="AddNewOperatorForm" name="AddNewOperatorForm">
                                    <fieldset> <!-- Name -->
                                        <div class="control-group">
                                            <div class="col-xs-9">
                                                <label class="control-label" for="username">Name </label>
                                                <div class="controls">
                                                    <input type="text" style="margin-left: 80px" id="_oprname" name="_oprname" class="form-control" placeholder="Operator Name " class="input-large">
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="username">Email </label>
                                                    <div class="controls">
                                                        <input type="text" id="_opremail" style="margin-left: 80px"name="_opremail" class="form-control" placeholder=" Email Id " class="input-large">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label" for="username">Phone</label>
                                                    <div class="controls">
                                                        <input type="text" id="_oprphone" style="margin-left: 80px" name="_oprphone" class="form-control" placeholder=" Phone Number " class="input-large">
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="username">Role</label>
                                                    <div class="controls">
                                                        <select class="span8" style="margin-left: 80px"  name="_oprrole" id="_oprrole">
                                                            <%   for (int i = 0; i < roles.length; i++) {
                                                                    if (roles[i].getName().equals(OperatorsManagement.admin) && !roles[i].getName().equals(OperatorsManagement.sysadmin)) {
                                                            %>
                                                            <option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>
                                                            <%
                                                            } else if (roles[i].getName().equals(OperatorsManagement.sysadmin)) {
                                                            %>
                                                            <option value="<%=roles[i].getRoleid()%>"><%=roles[i].getName()%></option>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">                            
                            <button class="btn btn-primary btn-xs" onclick="addOperator()" id="addnewOperatorSubmitBut">Add New Operator</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="container">
            <!-- Trigger the modal with a button -->
            <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Test Connection</button>-->
            <!-- Modal -->
            <div class="modal fade" id="editOperator" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h3 id="myModalLabel" style="margin-left: 20px">Edit New Operator</h3>
                        </div>
                        <div class="modal-body ">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="editOperatorForm" name="editOperatorForm">
                                    <fieldset> <!-- Name -->
                                        <div class="control-group">
                                            <div class="col-xs-9">

                                                <label class="control-label" for="username">Name</label><br>
                                                <div class="controls">
                                                    <input type="text" style="margin-left: 80px" id="_oprnameE" name="_oprnameE"  class="form-control" placeholder="Operator Name" class="input-xlarge">
                                                    <input type="hidden" id="_oprroleidE" name="_oprroleidE"/>
                                                    <input type="hidden" id="_oprstatusE" name="_oprstatusE"/>
                                                    <input type="hidden" id="_opridE" name="_opridE"/>
                                                </div>
                                                <label class="control-label" for="username">Email</label><br>
                                                <div class="controls">
                                                    <input type="text" style="margin-left: 80px" id="_opremailE" name="_opremailE" class="form-control" placeholder="Operator Emailid" class="input-xlarge">
                                                </div>
                                                <label class="control-label"  for="username">Phone</label><br>
                                                <div class="controls">
                                                    <input type="text" style="margin-left: 80px" id="_oprphoneE" name="_oprphoneE" class="form-control" placeholder="Operator phone" class="input-xlarge">
                                                </div>

                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div id="editoperator-result"></div>
<!--                            <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeoperator()">Close</button>-->
                            <button class="btn btn-primary" onclick="editoperator()" id="buttonEditOperatorSubmit">Save Changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--        <script src="bower_components/jquery/dist/jquery.min.js"></script>

         Bootstrap Core JavaScript 
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

         Metis Menu Plugin JavaScript 
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

         Morris Charts JavaScript 
        <script src="bower_components/raphael/raphael-min.js"></script>
        <script src="bower_components/morrisjs/morris.min.js"></script>
        <script src="js/morris-data.js"></script>

         Custom Theme JavaScript 
        <script src="dist/js/sb-admin-2.js"></script>-->
        <!-- DataTables JavaScript -->

    </body>
</html>
<%@include file="footer.jsp" %>
