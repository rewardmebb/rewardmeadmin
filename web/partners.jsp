
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.nio.channels.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>
<script src="js/operatorsOLD2.js" type="text/javascript"></script>
<script src="js/partnerRequest.js" type="text/javascript"></script>
<%    RmBrandownerdetails[] sglist = new BrandOwnerManagement().getAllBrandOwnerDetails(SessionId);
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
    String data = "Not Available";
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Brand Owner's Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Brand Owner's</b>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover display nowrap" id="partnerDetails">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th><font style="font-size:13px">No.</th>
                                        <th><font style="font-size:13px">Brand Name</th>
                                        <th><font style="font-size:13px">Phone</th>
                                        <th><font style="font-size:13px">Email</th>
                                        <th><font style="font-size:13px">Company Name</th>
                                        <th><font style="font-size:13px">Registration Number</th>
                                        <th><font style="font-size:13px">Manage</th>
                                        <th><font style="font-size:13px">Created On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        if (sglist != null) {
                                            for (int i = 0; i < sglist.length; i++) {
//                                                count++;                                                
                                    %>
                                    <tr>
                                        <td/>
                                        <td ><font style="font-size:13px"><%=i + 1%></font></td>
                                        <td ><font style="font-size:13px">
                                            <a href="./PartnerInfo.jsp?_partnerId=<%=sglist[i].getOwnerId()%>"><u><%= sglist[i].getBrandName()%></u></a>
                                        </td>
                                        <td >
                                            <font style="font-size:13px">
                                            <%= sglist[i].getPhone()%>
                                            </font>
                                        </td>                                                    
                                        <td >
                                            <font style="font-size:13px"><%= sglist[i].getEmail()%>
                                            </font>
                                        </td>
                                        <td ><font style="font-size:13px">
                                            <%= sglist[i].getCompanyName()%>
                                            </font>
                                        </td >
                                        <td >
                                            <font style="font-size:13px">
                                            <%=sglist[i].getRegistrationNumber()%>
                                            </font>
                                        </td>
                                        <td>
                                            <div class="btn btn-group btn-xs">
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" id="<%=sglist[i].getOwnerId()%>"><%if (sglist[i].getStatus() == GlobalStatus.ACTIVE) {%> <font style="font-size:13px;">ACTIVE</font> <%} else {%><font style="font-size:13px;"> SUSPENDED</font> <%}%><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"  onclick="changeBstatus(<%=GlobalStatus.ACTIVE%>, <%=sglist[i].getOwnerId()%>)" >Mark as Active?</a></li>
                                                    <li><a href="#" onclick="changeBstatus(<%=GlobalStatus.SUSPEND%>, <%=sglist[i].getOwnerId()%>)" >Mark as Suspended?</a></li>                                                               
                                                        <% if (sglist[i].getKycDocument() != null) {%>
                                                    <li><a class="divider"></a></li>
                                                    <li>                                   
                                                        <a href="#" onclick="downloadDocments('<%=sglist[i].getOwnerId()%>')"><i class="fa fa-download"></i>  Download Documents   </a>                                                                                                                                                              
                                                    </li>
                                                    <%}%>  
                                                    <li><a class="divider"></a></li>                                                        
                                                    <li><a href="#" onclick="sendrandompassword('<%=sglist[i].getOwnerId()%>')" >Send Random Password</a></li>                                                               
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <font style="font-size:13px">
                                            <%=sdf.format(sglist[i].getCreationDate())%>
                                            </font>
                                        </td>
                                    </tr> 
                                    <%}
                                    } else {
                                    %>
                                    <tr>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                        <td><%=data%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<%@include file="footer.jsp" %>
