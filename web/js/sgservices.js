/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global bootbox */

var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Authenticating...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}
function validateDocments(id) {
    var s = './ValidateDocument?id=' + id;
    window.location.href = s;
    return false;
}










function enabling() {
//    alert("enabling callled");
    document.getElementById("secondaryTab").style.display = 'block';
}
function disabling() {
//    alert("disabling callled");
    document.getElementById("secondaryTab").style.display = 'none';
}

function strcmpAP(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function prefixStatus() {
    var value = document.getElementById("_prefixStatus").value;
    if (value === 'yes') {
        document.getElementById("prefixStatus").style.display = 'block';
    } else {
        document.getElementById("prefixStatus").style.display = 'none';
    }
}
function Alert4AP(msg) {

    bootboxmodel("<h3>" + msg + "</h3>");
}

function viewoWSURL(heading, URL) {
    bootboxmodel("<h4><font style='color:#000000 '>Original WebService: </font><font style='color:#000000'>" + heading + "</font></h4><h5>URL : <a href=" + URL + " target='_blank'>" + URL + "</a></h5>")
}

function viewWSDLURL1(header, URL) {
    bootboxmodel("<h4><font style='color:#000000 '>Transformed WSDL of : </font><font style='color:#000000'>" + header + "</font></h4><h5><a href=" + URL + " target='_blank'>" + URL + "</a></h5>")
}

function viewWADLURL1(header, URL) {
    bootboxmodel("<h4><font style='color:#000000 '>Transformed WADL of : </font><font style='color:#000000'>" + header + "</font></h4><h5><a href=" + URL + " target='_blank'>" + URL + "</a></h5>")
}

function viewWSURL(URL) {
    bootboxmodel("<h5><a href=" + URL + " target='_blank'>" + URL + "</a></h5>")
}


function loadRequestDetails(tps, tpd, startdate, enddate, starttime, endtime) {
    $('#tps').val(tps);
    $('#tpd').val(tpd);
    $('#stime').val(starttime);
    $('#etime').val(endtime);
    $('#sdate').val(startdate);
    $('#edate').val(enddate);
    $('#abcd').modal('show');
}

function loadRequestDetails1(partnerId, partnerRequestStatus) {
    $('#_partnerRequestId').val(partnerId);
    $('#_partnerRequestStatus').val(partnerRequestStatus);
    $('#approvePartnerRequest').modal('show');
}



//function ChangePartnerStatus() {
//    var reason = document.getElementById('reason').value;
//    var id = document.getElementById('_rejectionPartnerRequestId').value;
//    var status = document.getElementById('_rejectionPartnerRequestStatus').value;
//    if (reason != "Select") {
//        $('#rejectionPartnerModal').modal('hide');
//        var s = './partnerRequestStatus?partnerRequestId=' + id + '&partnerRequestStatus=' + status + '&reason=' + reason;
//        pleasewaitV1();
//        $.ajax({
//            type: 'GET',
//            url: s,
//            dataType: 'json',
//            data: $("#rejectPartnerForm").serialize(),
//            success: function (data) {
//                if (strcmpAP(data._result, "error") === 0) {
//                    waitingV1.modal('hide');
//                    showAlert(data._message, "danger", 5000);
//                } else if (strcmpAP(data._result, "success") === 0) {
//                    waitingV1.modal('hide');
//                    showAlert(data._message, "success", 5000);
//                    location.reload();
//                }
//            }
//        });
//    } else {
//        Alert4Admin("<span><font color=red>Please Select Anyone Reason</font></span>");
//    }
//}

 function ChangePartnerStatus() {
    var reason = document.getElementById('reason').value;
    if (reason === "Select") {
        Alert4Admin("<h4><span><font color=red>Please Select Any Reason</font></span><h4>");
        return;
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font></h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (_result) {                
                if (_result === true) {
                    var id = document.getElementById('_rejectionPartnerRequestId').value;
                    var status = document.getElementById('_rejectionPartnerRequestStatus').value;
                    //if (reason != "Select") {
                    $('#rejectionPartnerModal').modal('hide');
                    var s = './partnerRequestStatus?partnerRequestId=' + id + '&partnerRequestStatus=' + status + '&reason=' + reason;                    
                    pleasewaitV1();
                    $.ajax({
                        type: 'GET',
                        url: s,
                        dataType: 'json',
                        data: $("#rejectPartnerForm").serialize(),
                        success: function (data) {
                            if (strcmpAP(data._result, "error") === 0) {
                                waitingV1.modal('hide');
                                showAlert(data._message, "danger", 5000);
                                //Alert4Admin("<span><font color=red>" + data._message + "</font></span>");
                            } else if (strcmpAP(data._result, "success") === 0) {
                                waitingV1.modal('hide');
                                //Alert4Admin("<span><font color=blue>" + data._message + "</font></span>");
                                showAlert(data._message, "success", 5000);
                                //location.reload();
                                setTimeout(function () {
                                    window.location.href = "./partnerRequest.jsp";
                                }, 3000);
                            }
                        }
                    });
                }
            }
        });
    }
}

function AddPartner() {
    var val1 = document.getElementById('_partnerRequestId').value;
    var val2 = document.getElementById('_groupId').value;
    var val3 = document.getElementById('_partnerRequestStatus').value;
    $('#approvePartnerRequest').modal('hide');
    pleasewaitV1();
    var s = './partnerRequestStatus?partnerRequestId=' + val1 + '&_groupId=' + val2 + '&partnerRequestStatus=' + val3;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddPartnerForm").serialize(),
        success: function (data) {
            if (strcmpAP(data._result, "error") === 0) {
                //Alert4AP("<span><font color=red>" + data._message + "</font></span>");
                waitingV1.modal('hide');
                showAlert(data._message, "danger", 5000);
                window.location.href = 'partnerRequest.jsp';
            } else if (strcmpAP(data._result, "success") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message, "success", 5000);
                window.location.href = 'partnerRequest.jsp';
                //Alert4AP("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}
function loadeditAPI(apid, resid) {
    var value = $('#_eResId option:selected').text();//document.getElementById("").text;
    var s = './iseditEnable.jsp?value=' + value + "&_resId=" + resid + "&_apId=" + apid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (data.count === 0) {
                disabling();
            } else {
                enabling();
            }
            document.getElementById('_ImplClassName').value = data.implclassname;
            $("#_responseType").find('option[value=' + data.type + ']').prop("selected", true);
            apiedit(value, apid, resid);
        }
    });
}
function apiedit(value, apid, resid) {
    var s = './edittransformAPI.jsp?value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_apiTab').empty();
            document.getElementById('_apiTab').innerHTML = data;
            prefixStatus();
        }
    });
}
function editloadClassModal(methodName, value) {
//    var value = document.getElementById("_eResId").value;
    var value = $('#_eResId option:selected').text()
    var _prefixStatus = document.getElementById("_prefixStatus").value;
    var _apPackage = document.getElementById("_apPackage").value;
    var _ImplClassName = document.getElementById("_ImplClassName").value;
    if (_apPackage.trim() === '' || _apPackage.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Package Name" + "</font></h2>");
        return;
    }
    var prefix = "";

    if (_prefixStatus === 'yes') {
        prefix = document.getElementById("_Prefix").value;
        if (prefix.trim() === '' || prefix.indexOf(' ') >= 0) {
            Alert4AP("<h2><font color=red>" + " Please Enter Prefix" + "</font></h2>");
            return;
        }
    }
    var s = './editclassModal.jsp?value=' + value + '&className=' + methodName + '&_prefixStatus=' + _prefixStatus + "&prefix=" + prefix + "&_apPackage=" + _apPackage + "&_ImplClassName=" + _ImplClassName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            document.getElementById('classModal').innerHTML = data;
            $('#newClassModal').modal();
        }
    });
}

function rejectPartner(partnerId, partnerRequestStatus) {
    $('#_rejectionPartnerRequestId').val(partnerId);
    $('#_rejectionPartnerRequestStatus').val(partnerRequestStatus);
    $('#rejectionPartnerModal').modal('show');
}
var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waitingV1.modal();
}
function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Admin(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}

function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    $("#alerts-container").prepend(alert);    
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}