function startProcessing(id, status) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            if (result === true) {
                changeStatus(id, status);
            }
        }
    });
}

function removeData(id, status) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            if (result === true) {
                changeStatus(id, status);
            }
        }
    });
}

function changeStatus(id, status) {
    pleaseWaitProcess();
    var s = './ChangeCDRStatus?id=' + id + "&status=" + status;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                showAlert(data.message, "danger", 3000);

            } else {
                waiting.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./ListOfCDRData.jsp";
                }, 3000);
            }
        }
    });
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function pleaseWaitProcess() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}
function strCompare(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function showCDRDetails(details) {
    details=atob(details);
    $('#editParty').modal();
    $('#cdrDetails').html(details);
}