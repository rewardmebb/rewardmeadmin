/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var waiting;
function pleasewaitBillingReport() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}

function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}


function generateGSTreport() {
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    if(sdate==='' || edate===''){
         var message = "Please select start and end date";
        showAlert(message, "danger", 3000);
        return;
    }
    pleasewaitBillingReport();
    var s = './GSTDetails.jsp?_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + '00:00 AM' + '&_etime=' + '11:59 PM';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#report_data').html(data);

        }
    });

}
function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {        
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }        
        type = type || "info";                  
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);            
    $("#alerts-container").prepend(alert);        
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}


function gstPDFDownload(reportType) {

    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    var s = './gstReports?_format=' + reportType +  '&_sdate=' + sdate + '&_edate=' + edate;
    window.location.href = s;
    return false;
}

