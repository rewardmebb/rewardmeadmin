/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//addRange function
k = 0;
function  addHeader() {
    k++;
    var table = document.getElementById("_headerTable");
    var row = table.insertRow(k);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7);
    //alert("i "+i);
    //cell1.innerHTML = "<label class='control-label'  for='_Name'>Key</label>";
    //cell2.innerHTML = "&nbsp;";
    //cell1.innerHTML = "<input type='text' autofocus name='RMText" + i + "' placeholder='Key' class='input-medium'>";
    cell1.innerHTML = "<div class='form-group'><input type='text' class='form-control' id='RMText" + i + "'name='RMText" + i + "'placeholder='Key'></div>";
    // cell4.innerHTML = "&nbsp;Value";
    //cell5.innerHTML = "&nbsp;";
    //cell2.innerHTML = "<input type='text' name='RMType" + i + "' placeholder='Value' class='input-medium'>";
    cell2.innerHTML = "<div class='form-group' style='padding: 1%'><input type='text' class='form-control' id='RMType" + i + "' name='RMType" + i + "' placeholder='Value'></div>";
    //cell3.innerHTML = "<a class='btn btn-primary btn-sm'   onclick='addHeader()' id='addUserButton"+i+"'><span class='glyphicon glyphicon-plus'></span></a>";
    cell3.style.padding = "1%";
    cell4.style.padding = "1%";
    cell3.innerHTML = "<a class='btn btn-info btn-sm'  onclick='addHeader()' id='addUserButton" + i + "' ><span class='fa fa-plus-circle'></span></a>";
    cell4.innerHTML = "<a class='btn btn-warning btn-sm'  onclick='removeHeader()' id='minusUserButton" + i + "' ><span class='fa fa-minus-circle'></span></a>";
    //cell4.innerHTML = "<a class='btn btn-primary btn-sm'   onclick='removeHeader()' id='minusUserButton"+i+"'><span class='glyphicon glyphicon-minus'></span></a>";
    var data = document.getElementById("RMText" + i);
    //alert("RMText"+i +" data"+data);
    if (i >= 1) {
        var j = i - 1;
        document.getElementById("addUserButton" + j).style.display = 'none';
        document.getElementById("minusUserButton" + j).style.display = 'none';
    } else {
        document.getElementById("addUserButton").style.display = 'none';
        document.getElementById("minusUserButton").style.display = 'none';
    }
    i = i + 1;
    document.getElementById("paramcountRM").value = i;
}

function LoanFacilityFeature(val) {
    if (val === 'readOnly') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="LoanFacility" name="LoanFacility" class="form-control" disabled>' +
                '<option selected value="Disable">Disable</option>' +
                '</select>' +
                '</div>';
        document.getElementById('_LoanFacilityFeature').innerHTML = content;
    }


    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="LoanFacility" name="LoanFacility" class="form-control" onchange="VolumeLoanFacility()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">$</span>' +
                '<input type="text" id="loanLimit" name="loanLimit" class="form-control" placeholder="Loan Limit">' +
                '</div>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">$</span>' +
                '<input type="text" id="interestRate" name="interestRate" class="form-control" placeholder="Interest rate">' +
                '</div>' +
                '</div>'
                ;
        document.getElementById('_LoanFacilityFeature').innerHTML = content;
    } else if (val === 'Disable') {
        var content = ' <div class="col-sm-2">' +
                '<select id="LoanFacility" name="LoanFacility" class="form-control" onchange="VolumeLoanFacility()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_LoanFacilityFeature').innerHTML = content;
    }
}

function VolumeLoanFacility() {
    value = document.getElementById("LoanFacility").value;
    LoanFacilityFeature(value);
}
// changes end

function ReedmeCreditPointFeature(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="CreditPoint" name="CreditPoint" class="form-control" onchange="ReedmeCreditFacility()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">$</span>' +
                '<input type="text" id="reedmePrice" name="reedmePrice" class="form-control" placeholder="Price">' +
                '</div>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">$</span>' +
                '<input type="text" id="reedmePoint" name="reedmePoint" class="form-control" placeholder="Point">' +
                '</div>' +
                '</div>'
                ;
        document.getElementById('_ReedmeCreditPointFeature').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="CreditPoint" name="CreditPoint" class="form-control" onchange="ReedmeCreditFacility()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_ReedmeCreditPointFeature').innerHTML = content;
    }
}

function ReedmeCreditFacility() {
    value = document.getElementById("CreditPoint").value;
    ReedmeCreditPointFeature(value);
}
// changes end

function ServiceCharge(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="ServiceCharge" name="ServiceCharge" class="form-control" onchange="VolumeRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<input type="text" id="serviceChargeRate" name="serviceChargeRate" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_ServiceCharge').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="ServiceCharge" name="ServiceCharge" class="form-control" onchange="VolumeRest()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_ServiceCharge').innerHTML = content;
    }
}

function VolumeRest() {
    value = document.getElementById("ServiceCharge").value;
    ServiceCharge(value);
}

// 
function LoanFeature(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="LoanFeature" name="LoanFeature" class="form-control" onchange="VolumeLoanFeature()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-3>' +
                '<input type="text" id="_tpd" name="_tpd" class="form-control" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_LoanFeature').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="LoanFeature" name="LoanFeature" class="form-control" onchange="VolumeLoanFeature()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_LoanFeature').innerHTML = content;
    }
}

function VolumeLoanFeature() {
    value = document.getElementById("LoanFeature").value;
    LoanFeature(value);
}
//
function ChangePackageCharge(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="ChangePackageCharge" name="ChangePackageCharge" class="form-control" onchange="VolumeCPC()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<input type="text" id="ChangePackageChargeRate" name="ChangePackageChargeRate" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_ChangePackageCharge').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="ChangePackageCharge" name="ChangePackageCharge" class="form-control" onchange="VolumeCPC()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_ChangePackageCharge').innerHTML = content;
    }
}

function VolumeCPC() {
    value = document.getElementById("ChangePackageCharge").value;
    ChangePackageCharge(value);
}

function ReactivationCharge(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="ReactivationCharge" name="ReactivationCharge" class="form-control" onchange="VolumeRC()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<input type="text" id="ReactivationChargeRate" name="ReactivationChargeRate" class="form-control" onkeypress="return isNumberKey(event)" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_ReactivationCharge').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="ReactivationCharge" name="ReactivationCharge" class="form-control" onchange="VolumeRC()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_ReactivationCharge').innerHTML = content;
    }
}

function VolumeRC() {
    value = document.getElementById("ReactivationCharge").value;
    ReactivationCharge(value);
}

function CancellationCharge(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="CancellationCharge" name="CancellationCharge" class="form-control" onchange="VolumeCC()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-2>' +
                '<input type="text" id="CancellationChargeRate" name="CancellationChargeRate" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_CancellationCharge').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="CancellationCharge" name="CancellationCharge" class="form-control" onchange="VolumeCC()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_CancellationCharge').innerHTML = content;
    }
}

function VolumeCC() {
    value = document.getElementById("CancellationCharge").value;
    CancellationCharge(value);
}

function LatePenaltyCharge(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-lg-2>' +
                '<select id="LatePenaltyCharge" name="LatePenaltyCharge" class="form-control" onchange="VolumeLP()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div><br><br>' +
//                '</div>'+
                //'<div class="form-group" style="margin-left:25%">'+
                '<div class=col-lg-2 style="margin-left:25%">' +
                '<div class="input-group">' +
                '<span class="input-group-addon">From</span>' +
                '<input type="text" id="LatePenaltyChargeSDay1" name="LatePenaltyChargeSDay1" class="form-control" onkeypress="return isNumericKey(event)" placeholder="Start Day">' +
                '</div>' +
                '</div>' +
                '<div class=col-lg-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">To</span>' +
                '<input type="text" id="LatePenaltyChargeEDay1" name="LatePenaltyChargeEDay1" class="form-control" onkeypress="return isNumericKey(event)" placeholder="End Day">' +
                '</div>' +
                '</div>' +
                '<div class=col-lg-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">Price</span>' +
                '<input type="text" id="LatePenaltyChargeRate1" name="LatePenaltyChargeRate1" class="form-control" onkeypress="return isNumberKey(event)" placeholder="Price">' +
                '</div></div><br><br>' +
                //'</div>'+

                //'<div class="form-group" style="margin-left:25%">'+
                '<div class=col-lg-2 style="margin-left:25%">' +
                '<div class="input-group">' +
                '<span class="input-group-addon">From</span>' +
                '<input type="text" id="LatePenaltyChargeSDay2" name="LatePenaltyChargeSDay2" class="form-control" onkeypress="return isNumericKey(event)" placeholder="Start Day">' +
                '</div>' +
                '</div>' +
                '<div class=col-lg-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">To</span>' +
                '<input type="text" id="LatePenaltyChargeEDay2" name="LatePenaltyChargeEDay2" class="form-control" onkeypress="return isNumericKey(event)" placeholder="End Day">' +
                '</div>' +
                '</div>' +
                '<div class=col-lg-2>' +
                '<div class="input-group">' +
                '<span class="input-group-addon">Price</span>' +
                '<input type="text" id="LatePenaltyChargeRate2" name="LatePenaltyChargeRate2" class="form-control" onkeypress="return isNumberKey(event)" placeholder="Price">' +
                '</div>' +
                '</div>'
                //'<div>'

//                '<div class="form-group">'+
//                '<label class="col-lg-3 control-label"></label>'+
//                '<div class=col-lg-2>' +
//                '<input type="text" id="_tpd" name="_tpd" class="form-control" placeholder="Start Day" width="40px">'+                
//                '</div>'+
//                '<div class=col-lg-2>'+
//                '<input type="text" id="_tpd" name="_tpd" class="form-control" placeholder="End Day">'+
//                '</div>'+
//                '<div class=col-lg-2>'+
//                '<input type="text" id="_tpd" name="_tpd" class="form-control" placeholder="Price">'+
//                '</div>'+
//                '<div>'

                ;
        document.getElementById('_LatePenaltyCharge').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="LatePenaltyCharge" name="LatePenaltyCharge" class="form-control" onchange="VolumeLP()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_LatePenaltyCharge').innerHTML = content;
    }
}

function VolumeLP() {
    value = document.getElementById("LatePenaltyCharge").value;
    LatePenaltyCharge(value);
}

function SlabPricing(val) {
    if (val === 'Enable') {
        var content =
                '<div class=col-sm-2>' +
                '<select id="SlabPricing" name="SlabPricing" class="form-control" onchange="VolumeSP()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>' +
                '</div>' +
                '<div class=col-sm-3>' +
                '<input type="text" id="_tpd" name="_tpd" class="form-control" placeholder="Price">' +
                '</div>'
                ;
        document.getElementById('_SlabPricing').innerHTML = content;
    } else {
        var content = ' <div class="col-sm-2">' +
                '<select id="SlabPricing" name="SlabPricing" class="form-control" onchange="VolumeSP()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_SlabPricing').innerHTML = content;
    }
}

function VolumeSP() {
    value = document.getElementById("SlabPricing").value;
    SlabPricing(value);
}