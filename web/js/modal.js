function bootboxmodel(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><br>' +
            content +
            '<br><a class="btn btn-primary" data-dismiss="modal" class="close">OK </a></div></div></div></div>');
    popup.modal();      
}
