function strcmpAP(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4AP(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function(result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function viewWSURL(URL) {
    bootbox.alert("<h5>" + URL + "</h5>");
}

var exceptionModal = $('<div id="addNewUser" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
        + '<div class="modal-header">'
        + '<h2 align="center" id="myModalLabel">Download Exception Logs</h2>'
        + '</div>'
        + '<div class="modal-footer">'
        + '<div id="addResource-result"></div>'
        + '<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'
        + '<button class="btn btn-primary" onclick="downloadLogs()" id="addUserButton">Download Logs</button>'
        + '</div>'
        + '</div>');

function generateImplClassrel() {
    var value = $("#_apName").val();
    //data2Send={"_apParamname":}
    document.getElementById("_apBaseUri1").value = document.getElementById("_apBaseUri").value;
    document.getElementById("_apName1").value = document.getElementById("_apName").value;
    document.getElementById("_apMethodType1").value = document.getElementById("_apMethodType").value;
    document.getElementById("_apclassname").value = document.getElementById("_apImplClassName").value;
    alert(document.getElementById("_apclassname").value + " new " + document.getElementById("_apImplClassName").value);
    var s = './genImplClass';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#genImpl").serialize(),
        success: function(data) {
            if (strcmpAP(data._message, "Success") === 0) {


            } else {

            }
        }
    });
}

function generateHttpImplClass(_apResId) {
    var value = $("#_apName").val();
    //data2Send={"_apParamname":}
    document.getElementById("_apBaseUri1").value = document.getElementById("_apBaseUri").value;
//    document.getElementById("_apName1").value = document.getElementById("_apImplClassName").value;
//    document.getElementById("_apMethodType1").value = document.getElementById("_apMethodType").value;
    document.getElementById("_apclassname").value = document.getElementById("_apImplClassName").value;

    var s = './generateXml?_apResId=' + _apResId;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#genImpl").serialize(),
        success: function(data) {
            if (strcmpAP(data._result, "success") === 0) {
                bootbox.confirm("<h2><font color=blue>" + data._message + "</font></h2>", function(result) {
                });
            } else {
                exceptionModal.modal();
            }
        }
    });
}
function SetStore() {
    var value = $("#_apName").val();
    //data2Send={"_apParamname":}
    document.getElementById("paramName").value = document.getElementById("_apParamname").value + "-" + document.getElementById("_apDefaultVal").value;
    var s = './temStorage';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SetLocal").serialize(),
        success: function(data) {
            if (strcmpAP(data._message, "Success") === 0) {
                alert('Test Data');
                location.reload();

            } else {
            }
        }
    });
}


function TimeRestriction(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="TimeRestriction" name="TimeRestriction" class="span2" onchange="TimeRest()">' +
                ' <option value="Enable" selected>Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>&nbsp;&nbsp;' +
//                'Start Time: &nbsp;<input type="text" id="_ApStartTime" name="_ApStartTime" class="input-mini">&nbsp;' +
                'Start Time: &nbsp;<div class="input-prepend">' +
                '<div class="bootstrap-timepicker">' +
                '<input id="_ApStartTime" type="text" class="input-small">' +
                '</div>' +
                '</div>' +
                '&nbsp;End Time: &nbsp;' +
                '<div class="input-prepend">' +
                '<div class="bootstrap-timepicker">' +
                '<input id="_ApEndTime" type="text" class="input-small">' +
                '</div>' +
                '</div>' +
                '</div>';
        document.getElementById('_TimeRestriction').innerHTML = content;
        $('#_ApStartTime').timepicker({
            minuteStep: 1,
            showInputs: false,
            disableFocus: true

        });
        $('#_ApEndTime').timepicker({
            minuteStep: 1,
            showInputs: false,
            disableFocus: true

        });

    } else {
        var content = ' <div class="controls">' +
                '<select id="TimeRestriction" name="TimeRestriction" class="span2" onchange="TimeRest()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_TimeRestriction').innerHTML = content;
    }
}

function IPRestriction(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="IPRestriction" name="IPRestriction" class="span2" onchange="IPRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select>&nbsp;&nbsp;' +
                'Allowed IPs: &nbsp;<textarea id="ips" name="ips" class="input-xxlarge"></textarea>' +
                '</div>';
        document.getElementById('_IPRestriction').innerHTML = content;
        $(document).ready(function() {
            $("#ips").select2({
                tags: [],
                dropdownCss: {display: 'none'},
                tokenSeparators: [","]
            });
        });
    } else {
        var content = ' <div class="controls">' +
                '<select id="IPRestriction" name="IPRestriction" class="span2" onchange="IPRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                '</select>'
                + '</div>';

        document.getElementById('_IPRestriction').innerHTML = content;
    }
}

function DayRestriction(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="DayRestriction" name="DayRestriction" class="span2" onchange="DayRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select> &nbsp;&nbsp;' +
                'Enforce Policy From:' +
                '&nbsp;' +
                '<select id="dayFrom" name="dayFrom" class="span2">' +
                '<option value="1">Sunday</option>' +
                '<option value="2">Monday</option>' +
                '<option value="3">Tuesday</option>' +
                '<option value="4">Wednesday</option>' +
                '<option value="5">Thursday</option>' +
                '<option value="6">Friday</option>' +
                '<option value="7">Saturday</option>' +
                '</select>' +
                '&nbsp;' +
                'To: &nbsp;' +
                '<select id="dayTo" name="dayTo" class="span2">' +
                '<option value="1">Sunday</option>' +
                '<option value="2">Monday</option>' +
                '<option value="3">Tuesday</option>' +
                '<option value="4">Wednesday</option>' +
                '<option value="5">Thursday</option>' +
                '<option value="6">Friday</option>' +
                '<option value="7">Saturday</option>' +
                '</select>' +
                '</div>';
        document.getElementById('_DayRestriction').innerHTML = content;
    } else {
        var content = '<div class="controls">' +
                '<select id="DayRestriction" name="DayRestriction" class="span2" onchange="DayRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                '</select>'
                + '</div>';

        document.getElementById('_DayRestriction').innerHTML = content;
    }
}

function VolumeRestriction(val) {
    if (val === 'Enable') {
        var content = '<div class="controls">' +
                '<select id="VolumeRestriction" name="VolumeRestriction" class="span2" onchange="VolumeRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                ' </select>&nbsp;&nbsp;' +
                'Transactions Allowed Per Day: &nbsp;<input type="text" id="_tpd" name="_tpd" class="input-medium">&nbsp;' +
                '</div>';
        document.getElementById('_VolumeRestriction').innerHTML = content;
    } else {
        var content = ' <div class="controls">' +
                '<select id="VolumeRestriction" name="VolumeRestriction" class="span2" onchange="VolumeRest()">' +
                ' <option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_VolumeRestriction').innerHTML = content;
    }
}

function ThroughputRestriction(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="ThroughputRestriction" name="ThroughputRestriction" class="span2" onchange="ThroughputRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select>&nbsp;&nbsp;' +
                'Transactions Allowed Per Second: &nbsp;' +
                '<select class="span1" name="tps" id="tps">' +
                '<option value="10">10</option>' +
                '<option value="25">25</option>' +
                '<option value="50">50</option>' +
                '<option value="100">100</option>' +
                '<option value="200">200</option>' +
                '<option value="300">300</option>' +
                '<option value="400">400</option>' +
                '<option value="500">500</option>' +
                '</select>' +
                '</div>';
        document.getElementById('_ThroughputRestriction').innerHTML = content;
    } else {
        var content = ' <div class="controls">' +
                '<select id="ThroughputRestriction" name="ThroughputRestriction" class="span2" onchange="ThroughputRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_ThroughputRestriction').innerHTML = content;
    }
}

function Billing(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="BillingRestriction" name="BillingRestriction" class="span2" onchange="BillingRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select> &nbsp;&nbsp;' +
                'Currency Code: &nbsp;<input type="text" id="_currencyCode" name="_currencyCode" class="input-mini">&nbsp;' +
                'Amount: &nbsp;<input type="text" id="_Amount" name="_Amount" class="input-mini">' +
                '</div>';
        document.getElementById('_Billing').innerHTML = content;
    } else {
        var content = ' <div class="controls">' +
                '<select id="BillingRestriction" name="BillingRestriction" class="span2" onchange="BillingRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_Billing').innerHTML = content;
    }
}

function Authentication(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="Authentication" name="Authentication" class="span2" onchange="AuthenticationRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select> &nbsp;&nbsp;' +
                'Certificate &nbsp;<input type="checkbox" id="checkCertificate" name="checkCertificate">&nbsp;</td>' +
                '&nbsp;&nbsp;Token: &nbsp;&nbsp;<input type="checkbox" id="checkToken" name="checkToken">' +
                '</div>';
        document.getElementById('_Authentication').innerHTML = content;
    } else {
        var content = ' <div class="controls">' +
                '<select id="Authentication" name="Authentication" class="span2" onchange="AuthenticationRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                ' </select>'
                + '</div>';

        document.getElementById('_Authentication').innerHTML = content;
    }
}

function TimeRest() {
    value = document.getElementById("TimeRestriction").value;
    TimeRestriction(value);
}

function IPRest() {
    value = document.getElementById("IPRestriction").value;
    IPRestriction(value);
}

function DayRest() {
    value = document.getElementById("DayRestriction").value;
    DayRestriction(value);
}

function VolumeRest() {
    value = document.getElementById("VolumeRestriction").value;
    VolumeRestriction(value);
}

function ThroughputRest() {
    value = document.getElementById("ThroughputRestriction").value;
    ThroughputRestriction(value);
}

function BillingRest() {
    value = document.getElementById("BillingRestriction").value;
    Billing(value);
}

function checkAvailability() {
    var value = $("#_apName").val();
    var s = './CheckAvailability?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                $('#checkAvailability-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#checkAvailability-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function addacesspoint() {
    var _ApStartTime = "";
    var _ApEndTime = "";
    if (document.getElementById("TimeRestriction").value === 'Enable') {
        _ApStartTime = encodeURIComponent(document.getElementById('_ApStartTime').value);
        _ApEndTime = encodeURIComponent(document.getElementById('_ApEndTime').value);
    }
    var s = './AddAccessPoint?_ApEndTime=' + _ApEndTime + '&_ApStartTime=' + _ApStartTime;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#AddAcceessPointForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./accessPoint.jsp";
                });
            }
        }
    });
}

function downloadLogs() {
    var s = './DownloadLogs';
    window.location.href = s;
    return false;
}

function customBindingModal(id) {
    document.getElementById("_customBindingAPID").value = id;
    $('#addCustomBinding').modal();
}

function transformAP() {
    $('#addCustomBinding').modal('hide');
    var id = document.getElementById("_customBindingAPID").value;
    var cben = $("#_customBinding").is(":checked");
    document.getElementById("_customBindingAPID").value = "";
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var s = './DoTransform?id=' + id + "&cben=" + cben;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
//        data: $("#cutsomBindingForm").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpAP(data.result, "exception") == 0) {
                exceptionModal.modal();
            }
            else if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else if (strcmpAP(data.result, "success") == 0) {
                window.location = "./transform.jsp?id=" + data.id;
            }
        }
    });
}

function editAP(id) {
    window.location = "./editAccessPoint.jsp?id=" + id;
}

function eIPRestriction(val) {
    if (val === 'Enable') {
        var content = ' <div class="controls">' +
                '<select id="IPRestriction" name="IPRestriction" class="span2" onchange="IPRest()">' +
                '<option selected value="Enable">Enable</option>' +
                '<option value="Disable">Disable</option>' +
                '</select>&nbsp;&nbsp;' +
                '</div>';
        document.getElementById('_IPRestriction').innerHTML = content;
        $(document).ready(function() {
            $("#ips").select2({
                tags: [],
                dropdownCss: {display: 'none'},
                tokenSeparators: [","]
            });
        });
    } else {
        var content = ' <div class="controls">' +
                '<select id="IPRestriction" name="IPRestriction" class="span2" onchange="IPRest()">' +
                '<option value="Enable">Enable</option>' +
                '<option selected value="Disable">Disable</option>' +
                '</select>'
                + '</div>';

        document.getElementById('_IPRestriction').innerHTML = content;
    }
}

function editacesspoint() {
    var _ApStartTime = "";
    var _ApEndTime = "";
    if (document.getElementById("TimeRestriction").value === 'Enable') {
        _ApStartTime = encodeURIComponent(document.getElementById('_ApStartTime').value);
        _ApEndTime = encodeURIComponent(document.getElementById('_ApEndTime').value);
    }
    var s = './EditAccessPoint?_ApEndTime=' + _ApEndTime + '&_ApStartTime=' + _ApStartTime;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#EditAcceessPointForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./accessPoint.jsp";
                });
            }
        }
    });
}
function loadAPI(id, transValue) {
    var value = document.getElementById("_eResId").value;
    var s = './isEnable.jsp?value=' + value + "&resId=" + id;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (data.count === 0 || transValue === 'Yes') {
                disabling();
            } else {
                enabling();
            }
//            document.getElementById('_Ip').value = data.host;
//            document.getElementById('_port').value = data.port;
            api(value, transValue);
        }
    });
}
function api(value, transValue) {
    var s = './transformAPI.jsp?value=' + value + "&transValue=" + transValue;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#_apiTab').empty();
            document.getElementById('_apiTab').innerHTML = data;
            prefixStatus();
        }
    });
}
function enabling() {
    document.getElementById("secondaryTab").style.display = 'block';
}
function disabling() {
    document.getElementById("secondaryTab").style.display = 'none';
}
function loadAPIModal(methodName, value) {
    var value = document.getElementById("_eResId").value;
    var s = './APIModal.jsp?value=' + value + '&methodName=' + methodName;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('apiModal').innerHTML = data;
            $('#newApiModal').modal();
        }
    });
}

function methodDesc(methodName, value) {
    var s = './methodDesc.jsp?methodName=' + methodName + "&value=" + value;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('apiDescription').innerHTML = data;
            $('#newApiDescModal').modal();
        }
    });
}

function methodReturnDesc(rmethodType, methodName, value) {

    var s = './methodReturnDesc.jsp?methodName=' + methodName + "&rmethodType=" + rmethodType + "&value=" + value;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('apiReturnDescription').innerHTML = data;
            $('#newApiReturnDescModal').modal();
        }
    });
}

function methodInputDesc(methodName, paraName, value) {

    var s = './methodInputDesc.jsp?methodName=' + methodName + "&paraName=" + paraName + "&value=" + value;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('apiInputDescription').innerHTML = data;
            $('#newApiInputDescModal').modal();
        }
    });
}

function loadeditAPIModal(methodName, value) {
//    var value = document.getElementById("_eResId").value;
    var value = $('#_eResId option:selected').text()
    var s = './editAPIModal.jsp?value=' + value + '&methodName=' + methodName;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('apiModal').innerHTML = data;
            $('#newApiModal').modal();
        }
    });
}

function validateMethodAPI(methodName, value) {
    var value = document.getElementById("_eResId").value;
    var s = './ValidateMethodAPI?value=' + value + '&methodName=' + methodName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4AP("<h2><font color=blue>" + data.message + "</font></h2>");
            }
        }
    });
}

function validateEditMethodAPI(methodName, value) {
//    var value = document.getElementById("_eResId").value;
    var s = './ValidateEditMethodAPI?value=' + value + '&methodName=' + methodName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4AP("<h2><font color=blue>" + data.message + "</font></h2>");
            }
        }
    });
}

function loadClassModal(methodName, value) {
    var value = document.getElementById("_eResId").value;
    var _prefixStatus = document.getElementById("_prefixStatus").value;
    var _apPackage = document.getElementById("_apPackage").value;
    if (_apPackage.trim() === '' || _apPackage.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Package Name" + "</font></h2>");
        return;
    }

    var prefix = "";
    if (_prefixStatus === 'yes') {
        prefix = document.getElementById("_Prefix").value;
        if (prefix.trim() === '' || prefix.indexOf(' ') >= 0) {
            Alert4AP("<h2><font color=red>" + " Please Enter Prefix" + "</font></h2>");
            return;
        }
    }
    var s = './classModal.jsp?value=' + value + '&className=' + methodName + '&_prefixStatus=' + _prefixStatus + "&prefix=" + prefix + "&_apPackage=" + _apPackage + "&_ImplClassName=" + _ImplClassName;
    $.ajax({
        type: 'GET',
        url: s,
//        dataType: 'json',
        success: function(data) {
            document.getElementById('classModal').innerHTML = data;
            $('#newClassModal').modal();
        }
    });
}

function saveMethodAPI(methodName, value) {
    var s = './SaveMethodAPI?methodName=' + methodName + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#addMethodAPIForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                $('#methodAPI-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#methodAPI-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function saveMethodDesc(id, methodName, value) {
    var s = './SaveMethodAPIDesc?methodName=' + methodName + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveMethodAPIDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./transform.jsp?id=" + id;
                });
            }
        }
    });
}

function saveEditMethodDesc(id, methodName, value) {
    var s = './EditAPIDesc?methodName=' + methodName + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveEditMethodAPIDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./editTransform.jsp?_apid=" + id;
                });
            }
        }
    });
}

function saveClassDesc(id, className, value) {
    var s = './ClassDescription?className=' + className + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveClassDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./transform.jsp?id=" + id;
                });
            }
        }
    });
}

function saveEditClassDesc(id, className, value) {
    var s = './EditClassDesc?className=' + className + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveClassDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./editTransform.jsp?_apid=" + id;
                });
            }
        }
    });
}

function saveMethodInputDesc(id, methodName, value) {
    var s = './SaveMethodInputDescription?methodName=' + methodName + '&value=' + value + "&paraName=" + paraName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveMethodInputAPIDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    $('#newApiInputDescModal').modal('hide');
                });
            }
        }
    });
}

function saveMethodReturnDesc(rmethodType, methodName, value) {
    var s = './SaveMethodAPIRDesc?methodName=' + methodName + '&value=' + value + "&rmethodType=" + rmethodType;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveMethodAPIRDescform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    $('#newApiReturnDescModal').modal('hide');
                });
            }
        }
    });
}

function saveEditMethodAPI(methodName, value) {
    var s = './SaveEditMethodAPI?methodName=' + methodName + '&value=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#addMethodAPIForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                $('#methodAPI-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#methodAPI-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function saveClassAPI(methodName, value, package) {
    var _tCName = document.getElementById("_tCName").value;
    if (_tCName.trim() === '' || _tCName.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Transformed Class Name" + "</font></h2>");
        return;
    }
    var s = './SaveClassAPI?cName=' + methodName + '&value=' + value + "&package=" + package;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#addClassAPIForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                $('#classAPI-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#classAPI-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function saveEditClassAPI(methodName, value, package) {
    var _tCName = document.getElementById("_tCName").value;
    if (_tCName.trim() === '' || _tCName.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Transformed Class Name" + "</font></h2>");
        return;
    }
    var s = './SaveEditClassAPI?cName=' + methodName + '&value=' + value + "&package=" + package;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#addClassAPIForm").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                $('#classAPI-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#classAPI-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function addTransform(id) {
    var _ImplClassName = document.getElementById("_ImplClassName").value;
    if (_ImplClassName.trim() === '' || _ImplClassName.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Implementation Class Name" + "</font></h2>");
        return;
    }
    var _responseType = document.getElementById("_responseType").value;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var value = document.getElementById("_eResId").value;
    var s = './SaveAPI?value=' + value + "&_ImplClassName=" + _ImplClassName + '&apId=' + id + "&_responseType=" + _responseType;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform,#validateClassAPIform").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpAP(data.result, "error") == 0) {
                exceptionModal.modal();
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                });
            }
        }
    });
}

function editTransform(id) {
    var _ImplClassName = document.getElementById("_ImplClassName").value;
    if (_ImplClassName.trim() === '' || _ImplClassName.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Implementation Class Name" + "</font></h2>");
        return;
    }
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    var value = $('#_eResId option:selected').text();
    var s = './SaveEditAPI?value=' + value + "&_ImplClassName=" + _ImplClassName + '&apId=' + id;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform,#validateClassAPIform").serialize(),
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                });
            }
        }
    });
}
function prefixStatus() {
    var value = document.getElementById("_prefixStatus").value;
    if (value === 'yes') {
        document.getElementById("prefixStatus").style.display = 'block';
    } else {
        document.getElementById("prefixStatus").style.display = 'none';
    }
}

function validateClassAPI(cName, value) {
    var value = document.getElementById("_eResId").value;
    var s = './ValidateClassAPI?value=' + value + '&cName=' + cName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4AP("<h2><font color=blue>" + data.message + "</font></h2>");
            }
        }
    });
}

function validateEditClassAPI(cName, value) {
//    var value = document.getElementById("_eResId").value;
    var s = './ValidateEditClassAPI?value=' + value + '&cName=' + cName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#validateMethodAPIform").serialize(),
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4AP("<h2><font color=blue>" + data.message + "</font></h2>");
            }
        }
    });
}

function changeAPStatus(id, status) {
    var s = './ChangeAPStatus?id=' + id + '&status=' + status;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strcmpAP(data.result, "error") == 0) {
                Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                    window.location = "./accessPoint.jsp";
                });
            }
        }
    });
}

function deleteAP(id) {
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            var s = './DeleteAP?id=' + id;
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strcmpAP(data.result, "error") == 0) {
                        Alert4AP("<h2><font color=red>" + data.message + "</font></h2>");
                    } else {
                        bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {
                            window.location = "./accessPoint.jsp";
                        });
                    }
                }
            });
        }
    });
}

function publishAPI(id, type) {
    var value = document.getElementById("_eResId").value;
    var s = './apiCount.jsp?value=' + value + "&id=" + id;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (data.count === 0) {
                Alert4AP("<h2><font color=red>" + "Please Select Minimum One API" + "</font></h2>");
            } else {
                window.location = "./publishAPI.jsp?id=" + id + '&value=' + value + "&type=" + type;
            }
        }
    });

}
function loadeditAPI(apid, resid, transValue) {
    alert("load edit API");
    var value = $('#_eResId option:selected').text();
    
    var s = './iseditEnable.jsp?value=' + value + "&_resId=" + resid + "&_apId=" + apid;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            
            if (data.count === 0 || transValue === 'Yes') {
                disabling();
            } else {
                enabling();
            }
            alert("edit API");
            document.getElementById('_ImplClassName').value = data.implclassname;
            $("#_responseType").find('option[value=' + data.type + ']').prop("selected", true);
            apiedit(value, apid, resid, transValue);
        }
    });
}
function apiedit(value, apid, resid, transValue) {
    alert("api edit called");
    var s = './edittransformAPI.jsp?value=' + value + "&transValue=" + transValue;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#_apiTab').empty();
            document.getElementById('_apiTab').innerHTML = data;
            prefixStatus();
        }
    });
}
function editloadClassModal(methodName, value) {
    var value = $('#_eResId option:selected').text()
    var _prefixStatus = document.getElementById("_prefixStatus").value;
    var _apPackage = document.getElementById("_apPackage").value;
    var _ImplClassName = document.getElementById("_ImplClassName").value;
    if (_apPackage.trim() === '' || _apPackage.indexOf(' ') >= 0) {
        Alert4AP("<h2><font color=red>" + "Enter Proper Package Name" + "</font></h2>");
        return;
    }
    var prefix = "";
    if (_prefixStatus === 'yes') {
        prefix = document.getElementById("_Prefix").value;
        if (prefix.trim() === '' || prefix.indexOf(' ') >= 0) {
            Alert4AP("<h2><font color=red>" + " Please Enter Prefix" + "</font></h2>");
            return;
        }
    }
    var s = './editclassModal.jsp?value=' + value + '&className=' + methodName + '&_prefixStatus=' + _prefixStatus + "&prefix=" + prefix + "&_apPackage=" + _apPackage + "&_ImplClassName=" + _ImplClassName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            document.getElementById('classModal').innerHTML = data;
            $('#newClassModal').modal();
        }
    });
}
function publish(id) {
    var s = './PublishAPI?id=' + id;
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    pleaseWaitDiv.modal();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            pleaseWaitDiv.modal('hide');
            if (strcmpAP(data.result, "error") == 0) {
                exceptionModal.modal();
            } else {
                bootbox.confirm("<h2><font color=blue>" + data.message + "</font></h2>", function(result) {

                });
            }
        }
    });
}

function AuthenticationRest() {
    value = document.getElementById("Authentication").value;
    Authentication(value);
}

function SetStore() {
    var value = $("#_apName").val();
    //data2Send={"_apParamname":}
    document.getElementById("paramName").value = document.getElementById("_apParamname").value + "-" + document.getElementById("_apDefaultVal").value;
    var s = './temStorage';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#SetLocal").serialize(),
        success: function(data) {
            if (strcmpAP(data._message, "Success") === 0) {
                location.reload();

            } else {
            }
        }
    });
}

function deleteHttpMethod(key, _sgresId) {
    var s = "./deleteHttpMethod.jsp?key=" + key;
    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
        if (result == false) {
        } else {
            $.ajax({
                type: 'GET',
                url: s,
                success: function(data) {
                    var dats = {"_sgresId": _sgresId};
                    $.ajax({
                        type: 'POST',
                        url: './httpMethodsList.jsp?_sgresId=' + _sgresId,
                        success: function(data1) {
                            $('#_showList').html(data1);

                        }
                    });
                }
            });
        }
    });
}

var i = 0;
function  addResourcessss() {
    var element = document.createElement("input");
    element.setAttribute("type", "input");
    element.setAttribute("name", "emytext" + i);
    element.setAttribute("class", "input-large");
    element.setAttribute("placeholder", "Enter Param Name");
    i = i + 1;
    var foo = document.getElementById("emyhttpMethod");
    foo.appendChild(element);
}

function editHttpMethod(key, _sgresId) {
    var s = './editHttpMethod.jsp?key=' + key;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            document.getElementById("_emyName").value = data.mName;
            $("#_epostType").find('option[value=' + data.mType + ']').prop("selected", true);
            i = 0;
            $('#emyhttpMethod').empty();
            for (j = 0; j < data._size; j++) {
//                var divv = document.createElement("div");
//                divv.setAttribute("class", "controls");
//                var label= document.createElement("label");
//                label.setAttribute("class","control-label");
//                label.setAttribute("value","Parameter Name");
//                label.setAttribute("text","Parameter Name");
//                var element = document.createElement("input");
//                element.setAttribute("type", "input");
//                element.setAttribute("name", "emytext" + j);
//                element.setAttribute("class", "input-large");
//                element.setAttribute("placeholder", "Enter Param Name");
//                element.setAttribute("value", data.List[j].pNAme);
//                divv.appendChild(label);
//                divv.appendChild(element);
//                var foo = document.getElementById("emyhttpMethod");
//                foo.appendChild(divv);
                i += 1;
                $("#emyhttpMethod").append("<div class='control-group'><label class='control-label'  for='_Name'>Parameter Name</label><div class='controls'><input type='text' name='emytext" + j + "' placeholder='Enter Param Name' class='input-large' value=" + data.List[j].pNAme + "></div></div>");
            }
            $('#editNewHttpMethods').modal();
        }
    });

}