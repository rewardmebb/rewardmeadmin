/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Users(msg) {
    bootboxmodel("<h4>" + msg + "</h4>");
}
function addMember() {
    var s = './addTeamMember';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#add_team_member").serialize(),
        success: function (data) {

            if (strCompare(data._result, "error") === 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                Alert4Users(data._message);
                window.location.href = "./members.jsp";
            }
        }
    });

}

function cancel(){
    window.location.href="members.jsp";
}

function editTeam() {
    var s = './editTeamMember';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#edit_team_member").serialize(),
        success: function (data) {

            if (strCompare(data._result, "error") == 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") == 0) {
                Alert4Users(data._message);
                window.location.href = "./members.jsp";
            }
        }
    });
}
function changeunlockpassword(status, userid) {

    var s = './unlockuserpassword?_status=' + status + '&_userid=' + encodeURIComponent(userid);
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strCompare(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
}
function resendpassword(userid) {
//    alert("send password");
//    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result === false) {
//        } else {
            var s = './resenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strCompare(data._result, "success") === 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    } else {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                }
            });
//        }
//    });
}

function resetpassword(userid) {
//    bootboxmodel("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result === false) {
//        } else {
            var s = './resetuserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strCompare(data._result, "error") === 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strCompare(data._result, "success") === 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
//        }
//    });
}

function resetandsendpassword(userid) {
//    bootbox.confirm("<h2><font color=red>Are you sure?</font></h2>", function(result) {
//        if (result === false) {
//        } else {
            var s = './setandresenduserpassword?_userid=' + encodeURIComponent(userid);
            $.ajax({
                type: 'GET',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strCompare(data._result, "error") === 0) {
                        Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                    }
                    else if (strCompare(data._result, "success") === 0) {
                        Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                    }
                }
            });
//        }
//    });
}