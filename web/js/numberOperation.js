/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}

function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");

        return false;
    }
    return true;
}

var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waitingV1.modal('show');
}

function editNumberModel(number, numberId,numberenvt) {
    document.getElementById("editNumberModel").innerHTML = "Edit Number " + number;
    document.getElementById("editNo").value = number;
    document.getElementById("numberId").value = numberId;
    var selectOption = document.getElementById("numberType");
    selectOption.innerHTML="";
    var array = ["TEST","LIVE"];
    for (var i = 0; i < array.length; i++) {
    var option = document.createElement("option");
        if(numberenvt == array[i]){
            option.value = array[i];
            option.text  = array[i];
            option.selected = true;
            selectOption.appendChild(option);
        }else{
            option.text  = array[i];
            option.value = array[i];            
            selectOption.appendChild(option);
        }
    }

    $('#editNumbers').modal();
}

function editNumber() {
    $('#editNumbers').modal('hide');
    var editedNo = document.getElementById("editNo").value;
    if(editedNo === "" || editedNo === null){
        var message = 'Empty number is not allowed';
        showAlert(message, "danger", 3000);
        return;
    }
    pleasewaitV1();
    var no = document.getElementById("editNo").value;
    var noId = document.getElementById("numberId").value;
    var s = './EditNumber';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#editnumberForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                waitingV1.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                }, 5000);
            }
        }
    });
}

function deleteNo(numberId) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            //console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleasewaitV1();
                var s = './DeleteNumber?numberId=' + numberId;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {
                            waitingV1.modal('hide');
                            showAlert(data.message, "danger", 3000);
                        } else {
                            waitingV1.modal('hide');
                            showAlert(data.message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                            }, 5000);
                        }
                    }
                });
            }
        }
    });
}

function unassignInfoblastNo(numberId) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            //console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleasewaitV1();
                var s = './UnassignInfoblastNo?numberId=' + numberId;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {
                            waitingV1.modal('hide');
                            showAlert(data.message, "danger", 3000);
                        } else {
                            waitingV1.modal('hide');
                            showAlert(data.message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                            }, 5000);
                        }
                    }
                });
            }
        }
    });
}    
function changeInfoblastPassword(numberId) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            //console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleasewaitV1();
                var s = './ChangeInfoblastPassword?numberId=' + numberId;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {
                            waitingV1.modal('hide');
                            showAlert(data.message, "danger", 3000);
                        } else {
                            waitingV1.modal('hide');
                            showAlert(data.message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                            }, 5000);
                        }
                    }
                });
            }
        }
    });
} 
function changeInfoblastUserStatus(status, numberId) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            //console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleasewaitV1();
                var s = './ChangeIBStatus?numberId=' + numberId +'&_status='+status;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data._result, "error") === 0) {
                            waitingV1.modal('hide');
                            showAlert(data._message, "danger", 3000);
                        } else {
                            waitingV1.modal('hide');
                            showAlert(data._message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                            }, 5000);
                        }
                    }
                });
            }
        }
    });
}    
   


function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        // default to alert-info; other options include success, warning, danger
        type = type || "info";          
        // create the alert div
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);    
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}

var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waitingV1.modal('show');
}
