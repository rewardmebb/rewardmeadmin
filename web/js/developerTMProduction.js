/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');    
    waitingV1.modal();
}
function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    $("#alerts-container").prepend(alert);    
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}
function approveProductionRequest(productionAccessId, status, developerId) {
//    var productionAccessId = productionAccessId;    
//    var status = status;    
    pleasewaitV1();
    var s = './DeveloperProductionAccessReq?productionAccessId=' + productionAccessId + '&status=' + status + '&developerId='+developerId;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',        
        success: function (data) {
            if (strcmpAP(data._result, "error") === 0) {                
                waitingV1.modal('hide');
                showAlert(data._message, "danger", 5000);
                //window.location.href = 'developerProductionAccessRequest.jsp';
            } else if (strcmpAP(data._result, "success") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message, "success", 5000);
                window.location.href = 'developerProductionAccessRequest.jsp';                
            }
        }
    });
}

function rejectDeveloperProductionRequest(productionId, partnerRequestStatus, productionpartnerId) {
    $('#_rejectionProductionId').val(productionId);
    $('#_rejectionPartnerRequestStatus').val(partnerRequestStatus);
    $('#_productionpartnerId').val(productionpartnerId);
    $('#rejectionProductionModal').modal('show');
}
function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Admin(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}
function ChangeDeveloperProductionStatus() {
    var reason = document.getElementById('reason').value;
    if (reason === "Select") {
        Alert4Admin("<h4><span><font color=red>Please Select Atleast One Reason</font></span><h4>");
        return;
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font></h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (_result) {                
                if (_result === true) {                    
                    $('#rejectionProductionModal').modal('hide');
                    var s = './DeveloperProductionAccessRejectReq';                    
                    pleasewaitV1();
                    $.ajax({
                        type: 'GET',
                        url: s,
                        dataType: 'json',
                        data: $("#productionRequestRejectionForm").serialize(),
                        success: function (data) {
                            if (strcmpAP(data._result, "error") === 0) {
                                waitingV1.modal('hide');
                                showAlert(data._message, "danger", 5000);                                
                            } else if (strcmpAP(data._result, "success") === 0) {
                                waitingV1.modal('hide');                                
                                showAlert(data._message, "success", 5000);                                
                                setTimeout(function () {
                                    window.location.href = "./developerProductionAccessRequest.jsp";
                                }, 3000);
                            }
                        }
                    });
                }
            }
        });
    }
}

function strcmpAP(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}