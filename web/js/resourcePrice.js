function strCompare4Res(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Res(msg) {
    bootboxmodel("<h5>" + msg + "</h5>");
}

function bootboxmodel(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><font style="font-weight: 500;color: blue">' +
            content +
            '</font><a class="btn btn-info btn-xs" data-dismiss="modal" class="close" style="align:right"> &nbsp; OK </a></div></div></div></div>');
    popup.modal();
}

function saveAPIPrice(resId) {
    var s = './SaveAPIPrice?resId=' + resId;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#saveAPIPriceDetails").serialize(),
        success: function (data) {
            if (strCompare4Res(data.result, "error") == 0) {
                Alert4Res("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4Res("<h2><font color=blue>" + data.message + "</font></h2>");
                setTimeout(function () {
                    window.location = "./resourceAPIManager.jsp"
                }, 5000)
            }
        }
    });
}

function editAPIPrice(resId) {
    var s = './EditAPIPrice?resId=' + resId;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#editAPIPriceDetails").serialize(),
        success: function (data) {
            if (strCompare4Res(data.result, "error") == 0) {
                Alert4Res("<h2><font color=red>" + data.message + "</font></h2>");
            } else {
                Alert4Res("<h2><font color=blue>" + data.message + "</font></h2>");
                setTimeout(function () {
                    window.location = "./resourceAPIManager.jsp"
                }, 5000)
            }
        }
    });
}
function rejectResourceOwnerRequest(_ResourceId, _Resourcestatus) {
    // document.getElementById("rejectPromoCodeModal").innerHTML = "Reject Promocode ";    
    document.getElementById("_ResourceId").value = _ResourceId;
    document.getElementById("_Resourcestatus").value = _Resourcestatus;

    $('#rejectResource').modal();
}

function rejectResourceRequest() {
    var reason = document.getElementById("reason").value;
    if (reason === "Select") {
        Alert4Res("<h4><span><font color=red>Please Select Any Reason</font></span><h4>");
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font></h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var id = document.getElementById("_ResourceId").value;
                    var status = document.getElementById("_Resourcestatus").value;
                    $('#rejectResource').modal('hide');
                    var s = './RejectResourceRequest?/ResourceId=' + id + '&ResourceStatus=' + status + '&reason=' + reason;
                    pleasewaitV1();
                    $.ajax({
                        type: 'POST',
                        url: s,
                        datatype: 'json',
                        data: $("#rejectResourceForm").serialize(),
                        success: function (data) {
                            if (strCompare4Res(data.result, "error") === 0) {
                                waitingV1.modal('hide');
                                showAlert(data.message, "danger", 3000);
                            } else if (strCompare4Res(data.result, "success") === 0) {
                                waitingV1.modal('hide');
                                showAlert(data.message, "success", 3000);
                                setTimeout(function () {
                                    window.location.href = "./resourceOwnerManager.jsp";
                                }, 3000);
                            }
                        }

                    });
                }
            }
        });
    }
}

//function rejectResourceRequest() {    
//    var reason = document.getElementById("reason").value;
//    var id = document.getElementById("_ResourceId").value;
//    var status = document.getElementById("_Resourcestatus").value;    
//    if (reason != "Select") {
//    $('#rejectResource').modal('hide');
//    var s = './RejectResourceRequest?/ResourceId=' + id + '&ResourceStatus=' + status + '&reason=' + reason;    
//    pleasewaitV1();
//    $.ajax({
//        type: 'POST',
//        url: s,
//        datatype: 'json',
//        data: $("#rejectResourceForm").serialize(),
//        success: function (data) {
//            if (strCompare4Res(data.result, "error") === 0) {
//                waitingV1.modal('hide');
//                showAlert(data.message,"danger",3000);
//            } else if (strCompare4Res(data.result, "success") === 0) {
//                waitingV1.modal('hide');
//                
//                showAlert(data.message,"success",3000);
//                setTimeout(function () {
//                    window.location.href = "./resourceOwnerManager.jsp";
//                }, 3000);
//            }
//        }
//        
//    });
//     }
//     
//     else {
//        Alert4Res("<span><font color=red>Please select any reason</font></span>");
//        //showAlert("Please select any reason","danger",3000);
//    }
//}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}
var waitingV1
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waitingV1.modal('show');
}
function changeResourceRequest(id, status) {
    var s = './ChangeResOwnerStatus?resId=' + id + "&status=" + status;
    pleasewaitV1();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare4Res(data.result, "error") == 0) {
                //Alert4Res("<h2><font color=red>" + data.message + "</font></h2>");
                waitingV1.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                //Alert4Res("<h2><font color=blue>" + data.message + "</font></h2>");
                waitingV1.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location = "./resourceOwnerManager.jsp"
                }, 5000)
            }
        }
    });
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}
function editResOwnerInfo(id) {
    var firstParty = document.getElementById("_selectFirstParties").value;
    var secondParty = document.getElementById("_selectSecondParties").value;
    var thirdParty = document.getElementById("_selectThirdParties").value;
    var fourthParty = document.getElementById("_selectFourthParties").value;

    var firstPartyValue = document.getElementById("_aPartyRev").value;
    var secondPartyValue = document.getElementById("_bPartyRev").value;
    var thirdPartyValue = document.getElementById("_cPartyRev").value;
    var fourthPartyValue = document.getElementById("_dPartyRev").value;

    if (firstParty !== '-1') {
        if (firstParty === secondParty || firstParty === thirdParty || firstParty === fourthParty) {
            showAlert("Two party cann't be same", "danger", 4000);
            return;
        }
    } else if (firstPartyValue > 0) {
        showAlert("Please select party name with revenue percentage", "danger", 4000);
        return;
    }
    if (secondParty !== '-1') {
        if (secondParty === firstParty || secondParty === thirdParty || secondParty === fourthParty) {
            showAlert("Two party cann't be same", "danger", 4000);
            return;
        }
    } else if (secondPartyValue > 0) {
        showAlert("Please select party name with revenue percentage", "danger", 4000);
        return;
    }
    if (thirdParty !== '-1') {
        if (thirdParty === secondParty || firstParty === thirdParty || thirdParty === fourthParty) {
            showAlert("Two party cann't be same", "danger", 4000);
            return;
        }
    } else if (thirdPartyValue > 0) {
        showAlert("Please select party name with revenue percentage", "danger", 4000);
        return;
    }
    if (fourthParty !== '-1') {
        if (fourthParty === secondParty || firstParty === fourthParty || thirdParty === fourthParty) {
            showAlert("Two party cann't be same", "danger", 4000);
            return;
        }
    } else if (fourthPartyValue > 0) {
        showAlert("Please select party name with revenue percentage", "danger", 4000);
        return;
    }
    var s = './UpdateOwnerInfo?resId=' + id;
    pleasewait();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#ownerInfoForm").serialize(),
        success: function (data) {
            if (strCompare4Res(data.result, "error") == 0) {
                waiting.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                waiting.modal('hide');
                showAlert(data.message, "success", 3000);
            }
        }
    });
}

function getRevenue() {
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value;
    s = "./GenerateRevenue?resId=" + resId + "&month=" + month + "&year=" + year;

    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#ownerInfoForm").serialize(),
        success: function (data) {
            $("#_revData").empty();
            Morris.Donut({
                element: '_revData',
                data: data,
                xkey: 'label',
                ykeys: ['value'],
                //colors:['#0BA462','#39B580','#67C69D','#95D7BB'],
            });
        }
    });
}

function loadjscssfile(filename, filetype) {
    if (filetype == "js") { //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    } else if (filetype == "css") { //if filename is an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }
}

function setResOwnerPassword(id) {
    var s = './SendOwnerPassword?ownerId=' + id;
    pleasewaitV1();
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare4Res(data._result, "error") == 0) {
                waitingV1.modal('hide');
                showAlert(data._message, "danger", 3000);
                //Alert4Res("<h2><font color=red>" + data._message + "</font></h2>");
            } else {
                waitingV1.modal('hide');
                showAlert(data._message, "success", 3000);
                //Alert4Res("<h2><font color=blue>" + data._message + "</font></h2>");
            }
        }
    });
}
