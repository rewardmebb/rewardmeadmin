/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//validate emailID 
var waiting;
function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 90%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";

    // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}

// reset password
function resetOperatorPasswordV2() {
    var pleaseWaitDiv = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
//    var _oUsername = document.getElementById('_oUsername').value;
    var _oEmailId = document.getElementById('_oEmailId').value;
    pleaseWaitDiv.modal();
    //var _oRandomString = document.getElementById('_oRandomString').value;

    var s = './ResetOperatorPassword?' + "_oEmailId=" + _oEmailId + "&_oRandomString=";
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            pleaseWaitDiv.modal('hide');
            if (strCompare(data.result, "error") == 0) {
                Alert4Users(data.message, "danger", 3000);
            } else {
                Alert4Users(data.message, "success", 3000);
                window.setTimeout(function () {
                    window.location = "./operatorlogin.jsp";
                }, 3000);

            }
        }
    });
}
