/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    $("#alerts-container").prepend(alert);    
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');    
    waitingV1.modal();
}
function sendInvoiceToDeveloper(invoiceId,partId,cancellationCharge,latePenaltyCharge) {
    var invoiceId = encodeURIComponent(invoiceId);
    var s = './SendInvoiceToDeveloper?_partnerId=' + partId + '&invoiceId=' + invoiceId + '&cancellationCharge='+cancellationCharge +'&latePenaltyCharge='+latePenaltyCharge;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"danger",5000);                
            }
            else if (strCompare(data._result, "success") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"success",4000);
//                setTimeout(function() {
//                    window.location = "./partners.jsp";
//                }, 3000);
            }
        }
    });
}
