function strcmpMC(a, b) {
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4MC(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
    });
}

function loadMCSetting() {
    var s = './LoadMCSettings';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            $("#mStatus").val(data.status);
            var maker = data.maker.split(",");
            for (count = 0; count < maker.length; count++) {
                var options = document.getElementById('maker').options;
                n = options.length;
                for (i = 0; i < n; i++) {
                    if (options[i].value === maker[count]) {
                        options[i].selected = true;
                    }
                }
            }
            $("#maker").select2();
            var chaker = data.chaker.split(",");
            for (count = 0; count < chaker.length; count++) {
                var options = document.getElementById('checker').options;
                n = options.length;
                for (i = 0; i < n; i++) {
                    if (options[i].value === chaker[count]) {
                        options[i].selected = true;
                    }
                }
            }
            $("#checker").select2();
        }
    });
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 90%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";

    // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}
function addMCFormData() {
//   waiting.modal('show');
    var mStatus = document.getElementById("mStatus").value;    
    if (mStatus.trim() === '' || mStatus.indexOf(' ') >= 0) {
        Alert4Users("<h3><font color=red>" + "Please select status" + "</font></h3>");
        return;
    }
    var s = './AddMCSettings';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#addMCForm").serialize(),
        success: function (data) {
            if (strcmpMC(data.result, "error") == 0) {
                Alert4Users("<h3><font color=red>" + data.message + "</font></h3>");
                return;
            } else {
                Alert4Users("<h3><font color=blue>" + data.message + "</font></h3>");
                return;
            }
        }
    });
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}

function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>' +
            content +
            '<a class="btn btn-info btn-xs " data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}