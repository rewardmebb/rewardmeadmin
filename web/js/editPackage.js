function slabPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('apSlabrate').style.display = 'block';
    } else {
        document.getElementById('apSlabrate').style.display = 'none';
    }
}

function loanPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('loanDiv').style.display = 'block';
    } else {
        document.getElementById('loanDiv').style.display = 'none';
    }
}

function creditPointDiv(value) {
    if (value === 'Enable') {
        document.getElementById('creditDiv').style.display = 'block';
    } else {
        document.getElementById('creditDiv').style.display = 'none';
    }
}

function accesspointChangeAPIPrice(value) {
    var s = './accessPointWiseAPI.jsp?_apId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
        }
    });
}

function acesspointChangeAPI(value) {
    var s = './accessPointChangeAPI.jsp?_apId=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
            showSlabPricing(value);
        }
    });
}

function accesspointChangePrice(value, versionData, _resourceId, packageId) {
    var s = './accessPointChangeBilling.jsp?_apId=' + value + '&versionData=' + versionData + '&_resourceId=' + _resourceId + '&_bucketId=' + packageId;
    $('#listOfAPI').html("");
    if (value != -2) {
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#listOfAPI').html(data);
            }
        });
    } else {
        $('#addPackageTab a[href="#apRate"]').tab('show');
    }
}

function showSlabPricing(value) {
    //var s ='./showSlabPriceWindow.jsp?_apId=' + value +'&_resourceId='+_resourceId +'&version='+versionData+'&apiName='+apiName+'&packageId='+packageId;

    var accesspointName = document.getElementById("_Accesspoint2").value;
    var resourceName = document.getElementById("_ResourceForSlabPricing2").value;
    var version = document.getElementById("_VersionForSlabPricing2").value;
    var packageID = document.getElementById("packageID").value;
//            alert("ap "+accesspointName);
//            alert("resourceName "+resourceName);
//            alert("version "+version);
    var s = './showSlabPriceWindow.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version + '&apiName=' + value + '&packageId=' + packageID;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_slabPriceWindow').html(data);
            // acesspointChangeAPIPrice(value);
        }
    });
}

function showSlabAPI(versionData) {
    var ap = document.getElementById("_Accesspoint2").value;
    var _resourceId = document.getElementById("_ResourceForSlabPricing2").value;
    var s = './slabAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForSlabPricing2').html(data);
            var apiName = document.getElementById('_APIForSlabPricing2').value;
            // showSlabAPI(versionData,ap);
            showSlabPricing(apiName);
        }
    });
}

function showSlabVersionAP(value, packageId) {
    var ap = document.getElementById("_Accesspoint12").value;
    var s = './slabVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPricing2').html(data);
            var versionData = document.getElementById('_VersionForAPricing2').value;
            showAPAPIAP(versionData, packageId);
        }
    });
}

function showSlabVersion(value) {
    var ap = document.getElementById("_Accesspoint2").value;
    var s = './slabVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForSlabPricing2').html(data);
            var versionData = document.getElementById('_VersionForSlabPricing2').value;
            showSlabAPI(versionData, ap, value);
        }
    });
}

function acesspointChangeResource() {
    var value = document.getElementById("_Accesspoint2").value;
    var s = './slabResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForSlabPricing2').html(data);
            var res = document.getElementById('_ResourceForSlabPricing2').value;
            var ap = document.getElementById('_Accesspoint2').value;
            showSlabVersion(res);
        }
    });
}

function showAPAPIAP(versionData, packageId) {
    var ap = document.getElementById("_Accesspoint12").value;
    var _resourceId = document.getElementById("_ResourceForAPPricing2").value;
    var s = './slabAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
    accesspointChangePrice(ap, versionData, _resourceId, packageId);
}

function showAPVersion(value, ap, packageId) {
    var s = './slabVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForAPricing2').html(data);
            var versionData = document.getElementById('_VersionForAPricing2').value;
            showAPAPI(versionData, ap, value, packageId);
        }
    });
}

function acesspointTabResource(value, packageId) {
    var s = './slabResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForAPPricing2').html(data);
            var res = document.getElementById('_ResourceForAPPricing2').value;
            var ap = document.getElementById('_Accesspoint12').value;
            showSlabVersionAP(res, packageId, ap);
        }
    });
}

function acessChangeResForTiering() {
    var value = document.getElementById("_Accesspoint3").value;
    var s = './tieringResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForTieringPricing2').html(data);
            var res = document.getElementById('_ResourceForTieringPricing2').value;
            var ap = document.getElementById('_Accesspoint3').value;
            showTieringVersion(res);
        }
    });
}

function showTieringVersion(value) {
    var ap = document.getElementById("_Accesspoint3").value;
    var s = './tieringVersion.jsp?_resourceId=' + value + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForTieringPricing2').html(data);
            var versionData = document.getElementById('_VersionForTieringPricing2').value;
            showTieringAPI(versionData, ap, value);
        }
    });
}

function showTieringAPI(versionData) {
    var ap = document.getElementById("_Accesspoint3").value;
    var _resourceId = document.getElementById("_ResourceForTieringPricing2").value;
    var s = './tieringAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForTieringPricing2').html(data);
            var apiName = document.getElementById('_APIForTieringPricing2').value;
            // showSlabAPI(versionData,ap);
            showTieringPricing(apiName);
        }
    });
}

function showTieringPricing(value) {

    var accesspointName = document.getElementById("_Accesspoint3").value;
    var resourceName = document.getElementById("_ResourceForTieringPricing2").value;
    var version = document.getElementById("_VersionForTieringPricing2").value;
    var packageID = document.getElementById("packageID").value;

    var s = './showTieringPriceWindow.jsp?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version + '&apiName=' + value + '&packageId=' + packageID;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_tieringPriceWindow').html(data);

        }
    });
}

function tieringPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('apTieringrate').style.display = 'block';
    } else {
        document.getElementById('apTieringrate').style.display = 'none';
    }
}
// select resource for api throttling
function getResourceForAPIThrottling(value) {
    //var value = document.getElementById("_accesspointAPIThrottling").value;
    var s = './apiThrottlingResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceForApiThrottling').html(data);
            var res = document.getElementById('_resourceForApiThrottling').value;
            getAPIThrottlingVersion(res);
        }
    });
}
function getAPIThrottlingVersion(resourceId) {
    var ap = document.getElementById("_accesspointAPIThrottling").value;
    var s = './apiThrottlingVersion.jsp?_resourceId=' + resourceId + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionForAPIThrottling').html(data);
            var versionData = document.getElementById('_versionForAPIThrottling').value;
            getAPIForAPIThrottling(versionData);
        }
    });
}
function getAPIForAPIThrottling(versionData) {
    var ap = document.getElementById("_accesspointAPIThrottling").value;
    var _resourceId = document.getElementById("_resourceForApiThrottling").value;
    var packageIDThro = document.getElementById("packageIDThro").value;
    var s = './apiThrottlingAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId + '&packageIDThro=' + packageIDThro;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForThrottling').html(data);
        }
    });
}
function throttlingDiv(value) {
    if (value === 'Enable') {
        document.getElementById('throttlingDiv').style.display = 'block';
    } else {
        document.getElementById('throttlingDiv').style.display = 'none';
        getResourceForAPIThrottling(-1);

    }
}


function flatRateDiv(value) {
    if (value === 'Enable') {
        document.getElementById('flatPriceDiv').style.display = 'block';
    } else {
        document.getElementById('flatPriceDiv').style.display = 'none';
        getResourceForFlatPrice(-1);

    }
}

function getResourceForFlatPrice(value) {
    var s = './flatPriceResource.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceForFlatPrice').html(data);
            var res = document.getElementById('_resourceForFlatPrice').value;
            getVersionForFlatPrice(res);
        }
    });
}
function getVersionForFlatPrice(resourceId) {
    var ap = document.getElementById("_accesspointFlatPrice").value;
    var s = './flatPriceVersion.jsp?_resourceId=' + resourceId + '&_apId=' + ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionForFlatPrice').html(data);
            var versionData = document.getElementById('_versionForFlatPrice').value;
            getAPIForFlatPrice(versionData);
        }
    });
}
function getAPIForFlatPrice(versionData) {
    var ap = document.getElementById("_accesspointFlatPrice").value;
    var _resourceId = document.getElementById("_resourceForFlatPrice").value;
    var packageIDFlat = document.getElementById("packageIDFlat").value;
    var s = './flatPriceAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId + '&packageIDFlat=' + packageIDFlat;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_flatPriceDetails').html(data);
        }
    });
}