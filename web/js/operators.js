function Alert4Operator(msg) {
    bootbox.alert("<h2>" + msg + "</h2>", function (result) {
        if (result == false) {
        } else {
            //end here
        }
    });
}

function RefreshOperators() {
    window.location.href = "./addOperator.jsp";
}


        
function strcmpOpr(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function OperatorAudits(_oprid, _duration) {

    var s = './getopraudits?_oprid=' + encodeURIComponent(_oprid) + "&_duration=" + _duration + "&_format=pdf";
    window.location.href = s;
    return false;

}


function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    type = type || "info";
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function get_time_zone_offset( ) {
    var current_date = new Date();
    return -current_date.getTimezoneOffset() / 60;
}

function ClearAddOperatorForm() {
    $('#add-new-operator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprname").val("");
    $("#_opremail").val("");
    $("#_oprphone").val("");
}

function ClearEditOperatorForm() {
    $('#editoperator-result').html("<span><font color=blue>" + "" + "</font></span>");
    $("#_oprnameE").val("");
    $("#_opremailE").val("");
    $("#_opremailE").val("");
}
function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}


function addOperator() {
    $('#addnewOperatorSubmitBut').attr("disabled", true);
    var s = './addoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#AddNewOperatorForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span></small>");
                $('#addnewOperatorSubmitBut').attr("disabled", false);
                //ClearAddOperatorForm();
                //window.setTimeout(RefreshOperators, 3000);
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    });
}



function loadEditOperatorDetails(_orpid) {
    var s = './getoperator?_oprid=' + encodeURIComponent(_orpid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "success") == 0) {
                $('#idEditOperatorName').html(data._name);
                $("#_oprnameE").val(data._name);
                $("#_opremailE").val(data._email);
                $("#_oprphoneE").val(data._phone);
                $("#_oprroleidE").val(data._roleid);
                $("#_oprstatusE").val(data._status);
                $("#_opridE").val(data._id);
                $('#editoperator-result').html("");
                $("#editOperator").modal();
            } else {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            }
        }
    });
}

//function closeoperator() {
//    window.location.href = "./addOperator.jsp"
//}

function editoperator() {
    $('#buttonEditOperatorSubmit').attr("disabled", false);
    var s = './editoperator';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#editOperatorForm").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span></small>");
                $('#buttonEditOperatorSubmit').attr("disabled", false);
                //ClearEditOperatorForm();
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //ClearEditOperatorForm();
                window.setTimeout(RefreshOperators, 3000);
            }
        }
    });
}

function ChangeOperatorStatus(oprid, status, uidiv) {
    var waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Please Wait...</h4>' +
                '<div class="progress progress-striped active">' +
                '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></div></div></div>');
    waitingV1.modal();
    var s = './changeoprstatus?_op_status=' + status + '&_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            waitingV1.modal('hide');                        
            if (strcmpOpr(data._result, "error") == 0) {                
                showAlert(data._message, "error", 4000);                    
//                Alert4Users("<span><font color=red>" + data._message + "</font></span>");                
            } else if (strcmpOpr(data._result, "success") == 0) {
                //Alert4Users("<span><font color=blue>" + data._message + "</font></span>");                
                showAlert(data._message, "success", 4000);
                window.setTimeout(RefreshOperators, 3000);
            }            
        }
    });
}


function ChangeOperatorRole(_roleid, _rolename, _oprid, uidiv) {
    var s = './changerole?_roleId=' + _roleid + '&_oprid=' + encodeURIComponent(_oprid) + "&_roleName=" + encodeURIComponent(_rolename);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Operator("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}



//function changestatus(){
//    var s = './changeoprstatus';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#licenses_data_table").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


//function getopraudits(){
//    var s = './getopraudits';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function listoperators(){
//    var s = './listoperators';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#addOperator").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}

//function resendpassword(){
//    var s = './resendoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}
//
//function setpassword(){
//    var s = './setoprpassword';
//    $.ajax({
//        type: 'POST',
//        url: s,
//        dataType: 'json',
//        data: $("#passwordid").serialize(),
//        success: function(data) {
//            if ( strcmpOpr(data._result,"error") == 0 ) {
//                $('#login-result').html("<span><font color=red>" + data._message + "</font></span></small>");
//            }
//            else if ( strcmpOpr(data._result,"success") == 0 ) {
//                $('#login-result').html("<span><font color=blue>" + data._message + "</font></span>");
//                window.location.href = data._url;
//            }
//        }
//    });
//}


function OperatorUnlockPassword(oprid, uidiv) {
    var s = './unlockpassword?_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                //$('#addchannel-result').html("<span><font color=blue>" + data._message + "</font></span>");
                var uiToChange = '#' + uidiv;
                $(uiToChange).html(data._value);
            }
        }
    });
}
function OperatorResendPassword(oprid) {
    var s = './resendpassword?_oprid=' + encodeURIComponent(oprid);
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}
function OperatorSetPassword(oprid) {
    var s = './sendrandompassword?_oprid=' + encodeURIComponent(oprid) + '&_send=false';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}
function OperatorSendRandomPassword(oprid) {
    var s = './sendrandompassword?_oprid=' + encodeURIComponent(oprid) + '&_send=true';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            } else if (strcmpOpr(data._result, "success") == 0) {
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });

}


function RefreshLogout() {
    window.location.href = "./signout.jsp";
}


function changeoperatorpassword() {
    var s = './changeoprpassword';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ChangePasswordform").serialize(),
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                $('#Password-result').html("<span><font color=red>" + data._message + "</font></span></small>");
                $('#_oldPassword').val("");
                $('#_newPassword').val("");
                $('#_confirmPassword').val("");
                //ClearAddOperatorForm();
            } else if (strcmpOpr(data._result, "success") == 0) {
                $('#Password-result').html("<span><font color=blue>" + data._message + "</font></span>");
                //ClearAddOperatorForm();
                $('#passwordChange').attr("disabled", true);
                window.setTimeout(RefreshLogout, 5000);
            }
        }
    });
}

function DownloadZippedLog() {
    var s = './downloadlogs';
    window.location.href = s;
    return false;

}

//function DownloadZippedLogByDay(){
//    $('#downloadzippedlog').modal('hide');
//     var val1 = encodeURIComponent(document.getElementById('_logsDate').value);
//    var s = './downloadlogsbyday?logsDate='+val1;
//    window.location.href = s;
//    return false;
//    
//}

function DownloadZippedLogByDay() {
    $('#downloadzippedlog').modal('hide');
    var val1 = encodeURIComponent(document.getElementById('_logsDate').value);
    var s = './downloadlogsbyday?logsDate=' + val1;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data._result, "error") == 0) {
                Alert4Operator("<span><font color=red>" + data._message + "</font></span>");
            } else {
                window.location.href = s;
                return false;
            }
        }
    });

}


function DownloadCleanupLog() {
    var s = './downloadCleanuplogs';
    window.location.href = s;
    return false;

}




function checkValidMonitor() {
    var s = './MonitorHandling';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {


        }
    });
}
function downloadServerCert() {
    var s = './DownloadServerCert';
    window.location.href = s;
    return false;
}
function resetOperatorPassword() {
    var _oUsername = document.getElementById('_oUsername').value;
    var _oEmailId = document.getElementById('_oEmailId').value;
    var _oRandomString = document.getElementById('_oRandomString').value;
    $('#password-reset-result').html("");
    var Exp = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
    if (!_oRandomString.match(Exp)) {
        $('#password-reset-result').html("<span><font color=red>" + 'Random String Must be Alphanumeric' + "</font></span></small>");
        return;
    }

    if (_oRandomString.length !== 8) {
        $('#password-reset-result').html("<span><font color=red>" + 'Random String Must be 8 Characters Only' + "</font></span></small>");
        return;
    }
    var s = './ResetOperatorPassword?_oUsername=' + _oUsername + "&_oEmailId=" + _oEmailId + "&_oRandomString=" + _oRandomString;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data.result, "error") == 0) {
                $('#password-reset-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
            }
        }
    });
}

function sendPasswordInPDF(randomString, smsRandomString) {
//    alert(document.getElementById('_ocUsername').value);
    $('#confirm-password-reset-result').html('');
    var _randomString = document.getElementById('_ocRandomString').value;
    var _cnfmRandomString = document.getElementById('_cnfmRandomString').value;
    if (_randomString !== randomString) {
        $('#confirm-password-reset-result').html("<span><font color=red>Random String Not Matched</font></span></small>");
        return;
    }
    if (_cnfmRandomString !== smsRandomString) {
        $('#confirm-password-reset-result').html("<span><font color=red>SMS Random String Not Matched</font></span></small>");
        return;
    }
    var _oUsername = document.getElementById('_ocUsername').value;
    var _oEmailId = document.getElementById('_ocEmailId').value;
    $('#confirm-password-reset-result').html("");
    var s = './ResetPasswordAfterConfirmDetails?_oUsername=' + _oUsername + "&_oEmailId=" + _oEmailId + "&_oRandomString=" + _randomString + "&smsRandomString=" + smsRandomString;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data.result, "error") == 0) {
                $('#confirm-password-reset-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#confirm-password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
                window.location = "./index.jsp";
            }
        }
    });
//    ResetPasswordAfterConfirmDetails
}

function resendRandomString() {
    var _oUsername = document.getElementById('_ocUsername').value;
    var _oEmailId = document.getElementById('_ocEmailId').value;
    $('#confirm-password-reset-result').html("");
    var s = './ResendRandomString?_oUsername=' + _oUsername + "&_oEmailId=" + _oEmailId;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strcmpOpr(data.result, "error") == 0) {
                $('#confirm-password-reset-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#confirm-password-reset-result').html("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    window.location = "./index.jsp"
                }, 5000);
            }
        }
    });
}