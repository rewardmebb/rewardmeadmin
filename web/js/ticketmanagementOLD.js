/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//function bootboxmodel(content) {
//    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
//            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
//            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><font style="font-weight: 900;color: blue">' +
//            content +
//            '</font><br><a class="btn btn-primary" data-dismiss="modal" class="close" style="align:right"> &nbsp; OK </a></div></div></div></div>');
//    popup.modal();
//}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootboxmodel("<h4>" + msg + "</h4>");
}
function ticketback() {
    window.location.href = './ticket.jsp';
}
function addticketing() {
    window.location.href = './ticketadd.jsp';
}
function ViewonClick(URL) {
    bootboxmodel("<h5>" + URL + "</h5>");
}

function addticket() {
    var s = './UploadImage';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        success: function(data) {

            if (strCompare(data.result, "error") === 0) {
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
            }
            else if (strCompare(data.result, "success") === 0) {
                $('#_filename').val(data.filename);
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                TicketAddition();
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
//    TicketAddition();
}

function TicketAddition() {
    var s = './AddTicket';
    $.ajax({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        data: $("#add_ticket").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                Alert4Users(data._message);
                setTimeout(function() {
                    window.location.href = "./ticket.jsp";
                }, 3000);
            }
        }
    });
}

function ImageUpload() {

    var s = './UploadImage';
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'fileImageToUpload',
        url: s,
        dataType: 'json',
        success: function(data) {

            if (strcmpPDFSign(data.result, "error") === 0) {
                Alert4PDFSign("<span><font color=blue>" + data.message + "</font></span>");
            }
            else if (strcmpPDFSign(data.result, "success") === 0) {
                $('#_filename').val(data.filename);
                Alert4PDFSign("<span><font color=blue>" + data.message + "</font></span>");
                document.getElementById("signeraddition").style.display = 'block';
            }
        },
        error: function(data, status, e)
        {
            alert(e);
        }
    });
}


function emailSend(){
    var subject = document.getElementById("subject").value;
    var category = document.getElementById("_setCategory").value;
    var messageBody = document.getElementById("msgBody").value;
    var message = null;
    
    if(subject === null || subject === "" || subject === "Select"){
        message = "Please select subject !";
        showAlert(message, "danger", 3000);
        return ;
    }
    if(category === null || category === "" || category === "Select"){
        message = "Please select category !";
        showAlert(message, "danger", 3000);
        return ;
    }
    if(messageBody === null || messageBody === ""){
        message = "Please Enter Message !";
        showAlert(message, "danger", 3000);
        return;
    }
    pleaseWaitProcess();
    var s = './ticketEmailSending';
    $.ajax({
        type: 'POST',
        fileElementId: 'ticketCompose',
        url: s,
        dataType: 'json',
        data: $("#ticketCompose").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                //Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message,"danger",3000);
            }
            else if (strCompare(data._result, "success") === 0) {
                //Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
                waiting.modal('hide');
                showAlert(data._message,"success",3000);
                setTimeout(function() {
                    window.location.href = "./ticketComposeMail.jsp";
                }, 3000);
            }
        
        },
        error: function(data, status, e)
            {
                waiting.modal('hide');
                alert(e);
            }
    });
}


function deleteEmailById(id){
    bootbox.confirm({
    message: "Do you want to delete this email ?",
    buttons: {
        confirm: {
            label: 'Yes',
            className: 'btn-success btn-xs'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger btn-xs'
        }
    },
    callback: function (result) {
        //console.log('This was logged in the callback: ' + result);
        if(result === true){
            pleaseWaitProcess();
            var s = './deleteEmail?id=' + id;
            $.ajax({
                type: 'POST',
                url: s,
                dataType: 'json',
                success: function(data) {
                    if (strCompare(data._result, "error") === 0) {
                        waiting.modal('hide');
                        showAlert(data._message,"danger",3000);
                    }
                    else if (strCompare(data._result, "success") === 0) {
                        waiting.modal('hide');
                        showAlert(data._message,"success",3000);
                        setTimeout(function() {
                            window.location.reload(true);
                        }, 3000);
                    }
                },
                error: function(data, status, e)
                    {
                        waiting.modal('hide');
                        alert(e);
                    }
            });
        }
    }
});
}


function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {        
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }        
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);            
    $("#alerts-container").prepend(alert);        
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}

function pleaseWaitProcess(){
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>'+
            '<div class="progress progress-striped active">'+
                '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                '</div>'+
            '</div>'+
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}