<%@include file="header.jsp"%>
<script src="./js/modal.js"></script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">                
                    <h4 class="page-header">Add Credits</h4>
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <div class="row">         
                <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;
                            <i class="fa fa-table"></i> Add Credits &#47;                                                                   
                        </div>
                        <div class="panel-body">
                            <table border="0">
                                <td><b>Developer Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                                <td style="width: 60%"> 
                                    <select id="_partnername" name="_partnername" class="form-control span3" onchange="loadCredits()" style="width: 100%">
                                        <option value="-1" selected>Select Brand Owner</option>     
                                        <%                                            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");

                                            RmBrandownerdetails[] pd = new BrandOwnerManagement().getAllBrandOwnerDetails(sessionId);
                                            if (pd != null) {
                                                for (int i = 0; i < pd.length; i++) {
                                        %>
                                        <option value="<%=pd[i].getOwnerId()%>"><%=pd[i].getBrandName()%></option>
                                        <%}
                                            }%>                                          
                                    </select>
                                </td>
                            </table>
                            <br>
                            <div id="loadCreditsResult"></div>   
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <%@include file="footer.jsp"%>                                    
    <script>
        var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Please Wait...</h4>' +
                '<div class="progress progress-striped active">' +
                '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></div></div></div>');
        function loadCredits() {

            waiting.modal();
            var x = document.getElementById("_partnername").value;

            var s = './loadCredits.jsp?developerId=' + x;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    waiting.modal('hide');
                    $('#loadCreditsResult').html(data);
                }
            });

        }
        function addCredits(x) {
            mc = document.getElementById("mainCredits").value;
            c = document.getElementById("Credits").value;
            waiting.modal();
            var s = './AddCredits?developerId=' + x + "&_mainCredits=" + mc + "&_Credits=" + c;
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    waiting.modal('hide');
                    if (data.result === 'success') {
                        showAlert(data.message, "success", 4000);
                    } else {
                        showAlert(data.message, "error", 4000);
                    }
                }
            });
        }
        function showAlert(message, type, closeDelay) {
            if ($("#alerts-container").length == 0) {
                $("body")
                        .append($('<div id="alerts-container" style="position: fixed;' +
                                'width: 50%; left: 25%; top: 10%;">'));
            }
            type = type || "info";
            message = '<i class="fa fa-info-circle"></i> ' + message;
            var alert = $('<div class="alert alert-' + type + ' fade in">')
                    .append(
                            $('<button type="button" class="close" data-dismiss="alert">')
                            .append("&times;")
                            )
                    .append(message);
            $("#alerts-container").prepend(alert);
            if (closeDelay)
                window.setTimeout(function () {
                    alert.alert("close")
                }, closeDelay);
        }
    </script>
</body>

