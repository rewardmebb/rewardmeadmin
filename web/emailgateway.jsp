<%@include file="header.jsp"%>
<!--<html>-->
<!--    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>-->
    <script src="js/emailgateway.js" type="text/javascript"></script>
<!--    <body>-->
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">                
                        <h3 class="page-header">Email SMTP Gateway Configuration</h3>
                        <p>To facilitate mobile based notification, Email SMTP Gateway(s) connection needs to be configured. For auto fail-over, please set the Secondary Gateway too. </p>
                    </div>
                    <!-- /.col-lg-10 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">              
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                                <i class="fa fa-shopping-cart"></i> Email Configuration Settings &#47;                                                          
                            </div>
                            <div class="tabbable">
                                <br>
                                <div class="col-lg-12">
                                    <ul class="nav nav-pills">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <li class="active"><a href="#primary" data-toggle="tab">Primary Email SMTP Gateway</a></li>
                                        &nbsp;&nbsp;&nbsp;
                                        <li><a href="#secondary" data-toggle="tab">Secondary Email SMTP Gateway</a></li>
                                    </ul>
                                </div>
                                <br/><br/><br/>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="primary">
                                        <div class="row-fluid">
                                            <form class="form-horizontal" id="emailprimaryform" name="emailprimaryform">

                                                <div id="legend">
                                                    <legend class=""><h4>&nbsp;&nbsp;&nbsp;Primary Email SMTP Gateway Configuration</h4></legend>
                                                </div>

                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">Status </label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_status-primary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeActiveStatusEMAIL(1, 1)">Mark as Active?</a></li>
                                                                <li><a href="#" onclick="ChangeActiveStatusEMAIL(1, 0)">Mark as Suspended?</a></li>
                                                            </ul>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">With Auto Fail-Over to Secondary Gateway </label>  
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_autofailover-primary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeFailOverEMAIL(1)">Mark as Enabled?</a></li>
                                                                <li><a href="#" onclick="ChangeFailOverEMAIL(0)">Mark as Disabled?</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>

                                                <input type="hidden" id="_statusEMAIL" name="_statusEMAIL">
                                                <input type="hidden" id="_perferenceEMAIL" name="_perferenceEMAIL" value="1">
                                                <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="4">
                                                <input type="hidden" id="_autofailoverEMAIL" name="_autofailoverEMAIL">
                                                <input type="hidden" id="_retriesEMAIL" name="_retriesEMAIL">
                                                <input type="hidden" id="_retrydurationEMAIL" name="_retrydurationEMAIL">

                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls"> 
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">Retry Count</label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_retries-primary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(1, 2)">2 Retries</a></li>
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(1, 3)">3 Retries</a></li>
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(1, 5)">5 Retries</a></li>
                                                            </ul>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">With wait time between retries as </label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_retryduration-primary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(1, 10)">10 seconds</a></li>
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(1, 30)">30 seconds</a></li>
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(1, 60)">60 seconds</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Host</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_ipEMAIL" name="_ipEMAIL" class="form-control" placeholder="example localhost/127.0.0.1">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <!--</div>-->
                                                        <!--&nbsp;&nbsp;&nbsp;-->
                                                        <!--<div class="form-group">-->
                                                        <label class="control-label col-lg-1">Port</label>
                                                        <div class="col-lg-2">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_portEMAIL" name="_portEMAIL" class="form-control" placeholder="443" >                                    
                                                            <!--</div>-->

                                                        </div>
                                                        <label class="control-label"  for="username">with &nbsp;&nbsp;</label><input type="checkbox" id="_sslEMAIL" name="_sslEMAIL" value="sslEnabled"><label class="control-label"  for="username">&nbsp;&nbsp;SSL enabled? </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Authenticate using</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_userIdEMAIL" name="_userIdEMAIL" class="form-control" placeholder="enter user id for authenticaiton ">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <!--</div>-->
                                                        <!--&nbsp;&nbsp;&nbsp;-->
                                                        <!--<div class="form-group">-->
                                                        <label class="control-label col-lg-1">Password</label>
                                                        <div class="col-lg-2">
                                                            <!--<div class="input-group">-->
                                                            <input type="password" id="_passwordEMAIL" name="_passwordEMAIL" class="form-control" placeholder="leave blank is no authentication" >                                    
                                                            <!--</div>-->

                                                        </div>
                                                        <label class="control-label"  for="username">with &nbsp;&nbsp;</label><input type="checkbox" name="_authRequiredEMAIL" id="_authRequiredEMAIL" value="authEnabled"><label class="control-label"  for="username">&nbsp;&nbsp;Authentication Enabled? </label> 
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Sender Details</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_fromNameEMAIL" name="_fromNameEMAIL" class="form-control" placeholder="Sender's Name" class="span4">                                                                     
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_fromEmailEMAIL" name="_fromEmailEMAIL" class="form-control"  placeholder="Sender's Emailid" class="span4" >
                                                            <!--</div>-->
                                                        </div>

                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Additional Attributes</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve1EMAIL" name="_reserve1EMAIL" class="form-control" placeholder="name1=value1">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve2EMAIL" name="_reserve2EMAIL" class="form-control" placeholder="name2=value2">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve3EMAIL" name="_reserve3EMAIL" class="form-control" placeholder="name3=value3">                                    
                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Implementation Class</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_classNameEMAIL" name="_classNameEMAIL" class="form-control" placeholder="complete class name including package" class="input-group-lg">                                    
                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Submit -->
                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div id="save-EMAIL-gateway-primary-result"></div>
                                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                        <% //if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                                        <button class="btn btn-primary" onclick="EditEMAILSetting(1)" type="button">Save Setting</button>
                                                        <%// } %>
                                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                                                        <!--<button class="btn" onclick="LoadTestEMAILConnectionUI(1)" type="button">Test Connection</button>-->
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#testEMAILPrimary">Test Connection</button>
                                                    </div>
                                                </div>
                                                <br>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="secondary">
                                        <div class="row-fluid">
                                            <form class="form-horizontal" id="emailsecondaryform" name="emailsecondaryform">

                                                <div id="legend">
                                                    <legend class=""><h4>&nbsp;&nbsp;&nbsp;Secondary Email SMTP Gateway Configuration</h4></legend>
                                                </div>
                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">Status </label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_status-secondary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeActiveStatusEMAIL(2, 1)">Mark as Active?</a></li>
                                                                <li><a href="#" onclick="ChangeActiveStatusEMAIL(2, 0)">Mark as Suspended?</a></li>
                                                            </ul>
                                                        </div>
                                                        <!--with Auto Fail-Over to Secondary Gateway
                                                        <div class="btn-group">
                                                            <button class="btn btn-small"><div id="_autofailover-secondary-EMAIL"></div></button>
                                                            <button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeFailOverEMAIL(2,1)">Mark as Enabled?</a></li>
                                                                <li><a href="#" onclick="ChangeFailOverEMAIL(2,0)">Mark as Disabled?</a></li>
                                                            </ul>
                                                        </div>
                                                        -->
                                                    </div>

                                                </div>

                                                <input type="hidden" id="_statusEMAILS" name="_statusEMAILS">
                                                <input type="hidden" id="_perferenceEMAILS" name="_perferenceEMAILS" value="2">
                                                <input type="hidden" id="_typeEMAIL" name="_typeEMAIL" value="4">
                                                <input type="hidden" id="_autofailoverEMAILS" name="_autofailoverEMAILS">
                                                <input type="hidden" id="_retriesEMAILS" name="_retriesEMAILS">
                                                <input type="hidden" id="_retrydurationEMAILS" name="_retrydurationEMAILS">

                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">Retry Count</label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_retries-secondary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(2, 2)">2 Retries</a></li>
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(2, 3)">3 Retries</a></li>
                                                                <li><a href="#" onclick="ChangeRetryEMAIL(2, 5)">5 Retries</a></li>
                                                            </ul>
                                                        </div>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="control-label"  for="username">With wait time between retries as</label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div class="btn-group">
                                                            <button class="btn btn-default"><div id="_retryduration-secondary-EMAIL"></div></button>
                                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(2, 10)">10 seconds</a></li>
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(2, 30)">30 seconds</a></li>
                                                                <li><a href="#" onclick="ChangeRetryDurationEMAIL(2, 60)">60 seconds</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Host</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_ipEMAILS" name="_ipEMAILS" class="form-control" placeholder="example localhost/127.0.0.1">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <!--</div>-->
                                                        <!--&nbsp;&nbsp;&nbsp;-->
                                                        <!--<div class="form-group">-->
                                                        <label class="control-label col-lg-1">Port</label>
                                                        <div class="col-lg-2">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_portEMAILS" name="_portEMAILS" class="form-control" placeholder="443" >                                    
                                                            <!--</div>-->

                                                        </div>
                                                        <label class="control-label"  for="username">with &nbsp;&nbsp;</label><input type="checkbox" id="_sslEMAILS" name="_sslEMAILS" value="sslEnabled"><label class="control-label"  for="username">&nbsp;&nbsp;SSL enabled? </label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Authenticate using</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_userIdEMAILS" name="_userIdEMAILS" class="form-control" placeholder="enter user id for authenticaiton ">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <!--</div>-->
                                                        <!--&nbsp;&nbsp;&nbsp;-->
                                                        <!--<div class="form-group">-->
                                                        <label class="control-label col-lg-1">Password</label>
                                                        <div class="col-lg-2">
                                                            <!--<div class="input-group">-->
                                                            <input type="password" id="_passwordEMAILS" name="_passwordEMAILS" class="form-control" placeholder="leave blank is no authentication" >                                    
                                                            <!--</div>-->

                                                        </div>
                                                        <label class="control-label"  for="username">with &nbsp;&nbsp;</label><input type="checkbox" name="_authRequiredEMAILS" id="_authRequiredEMAILS" value="authEnabled"><label class="control-label"  for="username">&nbsp;&nbsp;Authentication Enabled? </label> 
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Sender Details</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_fromNameEMAILS" name="_fromNameEMAILS" class="form-control" placeholder="Sender's Name" class="span4">                                                                     
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_fromEmailEMAILS" name="_fromEmailEMAILS" class="form-control"  placeholder="Sender's Emailid" class="span4" >
                                                            <!--</div>-->
                                                        </div>

                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Additional Attributes</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve1EMAILS" name="_reserve1EMAILS" class="form-control" placeholder="name1=value1" class="span3">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve2EMAILS" name="_reserve2EMAILS" class="form-control" placeholder="name2=value2" class="span3">                                    
                                                            <!--</div>-->
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_reserve3EMAILS" name="_reserve3EMAILS" class="form-control" placeholder="name3=value3" class="span3">                                    
                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Implementation Class</label>
                                                        <div class="col-lg-3">
                                                            <!--<div class="input-group">-->
                                                            <input type="text" id="_classNameEMAILS" name="_classNameEMAILS" class="form-control" placeholder="complete class name including package" class="input-xlarge">                                    
                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Submit -->
                                                <div class="control-group">
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div class="controls">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div id="save-EMAIL-gateway-primary-result"></div>
                                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                        <%// if (Integer.valueOf((String) session.getAttribute("_apOprRole")).intValue() >= 3) { %>
                                                        <button class="btn btn-primary" onclick="EditEMAILSetting(2)" type="button">Save Setting </button>
                                                        <%//}%>
                                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                                                        <!--<button class="btn" onclick="LoadTestEMAILConnectionUI(2)" type="button">Test Connection </button>-->
                                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#testEMAILSecondary">Test Connection</button>
                                                    </div>
                                                </div>
                                                <br>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script language="javascript" type="text/javascript">
                LoadEMAILSetting(1);
                LoadEMAILSetting(2);
            </script>
        </div>

        <div class="container">
            <!-- Trigger the modal with a button -->
            <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Test Connection</button>-->
            <!-- Modal -->
            <div class="modal fade" id="testEMAILPrimary" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h3 id="myModalLabel" style="margin-left: 20px">Test Primary Gateway</h3>
                        </div>
                        <div class="modal-body ">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="testEMAILPrimaryForm" name="testEMAILPrimaryForm">
                                    <fieldset> <!-- Name -->
                                        <div class="control-group">
                                            <div class="col-xs-9">
                                                <label class="control-label" for="username">Test Message</label>
                                                <br/><br/>
                                                <div class="controls">
                                                    <input type="text" style="margin-left: 80px" id="_testmsg" name="_testmsg" class="form-control" placeholder="this is the sample email to be sent out..." class="input-large">
                                                    <input type="hidden" id="_type" name="_type" value="4">
                                                    <input type="hidden" id="_preference" name="_preference" value="1">
                                                </div>
                                                <br/>
                                                <div class="control-group">
                                                    <label class="control-label" for="username">Test Email ID</label>
                                                    <br/><br/>
                                                    <div class="controls">
                                                        <input type="text" id="_testphone" style="margin-left: 80px"name="_testphone" class="form-control" placeholder="set email id to send message" class="input-large">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="span3" id="test-EMAIL-primary-configuration-result"></div>
                            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button class="btn btn-primary" onclick="testconnectionEMAILprimary()" id="testEMAILPrimaryBut">Send Message Now</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="container">
            <!-- Trigger the modal with a button -->
            <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Test Connection</button>-->

            <!-- Modal -->
            <div class="modal fade" id="testEMAILSecondary" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h3 id="myModalLabel"style="margin-left: 20px">Test Secondary Gateway</h3>
                        </div>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="testEMAILSecondaryForm" name="testEMAILSecondaryForm">
                                    <fieldset>
                                        <!-- Name -->
                                        <div class="control-group">
                                            <div class="col-xs-9">
                                                <label class="control-label" for="username">Test Message</label>
                                                <br/><br/>
                                                <div class="controls">
                                                    <input type="text" id="_testmsg" style="margin-left: 80px"name="_testmsg" class="form-control" placeholder="this is the sample email to be sent out..." class="input-xlarge">
                                                    <input type="hidden" id="_type" name="_type" value="4">
                                                    <input type="hidden" id="_preference" name="_preference" value="2">
                                                </div>
                                                <br/>
                                                <label class="control-label" for="username">Test Email ID</label>
                                                <br/><br/>
                                                <div class="controls">
                                                    <input type="text" id="_testphone" style="margin-left: 80px" name="_testphone" class="form-control" placeholder="set email id to send message" class="input-xlarge">
                                                </div>
                                            </div> </div>                   
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="span3" id="test-EMAIL-secondary-configuration-result"></div>
                            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button class="btn btn-primary" onclick="testconnectionEMAILsecondary()" id="testEMAILSecondaryBut">Send Message Now</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
<!--    </body>
</html>-->
<%@include file="footer.jsp" %>