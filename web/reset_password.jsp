<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RewardMe Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.css" rel="stylesheet" type="text/css"/>
        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet" type="text/css"/>
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
        <!-- Morris Charts CSS -->
        <link href="bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css"/>
        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/operatorForgotPassword.js" type="text/javascript"></script>
        <!--        <script src="./js/operators.js"></script>-->
        <script src="./js/operatorsOLD2.js"></script>
        <script src="./js/modal.js"></script>
    </head>
    <body>
        <div class="container">
            <div id = "alerts-container"></div>
            <div class="login-panel-show">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b> RESET PASSWORD</b>
                        </div>
                        <div class="panel-body">
                            <form action="#" id="loginForm">
                                <div class="form-group">
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" class="form-control" id="_oEmailId" name="_oEmailId" placeholder="Your email Id" tabindex="0" autofocus>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-default ladda-button btn-group-sm" data-style="zoom-in" id="resetButton" onclick="window.location.href = 'operatorlogin.jsp'"> Back to login</button>
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-success ladda-button btn-group-sm" data-style="zoom-in" id="resetButton" onclick="resetOperatorPasswordV2()"> Reset</button>                                                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--        New ly added-->

        <script>
            function loginFunction(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    document.getElementById("resetButton").click();
                }
            }
        </script>

        <%
            java.util.Date dFooter = new java.util.Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);
            SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            String completeTimewithLocalTZ = tz.format(dFooter);
            long LongTime = dFooter.getTime() / 1000;

        %>
        <footer>
            <div align="center" style="margin-top: 10%">
                <p>&copy; Molla Technologies 2009-<%=strYYYY%> <a href="http://www.mollatech.com" target="_blank"> (www.mollatech.com)</a></p>
                <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
            </div>
        </footer>
    </body>
</html>
