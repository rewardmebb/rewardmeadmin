<%@page import="com.mollatech.rewardme.nucleus.db.RmCreditinfo"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.CreditManagement"%>
<%
    String developerIdStr = request.getParameter("developerId");
    if (developerIdStr == null || developerIdStr.equals("-1")) {
        return;
    }
    int devId = Integer.parseInt(developerIdStr);
    RmCreditinfo info = new CreditManagement().getDetailsByOwnerId(devId);
    double availableCr = 0f;
    double credit = 0f;
    if (info != null) {
        availableCr = info.getMainCredit();

    }
%>
<table>
    <tr>
        <td>
            <label class="control-label" >Available Credits</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td >
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <%
                        if (info != null) {%>
                    <input type="text" class="form-control"  placeholder="Price" id="mainCredits" name="mainCredits" value="<%=availableCr%>" readonly>                                    
                    <% } else {%>
                    <input type="text" class="form-control"  placeholder="Not Available" readonly="" >                                    
                    <%}%>
                </div>
            </div>
        </td>
        <td>
            <label class="control-label" >&nbsp;&nbsp;&nbsp;&nbsp;Credit want to add</label>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" id="Credits" name="Credits" placeholder="Credit" value="0" >                                    
                </div>
            </div>
        </td>
    </tr>

</table>
<br><br>
<a  class="btn btn-success  btn-sm" onclick="addCredits('<%=devId%>')"><i class="fa fa-edit"></i> Add Credits</a> 