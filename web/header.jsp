
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.ApprovedPackageManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmBrandownerdetails"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.BrandOwnerManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.MakerChecker"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.UnapprovalPackageManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails"%>
<%@page import="com.mollatech.rewardme.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.rewardme.nucleus.db.RmOperators"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin Portal</title>
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- DataTables CSS -->
        <!--        <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
                <link href="bower_components/datatables-responsive/css/responsive.bootstrap.scss" rel="stylesheet" type="text/css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <!--        <script src="js/bootstrap.min.js"></script> -->
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <!--        <script src="js/bootbox.min.js" type="text/javascript"></script>-->
        <script src="./js/modal.js"></script>
        <link  rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
        <link  rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.bootstrap.min.css">
        <link  rel="stylesheet" href="//cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">       

        <%
            final int ADMIN = 0;
            final int OPERATOR = 1;
            final int REPORTER = 2;
            final int SUSPENED = 0;
            long partnerProductionRequest = 0;
            long resourceOwnerRequest = 0;
            // new change
            long partnerRequest = 0;
            response.setHeader("Cache-Control", "no-cache,must-revalidate"); //Forces caches to obtain a new copy of the page from the origin server
            response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
            response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            int newPackageRequest = 0;
                RmApprovedpackagedetails[] rmapproved = null;
                rmapproved = new ApprovedPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
                if (rmapproved != null) {
                    newPackageRequest = rmapproved.length;
                }  
            if (SessionId == null) {
        %>
        <jsp:forward page="operatorlogin.jsp" />

        <%     }%>
        <%
            String channelId = (String) request.getSession().getAttribute("_channelId");
            // new change
            RmOperators opObj = null;
            String name = null;
            opObj = (RmOperators) request.getSession().getAttribute("_apOprDetail");
            name = opObj.getName();
            
            RmBrandownerdetails[] ps = new BrandOwnerManagement().getAllBrandOwnerDetails(SessionId);
            if (ps != null) {
                partnerRequest = ps.length;
            }
        %>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp"><b>Admin Portal</b></a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                            <i class="fa fa-users fa-fw"></i>
                            <span class="badge bg-important"><%=partnerRequest%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="partners.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> Brand Owner
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                                     
                        </ul>

                    </li>
                    
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <span class="badge bg-important"><%=newPackageRequest%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="packageRequest.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> Package request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                          
                        </ul>    
                    </li>                   
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <span class="username"><%=name%></span>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">                            
                            <li><a href="oplogout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->
                <%
                        Object setting = new SettingsManagement().getSetting(channelId, SettingsManagement.MAKERCHAKER, SettingsManagement.PREFERENCE_ONE);
                        MakerChecker makerChakerObj = (MakerChecker) setting;
                        session.setAttribute("makerChacker", makerChakerObj);
                        if (makerChakerObj != null) {
                            if (makerChakerObj.chaker.contains(opObj.getEmailid()) || makerChakerObj.status == GlobalStatus.SUSPEND) {
                                int sglength11 = 0;
                                RmUnapprovalpackagedetails[] sgrequest1 = null;
                                //sgrequest1 = new UnapprovalPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
//                                if (sgrequest1 != null) {
//                                    sglength11 = sgrequest1.length;
//                                }                                
                                
                            }
                        }
                %>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">                                                                                                                                           
                            <%
                                if (makerChakerObj != null) {
                                    if (makerChakerObj.maker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
                            %>
                            <li>
                                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
<!--                                    <li>                                  
                                        <a href="smsgateway.jsp">SMS configuration setting</a>    
                                    </li>                              -->
                                    <li>                               
                                        <a href="emailgateway.jsp">Email configuration setting</a>      
                                    </li>                                     
                                </ul>
                            </li>                             
                            <li>
                                <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
                                    <li>                                  
                                        <a href="package.jsp">Package Manager</a>    
                                    </li>                                                                                          
                                </ul>
                            </li>
                            <%}else if (makerChakerObj.status == GlobalStatus.SUSPEND) {%>
                            <li>
                                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
<!--                                    <li>                                  
                                        <a href="smsgateway.jsp">SMS configuration setting</a>    
                                    </li>                              -->
                                    <li>                               
                                        <a href="emailgateway.jsp">Email configuration setting</a>      
                                    </li> 
                                    <li>                               
                                        <a href="makerChecker.jsp">Maker Checker setting</a>      
                                    </li>  
                                </ul>
                            </li> 
                            <li>
                                <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>   
                                <ul class="nav nav-second-level">                        
                                    <li>                                  
                                        <a href="package.jsp">Package Manager</a>         
                                    </li>                                                                      
                                </ul>
                            </li>
                            
                            <%}else{
                            %>
                            <li>
                                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
<!--                                    <li>                                  
                                        <a href="smsgateway.jsp">SMS configuration setting</a>    
                                    </li>                              -->
                                    <li>                               
                                        <a href="emailgateway.jsp">Email configuration setting</a>      
                                    </li> 
                                    <li>                               
                                        <a href="makerChecker.jsp">Maker Checker setting</a>      
                                    </li>  
                                </ul>
                            </li> 
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>   
                                <ul class="nav nav-second-level">                        
                                    <li>                                  
                                        <a href="packageForChecker.jsp">Package Manager</a>         
                                    </li>                                                                      
                                </ul>
                            </li>                                                                                  
                            <%}}else{%>
                            <li>
                                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
<!--                                    <li>                                  
                                        <a href="smsgateway.jsp">SMS configuration setting</a>    
                                    </li>                              -->
                                    <li>                               
                                        <a href="emailgateway.jsp">Email configuration setting</a>      
                                    </li>
                                    <li>                               
                                        <a href="makerChecker.jsp">Maker Checker setting</a>      
                                    </li> 
                                </ul>
                            </li>
                            <li>
                                <a href="package.jsp"><i class="fa fa-shopping-cart fa-fw"></i> Package Manager</a>
                            </li> 
                            <%}%>
                            <li>
                                <a href="addCredits.jsp"><i class="fa fa-money fa-fw"></i> Add Credits</a>
                            </li>
                            <li>
                                <a href="addOperator.jsp"><i class="fa fa-users fa-fw"></i> Operator Management</a>
                            </li>
                        </ul>

                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
        </div>
   
<!-- /.row -->